#!/usr/bin/env python
import os
from os import listdir  #to list directories
from os.path import isdir, join  #check existence of dir, join paths


rmFlag = False
#@Todo make as a class to be used by both rivet and pythia?
pythia_dir = "/home/vozak/top_ana/pythia8"
rivet_dir = "/home/vozak/top_ana/rivet"
setup_dir = "/home/vozak/setup"
pythia_exe_name = 'ttbar_cr'
rivetjob_dir = 'rivetJobs'
#@Todo pass it as an argument

#Choose type of events (Minimum bias or top)
type_choice = eventType = raw_input("Choose top, mb, ue, pull or fragmentation func (t/m/ue/p/f) \n")
cMB = ('mb', 'm')
cTOP = ('top', 't')
cPUL = ('pull', 'p')
cUE = ('ue', 'u')
cFrag = ('frag', 'f')

eT = type_choice.strip().lower()
if eT in cTOP:
   analysis_name = 'ATLAS_2016_UETop' 
   pyOutput = 'top'
if eT in cMB:
   analysis_name = 'ATLAS_2015_UEana' 
   pyOutput = 'mb'
if eT in cPUL:
   analysis_name = 'ATLAS_2015_I1376945' 
   pyOutput = 'top'
if eT in cUE:
   analysis_name = 'ATLAS_2017_I1509919'
   pyOutput = 'mb'
if eT in cFrag:
   analysis_name = 'ATLAS_2011_I929691'
   pyOutput = 'jet_deftune/p5GeVinv5'


cms_choice = eventType = raw_input("Choose CMS energy (7,8,13) \n")
c13 = ('13', '13TeV')
c7 = ('7', '7TeV')
c8 = ('8', '8TeV')
#@Todo check
cms_dir = 'Jobs7TeV'
cT = type_choice.strip().lower()
if cT in c13:
   cms_dir = 'Jobs13TeV' 
if cT in c7:
   cms_dir = 'Jobs7TeV' 
if cT in c8:
   cms_dir = 'Jobs8TeV' 


#pythia_exe_name = pyFile_name
pythiajob_dir = cms_dir + '/pythia8Jobs/' + pyOutput
print pythiajob_dir
#pythiajob_dir = 'Jobs8TeV/pythia8Jobs/' + pyOutput

bin_dir = "/home/vozak/bin"
hepmc_files = []
run_list = []
#_pythia_exe = join(pythia_dir, pythia_exe_name )
#@Todo check existance of dir
pythia_working_dir = join( pythia_dir, pythiajob_dir )
rivet_working_dir = join( rivet_dir, rivetjob_dir)
if os.path.isdir( rivet_working_dir): 
   message = '%s directory in %s already exists' % (rivetjob_dir, rivet_dir)
   remove = True if rmFlag == True else False
   if not remove:
            choices = 'y/n/a'
            choice = raw_input("%s \n Delete (%s) ?" % (message, choices))
            valuesY = ('y', 'yes')
            valuesN = ('n', 'no')
            valuesA = ('a', 'all')
            #strip() removes spaces, lower() return string with lowercased characters 
            ch = choice.strip().lower()
            decision = False
            if ch in valuesY:
                  remove = True

            if ch in valuesA:
                  remove = True
                  rmFlag = True
            if remove:
                  print "Removing " + rivet_working_dir
                  os.system( "rm -r %s/*" % rivet_working_dir)
            else:
                  print "End"
                  quit()
               
if not os.path.isdir( pythia_working_dir ): 
   print '%s directory in %s is not created, do configuration of pythia first (./config_skeleton.py)' % (pythiajob_dir, pythia_dir)
else:
   #@Todo check existance of dir
   #os.mkdir( rivet_working_dir )
   pythia_modes_dir = os.listdir(  pythia_working_dir )  
   #pythia_modes_dir = [ x for x in pythia_modes_dir_all if ]  

   merge_file = open( join(rivet_dir, 'merge_rivet.sh'), 'w' )
   submitfile = open(join(rivet_dir, 'submit_rivet.sh'), 'w')
   merge_file.write('source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n' )
   merge_file.write( 'source %s \n' %join(setup_dir, 'setup_rivet.sh') )

   print pythia_modes_dir
   print len(pythia_modes_dir)
   #if (len(pythia_modes_dir) != 0)
   for mode_dir in pythia_modes_dir:
      _rivet_mode_dir = join( rivet_working_dir, mode_dir)
      _pythia_mode_dir = join( pythia_working_dir, mode_dir)
      #Copy the working structure of dir for rivet
      rivet_mode_dir = join(rivet_working_dir, mode_dir )
      os.mkdir( rivet_mode_dir )
      files = os.listdir( join( pythia_working_dir, mode_dir) )
      #Find only the .hepmc files
      yoda_merge = []
      hepmc = [f for f in files if(f.find('.hepmc') != -1)]
      for f in hepmc:
           hepmc_files.append(f) 
           run_rivet =  f.replace( pythia_exe_name, 'runrivet_' ) 
           vFlag_helper = f.replace(pythia_exe_name, '')
           vFlag = vFlag_helper.replace('.hepmc', '') 
           #vFlags.append( vFlag )
           rivet_output_name = 'Rivet_' + vFlag + '.yoda'
           _rivet_output_name = join(_rivet_mode_dir, rivet_output_name )
           yoda_merge.append( _rivet_output_name )
           run_rivet = run_rivet.replace( '.hepmc', '.sh') 
           _run_rivet = join( rivet_mode_dir, run_rivet )
           #print run_rivet
           run_file = open( _run_rivet, 'w')
           #setup atlas
           run_file.write( 'export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n')
           run_file.write( 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n')
           #setup rivet
           run_file.write( 'source %s \n' %join(setup_dir, 'setup_rivet.sh') )
           run_file.write( 'cd %s \n' % rivet_dir )
           run_file.write( 'export RIVET_ANALYSIS_PATH=$PWD\n')
           run_file.write( 'rivet --analysis=%s %s -o %s \n' % (analysis_name, join(_pythia_mode_dir, f), _rivet_output_name ) )
           run_list.append(_run_rivet) 

          #@Todo iatlas or gridatlas?
           submitfile.write( 'cd %s \n' % rivet_mode_dir )
           submitfile.write( '%s/send2pbs gridatlas %s \n' % (bin_dir, _run_rivet) )


      #File for merging output yoda file
      #merge_file = open( join(rivet_dir, 'merge_rivet.sh'), 'a' )
      merge_file.write( 'yodamerge -o%s%s.yoda ' %  (join(rivet_working_dir, 'Rivet_' ),mode_dir)  )  
      for yoda in yoda_merge:          
         merge_file.write( '%s ' %  yoda )  
      merge_file.write( '\n' )  
      
         
   #submitfile = open(join(rivet_dir, 'submit_rivet.sh'), 'w')
  # for run in run_list:
   #   submitfile.write( 'send2pbs gridatlas %s \n' % run)



