// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include <utility> //for pairs
#include <cmath> //for ceil

namespace Rivet {
/// @brief ATLAS 13 TeV Top-UE analysis
///
/// @author M .Vozak <mvozak@cern.ch>
class ATLAS_2016_UETop : public Analysis {
public:
    /// Constructor
    /// Q:What for?
    DEFAULT_RIVET_ANALYSIS_CTOR(ATLAS_2016_UETop);

    enum CUT_FLOW{
        none,
        n_elmu_neutrions,
        n_elmu,
        overlap,
        //e_phasespace,
        //mu_phasespace,
        emu_phasespace,
        n_bjets,
        n_lightjets,
        near_w_mass
    };
    enum Particle_samples{
        PseudoHadronic_bjet,
        PseudoLeptonic_bjet,
      //  Nearbjet,
      //  Nearljet,
        Lightjet1,
        Lightjet2,
        //UE
        UE_inclusive,
        UE_notrackjets,
        //Regions
        rAll,
        rToward,
        rTransverseMin,
        rTransverseMax,
        rTransverse,
        rAway,
        Nsamples
    };

    enum dR_CUT{
        dRl1,
        dRg1,
        dRl25,
        dRg25,
        dRl42
    };

    enum ANNULUS{
        INNER,
        OUTER
    };

    enum CUT_PT{
        noptcut,
        pt25_50GeV,  //25 - 50 GeV
        pt50_100GeV, //50 -100 GeV
        pt100_hGeV,  //from 100 to higher values GeV
        Nptcuts
    };

   //Pt in GeV
   CUT_PT which_pt_cut(double pt){
      CUT_PT pt_cut = Nptcuts;
        if( pt > 100 ) pt_cut = pt100_hGeV;
        else if( pt > 50 ) pt_cut = pt50_100GeV;
        else if( pt > 25 ) pt_cut = pt25_50GeV;
    //should not be lesser then 25 GeV cut
    assert( pt_cut != Nptcuts);
    return pt_cut;
   }

    bool pass_dR( dR_CUT cut, double value ){
        bool pass = false;
        switch(cut){
        case dR_CUT::dRl1:
            pass = (value < 1. ) ? true:false;
            break;
        case dR_CUT::dRg1:
            pass = (value > 1. ) ? true:false;
            break;
        case dR_CUT::dRl25:
            pass = (value < 2.5 ) ? true:false;
            break;
        case dR_CUT::dRg25:
            pass = (value > 2.5 ) ? true:false;
            break;
        case dR_CUT::dRl42:
            pass = (value < 4.2 ) ? true:false;
            break;
        }
        return pass;
    }

    int which_cut(CUT_FLOW cut){
        std::map<CUT_FLOW, int> cut_flow;
        cut_flow[CUT_FLOW::none] = 1;
        cut_flow[CUT_FLOW::n_elmu_neutrions] = 2;
        cut_flow[CUT_FLOW::n_elmu] = 3;
        cut_flow[CUT_FLOW::overlap] = 4;
        // cut_flow[CUT_FLOW::e_phasespace] = 5;
        // cut_flow[CUT_FLOW::mu_phasespace] = 6;
        cut_flow[CUT_FLOW::emu_phasespace] = 5;
        cut_flow[CUT_FLOW::n_bjets] = 6;
        cut_flow[CUT_FLOW::n_lightjets] = 7;
        cut_flow[CUT_FLOW::near_w_mass] = 8;
        for(auto c : cut_flow){
            if( cut == c.first ) return c.second;
        }
        //if not find among CUT_FLOW return underflow bin
        return -1;
    }


    const static int    nBin_nch   ;
    const static double Bin_nchMin ;
    const static double Bin_nchMax ;
    //Q: Best way to initialize?
    const static int NiAn = 9; // number of inner annuluses under study
    //@Todo change globally also inAnRange
    const static int NoAn = 4; //number of outer annuluses under study

    std::vector<int> inAnRange;  //inner annulus ranges
    std::vector<int> outAnRange; //outer annulus ranges
    //std::vector<int> selected_inAnRange;
    //particle types included
    enum partTypes {
        k_AllCharged,
        k_NoStrange,
        kNPartTypes
    };

    //@Todo: Use cut class?
    std::vector< std::pair< std::string, double > > top_pt_slices;
    //labels for different particle types
    const static string partTypeStr[kNPartTypes];
    const static string regionStr  [Nsamples];
    const static string ptCutStr  [Nptcuts];
    //const static unsigned int NdR = 4; // number of different UE_deltaR in study//i
    const static double w_mass;

    // Angular region definition: TR_SIZE is half the angle of the total transverse region angle
    const static double TR_SIDE_SIZE;// = M_PI/3.0; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
    const static double TR_SIZE;// = 2*TR_SIDE_SIZE; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
    const static double TO_ANGLE;// = M_PI/2.0 - TR_SIDE_SIZE/2.0; //< dphi at which TO region ends and TR begins
    const static double TO_SIZE;// = 2*TO_ANGLE; // total toward size
    const static double AW_ANGLE;// = M_PI/2.0 + TR_SIDE_SIZE/2.0; //< dphi at which TR region ends and AW begins
    const static double AW_SIZE;// = 2*(M_PI - AW_ANGLE); // total away size



    FourMomentum pseudoLEPbjet; //Momentum of bjet1
    FourMomentum pseudoHADbjet; //Momentum of bjet2
    FourMomentum pjet1;
    FourMomentum pjet2;

    void init() {
        //Initialize values of inner annuluses
        for( int iR = 0; iR <= NiAn; ++iR ){
            inAnRange.push_back( 2 + 4*iR );
        }
        for( int iR = 0; iR <= NoAn; ++iR ){
            outAnRange.push_back( 5 + 2*iR );
        }

        // Top pt cuts
        // double pt_lthresh = 40;
        double pt_hthresh = 100;
        double pt_step = 10;
        for( unsigned int iSlice = 1; iSlice <= 5; ++iSlice){
            //Q: Use Cut class?
            std::pair < std::string, double > top_pt_slice;
            top_pt_slice = make_pair( std::to_string( (int)(pt_hthresh - pt_step*iSlice) ) + "GeV", (pt_hthresh - pt_step *iSlice)*GeV  );
            top_pt_slices.push_back( top_pt_slice );
        }
        //for( unsigned int dR = 0; dR <= NdR; ++dR) UE_Delta_Rs.push_back( 0.8 + 4*dR );

        // Eta ranges
        //Q:Really from 1MeV?
        Cut eta_full = (Cuts::abseta < 5.0) & (Cuts::pT >= 1.0*MeV);
        Cut eta_lep = (Cuts::abseta < 2.5);

        // All final state particles
        //Q: No need for addProjection?
        //A: Already inside FinalState constructor

        //Q: What does addProjection really do?
        //Q: How is the final state defined? What about strange baryons?
        FinalState fs(eta_full);

        //Q: Identification of final states? Splitting into categories (electrons, muons, neutrinos, ...)?
        IdentifiedFinalState photons(fs);
        //Q:Conteiner of particles ID?
        photons.acceptIdPair(PID::PHOTON);

        // Projection to find the electrons
        IdentifiedFinalState el_id(fs);
        el_id.acceptIdPair(PID::ELECTRON);

        //Q: Why do I need to introduce Prompt?
        PromptFinalState electrons(el_id);
        //Q: Accept electrons decayed from muon from tau except directly produced ones?
        electrons.acceptTauDecays(true);
        //Q: what does it do? Why do I need to do this?
        //A: Declare projection -> register projection
        //Q: What is meant by register?
        declare(electrons, "electrons");

        //A: Clustered Object where photons are assigned to leptons
        //Get photons to dress leptons
        DressedLeptons dressedelectrons(photons, electrons, 0.1, eta_lep & (Cuts::pT >= 25.0*GeV), true, true);
        declare(dressedelectrons, "dressedelectrons");

        DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
        declare(ewdressedelectrons, "ewdressedelectrons");

        DressedLeptons vetodressedelectrons(photons, electrons, 0.1, eta_lep & (Cuts::pT >= 15.0*GeV), true, true);
        declare(vetodressedelectrons, "vetodressedelectrons");

        // Projection to find the muons
        IdentifiedFinalState mu_id(fs);
        mu_id.acceptIdPair(PID::MUON);
        PromptFinalState muons(mu_id);
        muons.acceptTauDecays(true);
        declare(muons, "muons");
        DressedLeptons dressedmuons(photons, muons, 0.1, eta_lep & (Cuts::pT >= 25.0*GeV), true, true);
        declare(dressedmuons, "dressedmuons");
        DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
        declare(ewdressedmuons, "ewdressedmuons");
        DressedLeptons vetodressedmuons(photons, muons, 0.1, eta_lep & (Cuts::pT >= 15.0*GeV), true, true);
        declare(vetodressedmuons, "vetodressedmuons");

        // Projection to find neutrinos and produce MET
        IdentifiedFinalState nu_id;
        nu_id.acceptNeutrinos();
        PromptFinalState neutrinos(nu_id);
        //neutrinos.acceptTauDecays(true);
        neutrinos.acceptTauDecays(false); //small effect on n_neutrino cut?

        declare(neutrinos, "neutrinos");

        // Jet clustering.
        VetoedFinalState vfs;
        vfs.addVetoOnThisFinalState(ewdressedelectrons);
        vfs.addVetoOnThisFinalState(ewdressedmuons);
        vfs.addVetoOnThisFinalState(neutrinos);
        FastJets jets(vfs, FastJets::ANTIKT, Delta_R);
        //CHANGE: A-S article exclude neutrinos from forming a jet!!
        jets.useInvisibles();
        declare(jets, "jets");


        /////UE
        //Q: Diff cut for pt of final charged particle
        const ChargedFinalState cfs500(-2.5, 2.5, 500*MeV);
        addProjection(cfs500, "CFS500");
        //Q: Init outside and pass as a argument, or assigned through function?
        // std::vector<double> ptbinning = ue_ptbinning();
        /* for (int iType=0; iType < kNPartTypes; ++iType) {
        for (unsigned int iRegion=0; iRegion < Nregions; ++iRegion) {
        for( unsigned int dR = 0; dR < NdR; ++dR ){  //Looping over different UE cones
            //@Todo Select better binning
            // _hist_ptavg_toppt [iType][iRegion][dR]  = bookProfile1D( partTypeStr[iType] + "_ptavgpt_"  + regionStr[iRegion] + "_ueR" + std::to_string(dR)  ,ptbinning);
            // _hist_ptavg_nch[iType][iRegion]  = bookProfile1D( partTypeStr[iType] + "_ptavgnch_"    + regionStr[iRegion] , nBin_nch, rangeMin_nch, rangeMax_nch);

        }
        }
    }
    */
        //@Todo similar cut flow as in UE analysis
        cut_flow = bookHisto1D( "cut_flow", 10, 0, 10  );

        //@Todo binning?
        vector<double> pt_w_binning;//[41];
        vector<double> pt_t_binning;//[39];
        vector<double> pt_el_binning;//[37];
        //double pt_mu_binning[];
        vector<double> pt_lj_binning;//[37];
        vector<double> mpt_binning;
        vector<double> pt_binning;
        vector<double> spt_binning;

        vector<double> r_binning;
        // double pt_slj_binning[];

        //w pt binning
        for(unsigned int i = 0; i<=30 ; ++i){
            //pt_w_binning[i] = 5*i;
            pt_w_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=9 ; ++i){
            //pt_w_binning[i+31] = 160 + 10*i;
            pt_w_binning.push_back(160 + 10*i);
        }

        //top pt binning
        for(unsigned int i = 0; i<=5 ; ++i){
            //pt_t_binning[i] = 10*i;
            pt_t_binning.push_back(10*i);
        }
        for(unsigned int i = 0 ; i<=19 ; ++i){
            //pt_t_binning[i+6] = 55 + 5*i;
            pt_t_binning.push_back( 55 + 5*i);
        }
        for(unsigned int i = 0 ; i<=9; ++i){
            //pt_t_binning[i+26] = 160 + 10*i;
            pt_t_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=2; ++i){
            //pt_t_binning[i+37] = 260 + 20*i;
            pt_t_binning.push_back( 260 + 20*i);
        }

        //el pt binning = mu pt binning
        for(unsigned int i = 0; i<=30 ; ++i){
            //pt_el_binning[i] = 5*i;
            pt_el_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=4 ; ++i){
            //pt_el_binning[i+31] = 160 + 10*i;
            pt_el_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=1 ; ++i){
            //pt_el_binning[i+36] = 250 + 50*i;
            pt_el_binning.push_back(250 + 50*i);
        }

        //lj pt binning
        for(unsigned int i = 5; i<=30 ; ++i){
            pt_lj_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=4 ; ++i){
            //pt_lj_binning[i+31] = 160 + 10*i;
            pt_lj_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=1 ; ++i){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_lj_binning.push_back(250 + 50*i);
        }


  //      for(unsigned int i = 0 ; i<=25 ; ++i){
  //       mpt_binning.push_back( i );
  //      }

        for(double i = 0.5 ; i <=3 ; i=i+0.1){
            //pt_lj_binning[i+36] = 250 + 50*i;
            r_binning.push_back( i );
        }
        for(double i = 3.2 ; i <=3.4 ; i=i+0.2){
            //pt_lj_binning[i+36] = 250 + 50*i;
            r_binning.push_back( i );
        }
        for(double i = 3.8 ; i <=4.2 ; i=i+0.4){
            //pt_lj_binning[i+36] = 250 + 50*i;
            r_binning.push_back( i );
        }



        for(double i = 0.5 ; i <=15 ; i=i+0.5){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }
        for(double i = 16 ; i <=30 ; i=i+1.0){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }

        for(double i = 32 ; i <= 60 ; i=i+2.0){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }
        for(double i = 64 ; i <= 100 ; i=i+4.0){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }

        spt_binning = pt_binning;
        for(double i = 105; i<=200; i=i+5.0) spt_binning.push_back(i);

        for(double i = 0 ; i <=5 ; i=i+0.1){
            //pt_lj_binning[i+36] = 250 + 50*i;
            mpt_binning.push_back( i );
        }
        for(unsigned int i = 6 ; i<=70 ; ++i){
         mpt_binning.push_back( i );
        }

        //Binning and ranges
        double eta_min = -2.5;
        double eta_max = 2.5;
        int eta_Nbin = 50;

        double phi_min = -1.;
        double phi_max = 1.;
        int phi_Nbin = 50;

        //Mass distribution of event objects
        _h["mass_ttbar"]       = bookHisto1D( "ttbarmass", 40, 200, 1000, "", "m_{top}", "ent"  );
        _h["mass_tbar"]        = bookHisto1D( "tmass_leptonic", 800, 100, 300, "", "m_{top}", "ent"  );
        _h["mass_t"]           = bookHisto1D( "tmass_hadronic", 800, 100, 300, "", "m_{top}", "ent"  );
        _hmass["mass_t_unnorm"]           = bookHisto1D( "tmass_hadronic_unnorm", 800, 100, 300, "", "m_{top}", "ent"  );

        _h["mass_Wleptonic"]   = bookHisto1D( "wmass_leptonic", 180, 30, 120, "", "m_{W}", "ent"  );
        _h["mass_Whadronic"]   = bookHisto1D( "wmass_hadronic", 180, 30, 120, "", "m_{W}", "ent"  );
        //_h["mass_lightjet"]    = bookHisto1D( "mass_lj",600, 0, 300, "", "m_{2lj}", "ent"  );


        //Multiplicity of lightjets
        _h["n_lj"]                = bookHisto1D( "n_lj",11, -0.5, 10.5, "", "n_{lj}", "ent"  );


        //Delta R distributions between various event objects
        _h["dr_lj"]  = bookHisto1D( "lightjetcorrelation", 42, 0, 4.2, "", "#Delta R", "1/N dN /d #Delta R");
       // _h["dr_wbjet"]     = bookHisto1D( "bjetwcorrelation", 42, 0, 4.2, "", "#Delta R", "1/N dN /d #Delta R");
        _h["dr_bbbar"]  = bookHisto1D( "bbjetcorrelation", 42, 0, 4.2, "", "#Delta R", "1/N dN /d #Delta R");
        _h["hhist_jetshape"]       = bookHisto1D( "helphist", 50, 0, 1, "", "r/R", "#rho");
        //Delta R correlations
        _h2["dr_sbljet"]  = bookHisto2D( "sameblcor", r_binning, r_binning , "", "dr_{b1,b2}", "dr_{b,l}", "");
        _h2["dr_cbljet"]  = bookHisto2D( "closeblcor", r_binning, r_binning , "", "dr_{b1,b2}", "dr_{b,l}", "");
        _h2["dr_clsmbljet"]  = bookHisto2D( "closesameblcor", r_binning, r_binning , "", "dr_{b,bl}", "dr_{b,l}", "");
       //Comparison of truth top vs reconstructed top
        _h["dr_tTruthReco"]  = bookHisto1D( "had_truthVreco_r", 42, 0, 4.2);
        _h["dr_tbarTruthReco"]  = bookHisto1D( "lep_truthVreco_r", 42, 0, 4.2);



        //Invariant mass of closest lightjet to bjet which is from the same decay
        _h["sbll_invmass"]   = bookHisto1D( "sbll_invmass", 200, 0, 200, "", "inv mass", "ent");
        //Invariant mass of the closest lightjet to bjet
        _h["cbll_invmass"]   = bookHisto1D( "cbll_invmass", 200, 0, 200, "", "inv mass", "ent");



        //Dummy hist for storing the diferential jet shape from individual jets
        //TODO: Try to plot for different pT slices
        _p["diffjetshape" ]   = bookProfile1D( "difjetshape", 50, 0, 1,"", "r/R", "#rho"  );
        _h["diffbjetshape" ]  = bookHisto1D( "difbjetshape", 50, 0, 1,"", "r/R", "#rho"  );
        _h["diffabjetshape" ] = bookHisto1D( "difabjetshape", 50, 0, 1,"", "r/R", "#rho"  );
        _h["diffl1jetshape" ] = bookHisto1D( "difl1jetshape", 50, 0, 1,"", "r/R", "#rho"  );
        _h["diffl2jetshape" ] = bookHisto1D( "difl2jetshape", 50, 0, 1,"", "r/R", "#rho"  );
       // foreach( auto slice,  top_pt_slices  ){
       //     _p["diffjetshape" + slice.first + "pt"]     = bookProfile1D( "difjetshape" + slice.first, 50, 0, 1,"", "r/R", "#rho"  );
       // }


        //Pseudorapidity of event objects
        _h["eta_e"]       = bookHisto1D( "eta_e",  eta_Nbin, eta_min,eta_max , "electron pseudorapidity", "#eta_{e}", "ent"  );
        _h["eta_mu"]      = bookHisto1D( "eta_mu", eta_Nbin, eta_min,eta_max , "muon pseudorapidity", "#eta_{#mu}", "ent"  );
        _h["eta_b"]       = bookHisto1D( "eta_hb", eta_Nbin, eta_min,eta_max , "b pseudorapidity", "#eta_{hb}", "ent"  );
        _h["eta_bbar"]    = bookHisto1D( "eta_lb", eta_Nbin, eta_min,eta_max , "bbar pseudorapidity", "#eta_{ab}", "ent"  );
        _h["eta_l1"]      = bookHisto1D( "eta_l1", eta_Nbin, eta_min,eta_max , "lightjet1 pseudorapidity", "#eta_{l1}", "ent"  );
        _h["eta_l2"]      = bookHisto1D( "eta_l2", eta_Nbin, eta_min,eta_max , "lightjet2 pseudorapidity", "#eta_{l2}", "ent"  );
        _h["eta_t"]       = bookHisto1D( "eta_ht", eta_Nbin, eta_min,eta_max , "t pseudorapidity", "#eta_{ht}", "ent"  );
        _h["eta_tbar"]    = bookHisto1D( "eta_lt", eta_Nbin, eta_min,eta_max , "tbar pseudorapidity", "#eta_{lt}", "ent"  );
        _h["eta_ttbar"]   = bookHisto1D( "eta_ttbar", eta_Nbin, eta_min, eta_max, "ttbar pseudorapidity", "#eta_{ttbar}", "ent"  );
        _h["deta_ttbar"]  = bookHisto1D( "deta_ttbar", 50, 0, 5, "pseudorapidity difference between t and tbar", "#Delta #eta", "1/N dN /d #Delta #eta");
        //Pseudorapidity correlations
        _h2["eta_tVtbar"] 			 = bookHisto2D( "topetaLvH", 25, 0 ,2.5, 25, 0, 2.5, "", "eta_{W->qq}", "eta_{W->ll}", "");
        _h2["eta_ttruVtreco"]        = bookHisto2D( "topetaRvT", 25, 0 ,2.5, 25, 0, 2.5, "", "eta_{W->qq}", "eta_{W->ll}", "");
        _h2["eta_tbartruVtbarreco"]  = bookHisto2D( "atopetaRvT", 25, 0 ,2.5, 25, 0, 2.5, "", "eta_{W->qq}", "eta_{W->ll}", "");


        //Azimuthal distributions of event objects
        _h["phi_e"]        = bookHisto1D( "phi_e",  phi_Nbin, phi_min,phi_max , "electron azimuthal angle", "#phi_{e}", "ent"  );
        _h["phi_mu"]       = bookHisto1D( "phi_e",  phi_Nbin, phi_min,phi_max , "muon azimuthal angle", "#phi_{#mu}", "ent"  );
        _h["phi_b"]        = bookHisto1D( "phi_e",  phi_Nbin, phi_min,phi_max , "b azimuthal angle", "#phi_{b}", "ent"  );
        _h["phi_bbar"]     = bookHisto1D( "phi_e",  phi_Nbin, phi_min,phi_max , "bbar azimuthal angle", "#phi_{ab}", "ent"  );
        _h["phi_l1"]       = bookHisto1D( "phi_l1", phi_Nbin, phi_min,phi_max , "lightjet1 azimuthal angle", "#phi_{l1}", "ent"  );
        _h["phi_l2"]       = bookHisto1D( "phi_l2", phi_Nbin, phi_min,phi_max , "lightjet2 azimuthal angle", "#phi_{l2}", "ent"  );
        _h["phi_t"]        = bookHisto1D( "phi_ht", phi_Nbin, phi_min,phi_max , "t azimuthal angle", "#phi_{ht}", "ent"  );
        _h["phi_tbar"]     = bookHisto1D( "phi_lt", phi_Nbin, phi_min,phi_max , "tbar azimuthal angle", "#phi_{lt}", "ent"  );
        _h["phi_ttbar"]    = bookHisto1D( "phi_ttbar", phi_Nbin, phi_min, phi_max, "ttbar azimuthal angle", "#phi_{ttbar}", "ent"  );
        _h["dphi_ttbar"]   = bookHisto1D( "dphi_ttbar", 10, 0, 1, "azimuthal angle dif between t and tbar", "#Delta #phi", "1/N dN /d #Delta #phi");
       //Azimuthal angle correlations
        _h2["phi_tVtbar"]           = bookHisto2D( "topphiLvH", 100, 0, 1, 10, 0, 1, "", "phi_{W->qq}", "phi_{W->ll}", "");
        _h2["phi_ttruVtreco"]       = bookHisto2D( "topphiRvT", 100, 0, 1, 10, 0, 1, "", "phi_{W->qq}", "phi_{W->ll}", "");
        _h2["phi_tbartruVtbarreco"] = bookHisto2D( "atopphiRvT", 100, 0, 1, 10, 0, 1, "", "phi_{W->qq}", "phi_{W->ll}", "");


       // Eta-Phi correlations between various object
        _h2["etaphi_bttbar"]     = bookHisto2D( "bttbaretaphi", 50,0,5, 50, 0., 1., "", "pT_{reco}", "pT_{truth}", "");
        _h2["etaphi_bbarttbar"]  = bookHisto2D("abttbaretaphi", 50,0,5, 50, 0., 1., "", "pT_{reco}", "pT_{truth}", "" );
        _h2["etaphi_l1ttbar"]    = bookHisto2D("l1ttbaretaphi", 50,0,5, 50, 0., 1., "", "pT_{reco}", "pT_{truth}", "" );
        _h2["etaphi_l2ttbar"]    = bookHisto2D("l2ttbaretaphi", 50,0,5, 50, 0., 1., "", "pT_{reco}", "pT_{truth}", "" );


        //Transverse momentum of event objects
        _h["pt_Wleptonic"]   = bookHisto1D( "ptw_l",  pt_w_binning , "", "pT_{wl}", "ent"  );
        _h["pt_Whadronic"]   = bookHisto1D( "ptw_h",  pt_w_binning, "", "pT_{wh}", "ent"  );
        _h["pt_tbar"]        = bookHisto1D( "ptT_l",  pt_t_binning, "", "pT_{Tl}", "ent"  );
        _h["pt_t"]           = bookHisto1D( "ptT_h", pt_t_binning, "", "pT_{Th}", "ent"  );
        _h["pt_e"]           = bookHisto1D( "pt_e", pt_el_binning, "", "pT_{e}", "ent"  );
        _h["pt_mu"]          = bookHisto1D( "pt_mu", pt_el_binning, "", "pT_{mu}", "ent"  );
        _h["pt_l"]           = bookHisto1D( "pt_mu", pt_el_binning, "", "pT_{lepton}", "ent"  );
        _h["pt_leadinglj"]   = bookHisto1D( "pt_leadinglj", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_Wlj"]         = bookHisto1D( "pt_Wlj", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_restlj"]      = bookHisto1D( "pt_restljs", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_lj"]          = bookHisto1D( "pt_lj", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_l1"]          = bookHisto1D( "pt_l1", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_l2"]          = bookHisto1D( "pt_l2", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_b"]   = bookHisto1D( "pt_bjet", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_b_25_30"]   = bookHisto1D( "pt_bjetparticles_25_30", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_b_25_50"]   = bookHisto1D( "pt_bjetparticles_25_50", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_bbar"]   = bookHisto1D( "pt_antibjet", pt_lj_binning, "", "pT_{bbar}", "ent"  );
        _h["pt_bbar_25_30"]   = bookHisto1D( "pt_antibjetparticles_25_30", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_bbar_25_50"]   = bookHisto1D( "pt_antibjetparticles_25_50", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_l1_25_30"]   = bookHisto1D( "pt_l1particles_25_30", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_l1_25_50"]   = bookHisto1D( "pt_l1particles_25_50", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_l2_25_30"]   = bookHisto1D( "pt_l2particles_25_30", pt_binning, "", "p_{T}", "ent"  );
        _h["pt_l2_25_50"]   = bookHisto1D( "pt_l2particles_25_50", pt_binning, "", "p_{T}", "ent"  );


        //Transverse momentum correlations
        _h2["pt_tVtbar"]           = bookHisto2D( "topptLvH", pt_t_binning, pt_t_binning, "", "pT_{W->qq}", "pT_{W->ll}", "");
        _h2["pt_ttruVtreco"]       = bookHisto2D( "topptRvT", pt_t_binning, pt_t_binning, "", "pT_{reco}", "pT_{truth}", "");
        _h2["pt_tbartruVtbarreco"] = bookHisto2D( "atopptRvT", pt_t_binning, pt_t_binning, "", "pT_{reco}", "pT_{truth}", "");

        //Nch inside jets within a certain kinematic region
        _h["nch_b_25_30"]   = bookHisto1D( "nch_bjetparticles_25_30", 200, 0.5, 200.5, "", "n_{ch}", "ent"  );
        _h["nch_b_25_50"]   = bookHisto1D( "nch_bjetparticles_25_50", 200, 0.5, 200.5, "", "n_{nch}", "ent"  );
        _h["nch_bbar_25_30"]   = bookHisto1D( "nch_antibjetparticles_25_30", 200, 0.5, 200.5, "", "n_{ch}", "ent"  );
        _h["nch_bbar_25_50"]   = bookHisto1D( "nch_antibjetparticles_25_50", 200, 0.5, 200.5, "", "n_{nch}", "ent"  );
        _h["nch_l1_25_30"]   = bookHisto1D( "nch_l1particles_25_30", 200, 0.5, 200.5, "", "n_{ch}", "ent"  );
        _h["nch_l1_25_50"]   = bookHisto1D( "nch_l1particles_25_50", 200, 0.5, 200.5, "", "n_{nch}", "ent"  );
        _h["nch_l2_25_30"]   = bookHisto1D( "nch_l2particles_25_30", 200, 0.5, 200.5, "", "n_{ch}", "ent"  );
        _h["nch_l2_25_50"]   = bookHisto1D( "nch_l2particles_25_50", 200, 0.5, 200.5, "", "n_{nch}", "ent"  );


        //Pull Angle
        _h["pa_b"]   = bookHisto1D( "pullangle_b1", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_lj"]  = bookHisto1D( "pullangle_lj1", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_bl1"]   = bookHisto1D( "pullangle_bl1", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_bl2"]   = bookHisto1D( "pullangle_bl2", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_barbl1"]   = bookHisto1D( "pullangle_barbl2", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_barbl2"]   = bookHisto1D( "pullangle_barbl2", 32, -3.2, 3.2 , "", " \theta []", "ent"  );



        //Scattering plots for division of bjet and lightjet
        _s["pt_bDl1_25_30"] = bookScatter2D( "pt_bDl1_25_30", false, "p_{T}", "p_{T}(bjet)/p_{T}(lightjet1)" );
        _s["pt_bDl1_25_50"] = bookScatter2D( "pt_bDl1_25_50", false, "p_{T}", "p_{T}(bjet)/p_{T}(lightjet1)" );
        _s["pt_bDl2_25_30"] = bookScatter2D( "pt_bDl2_25_30", false, "p_{T}", "p_{T}(bjet)/p_{T}(lightjet2)" );
        _s["pt_bDl2_25_50"] = bookScatter2D( "pt_bDl2_25_50", false, "p_{T}", "p_{T}(bjet)/p_{T}(lightjet2)" );

        _s["pt_bbarDl1_25_30"] = bookScatter2D( "pt_bbarDl1_25_30", false, "p_{T}", "p_{T}(antibjet)/p_{T}(lightjet1)" );
        _s["pt_bbarDl1_25_50"] = bookScatter2D( "pt_bbarDl1_25_50", false, "p_{T}", "p_{T}(antibjet)/p_{T}(lightjet1)" );
        _s["pt_bbarDl2_25_30"] = bookScatter2D( "pt_bbarDl2_25_30", false, "p_{T}", "p_{T}(antibjet)/p_{T}(lightjet2)" );
        _s["pt_bbarDl2_25_50"] = bookScatter2D( "pt_bbarDl2_25_50", false, "p_{T}", "p_{T}(antibjet)/p_{T}(lightjet2)" );

        _s["nch_bDl1_25_30"] = bookScatter2D( "nch_bDl1_25_30", false, "nch", "nch(bjet)/nch(lightjet1)" );
        _s["nch_bDl1_25_50"] = bookScatter2D( "nch_bDl1_25_50", false, "nch", "nch(bjet)/nch(lightjet1)" );
        _s["nch_bDl2_25_30"] = bookScatter2D( "nch_bDl2_25_30", false, "nch", "nch(bjet)/nch(lightjet2)" );
        _s["nch_bDl2_25_50"] = bookScatter2D( "nch_bDl2_25_50", false, "nch", "nch(bjet)/nch(lightjet2)" );

        _s["nch_bbarDl1_25_30"] = bookScatter2D( "nch_bbarDl1_25_30", false, "nch", "nch(antibjet)/nch(lightjet1)" );
        _s["nch_bbarDl1_25_50"] = bookScatter2D( "nch_bbarDl1_25_50", false, "nch", "nch(antibjet)/nch(lightjet1)" );
        _s["nch_bbarDl2_25_30"] = bookScatter2D( "nch_bbarDl2_25_30", false, "nch", "nch(antibjet)/nch(lightjet2)" );
        _s["nch_bbarDl2_25_50"] = bookScatter2D( "nch_bbarDl2_25_50", false, "nch", "nch(antibjet)/nch(lightjet2)" );



        //Simple general observables for UE but for jets as well
        for (unsigned int iSample=0; iSample < Nsamples; ++iSample) {
            _spt[iSample]      = bookHisto1D( "spt_"  + regionStr[iSample] , pt_el_binning);
            _nch[iSample]      = bookHisto1D( "nch_"  + regionStr[iSample] , 200, 0.5, 200.5);
            _mpt[iSample]      = bookHisto1D( "mpt_"  + regionStr[iSample] , mpt_binning);
            _pt[iSample]       = bookHisto1D( "pt_"  + regionStr[iSample] , pt_binning);
            _spt_pt[iSample]   = bookProfile1D( "spt_pt_"  + regionStr[iSample] , pt_el_binning);
            _nch_pt[iSample]   = bookProfile1D( "nch_pt_"  + regionStr[iSample] , pt_el_binning);
            _mpt_pt[iSample]   = bookProfile1D( "mpt_pt_"  + regionStr[iSample] , pt_el_binning);
            _mpt_nch[iSample]  = bookProfile1D( "mpt_nch_"  + regionStr[iSample] , 120, 0.5, 120.5);
            _spt_nch[iSample]  = bookProfile1D( "spt_nch_"  + regionStr[iSample] , 120, 0.5, 120.5);
        }

    //Jet shapes plots
    for (unsigned int iP = 0; iP < Nptcuts ; ++iP) {
        //diferential and integral shapes of bjets and lightjets
        for (unsigned int iR = 0; iR < inAnRange.size() ; ++iR) {
           // _diffBJshape[iR][iP]  = bookHisto1D( "difBJshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]   , 25, 0, 25);
            //normalized sumpt/ptjet
            _diffBJshape[iR][iP]  = bookHisto1D( "difBJshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 25, 0, 25);
            _diffLJshape[iR][iP]  = bookHisto1D( "difLJshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 25, 0, 25);
            //pt distribution
            _diffBJptshape[iR][iP]  = bookHisto1D( "difBJptshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 300, 0, 15);
            _diffLJptshape[iR][iP]  = bookHisto1D( "difLJptshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 300, 0, 15);
            //cumulative sum of normalized sumpt/ptjet
            _intBJshape[iR][iP]   = bookHisto1D( "intBJshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 20, 0, 1);
            _intLJshape[iR][iP]   = bookHisto1D( "intLJshape_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 20, 0, 1);

          //  _diffBJnch[iR][iP]    = bookHisto1D( "difBJnch_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP]  , 20, 0.0, 1.0);
            _diffBJnch[iR][iP]    = bookHisto1D( "difBJnch_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP], 20, 0.0, 1.0);
            _diffLJnch[iR][iP]    = bookHisto1D( "difLJnch_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP], 20, 0.0, 1.0);
            _intBJnch[iR][iP]     = bookHisto1D( "intBJnch_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP], 20, 0.0, 1.0 );
            _intLJnch[iR][iP]     = bookHisto1D( "intLJnch_"  + std::to_string( inAnRange[iR]) + "_" + ptCutStr[iP], 20, 0.0, 1.0 );
        }
        //diferential and integral shapes of UE in cones near jets
        for (unsigned int iR = 0; iR < outAnRange.size() ; ++iR) {
            //_diffUEBJshape[iR][iP]  = bookHisto1D( "difUEBJshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP]   , 50, 0.5, 25.5);
            //sumpt distribution
            _diffUEBJshape[iR][iP]  = bookHisto1D( "difUEBJshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP], pt_binning );
            _diffUELJshape[iR][iP]  = bookHisto1D( "difUELJshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP], pt_binning );
            //pt distribution
            _diffUEBJptshape[iR][iP]  = bookHisto1D( "difUEBJptshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP], pt_binning );
            _diffUELJptshape[iR][iP]  = bookHisto1D( "difUELJptshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP], pt_binning );
            _intUEBJshape[iR][iP]   = bookHisto1D( "intUEBJshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP], pt_binning );
            _intUELJshape[iR][iP]   = bookHisto1D( "intUELJshape_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP], pt_binning );

            _diffUEBJnch[iR][iP]    = bookHisto1D( "difUEBJnch_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP]  , 80, 0.5, 80.5 );
            _diffUELJnch[iR][iP]    = bookHisto1D( "difUELJnch_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP]  , 80, 0.5, 80.5 );
            _intUEBJnch[iR][iP]     = bookHisto1D( "intUEBJnch_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP]  , 80, 0.5, 80.5 );
            _intUELJnch[iR][iP]     = bookHisto1D( "intUELJnch_"  + std::to_string( outAnRange[iR]) + "_" + ptCutStr[iP]  , 80, 0.5, 80.5 );
        }
    }
        _intBJshape_eta  = bookProfile1D( "intBJshape_2_eta", 25, 0, 2.5);
        _intBJshape_pt   = bookProfile1D( "intBJshape_2_pt", pt_el_binning);
        _intLJshape_eta  = bookProfile1D( "intLJshape_2_eta", 25, 0, 2.5);
        _intLJshape_pt   = bookProfile1D( "intLJshape_2_pt", pt_el_binning);

        //Calculate particles between b jet and light jet in rectangular given by eta-phi space
        _blJetsNch_r[0]    = bookProfile1D( "bljets_nch_r", r_binning, "", "r", "<n_{ch}>");
        _blJetsNch_phi[0]  = bookProfile1D( "bljets_nch_phi", 10,0,1, "", "phi", "<n_{ch}>");
        _blJetsMpt_r[0]    = bookProfile1D( "bljets_mpt_r", r_binning, "", "r", "<p_{T}>");
        _blJetsR[0]  	   = bookHisto1D( "bljets_r", r_binning );


        //calculate particles between bjet(from the same top as ljets) and light jet in rectangular given by eta-phi space
        _blJetsNch_r[1]    = bookProfile1D( "samebljets_nch_r", r_binning, "", "r", "<n_{ch}>");
        _blJetsNch_phi[1]  = bookProfile1D( "samebljets_nch_phi", 10 ,0, 1, "", "phi", "<n_{ch}>");
        _blJetsMpt_r[1]    = bookProfile1D( "samebljets_mpt_r", r_binning, "", "r", "<p_{T}>");
        _blJetsR[1]  	   = bookHisto1D( "samebljets_r", r_binning );

    }

    void analyze(const Event& event) {
        // Get the selected objects, using the projections.
        //Q: The connection to projections defined in init are via strings ("neutrinos", ...)?
        _dressedelectrons     = apply<DressedLeptons>(  event, "dressedelectrons").dressedLeptons();
        _vetodressedelectrons = apply<DressedLeptons>(  event, "vetodressedelectrons").dressedLeptons();
        _dressedmuons         = apply<DressedLeptons>(  event, "dressedmuons").dressedLeptons();
        _vetodressedmuons     = apply<DressedLeptons>(  event, "vetodressedmuons").dressedLeptons();
        _neutrinos            = apply<PromptFinalState>(event, "neutrinos").particlesByPt();
        // These are the charged particles (tracks) with pT > 500 MeV
        //Q: difference between apply and applyProjection?
        const ChargedFinalState& charged500 = applyProjection<ChargedFinalState>(event, "CFS500");
        MSG_DEBUG("charged500 size : " << charged500.size() );

        //CHANGE A-S eta < 4.5
        //Q: Cuts are taken with respect to what? To jet as the whole (sum of pT, Eta of the highest particle? Or weighted?)?
        //const Jets& all_jets  = apply<FastJets>(        event, "jets").jetsByPt(Cuts::pT > 25.0*GeV && Cuts::abseta < 2.5);
        const Jets& all_jets  = apply<FastJets>(        event, "jets").jetsByPt(Cuts::pT > 25.0*GeV && Cuts::abseta < 2.5);


        Particles _eventparticles[kNPartTypes];
        _eventparticles[k_AllCharged] = charged500.particlesByPt();
        MSG_DEBUG("particlesAll size : " << _eventparticles[k_AllCharged].size() );


        //-----Selection criteria-------------------
        cut_flow->fill( which_cut( CUT_FLOW::none) );
        //get true l+jets events by removing events with more than 1 electron||muon neutrino
        //CHANGE A-S cut on neutrinos? if expected them to be only the one coming from W decay
        unsigned int n_elmu_neutrinos = 0;
        foreach (const Particle p, _neutrinos) {
            if (p.abspid() == 12 || p.abspid() == 14)  ++n_elmu_neutrinos;
        }
        if (n_elmu_neutrinos != 1)  vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_elmu_neutrions) );

        DressedLepton *lepton;
        //Q: what about the case that there are both dressed el and mu?
        if ( _dressedelectrons.size())  lepton = &_dressedelectrons[0];
        else if (_dressedmuons.size())  lepton = &_dressedmuons[0];
        else vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_elmu) );

        // Calculate the missing ET, using the prompt neutrinos only (really?)
        /// @todo Why not use MissingMomentum?
        FourMomentum met;
        //Q: Should be only one contribution from neutrinos due to the upper cut, right?
        foreach (const Particle& p, _neutrinos)  met += p.momentum();

        //remove jets if they are within dR < 0.2 of lepton
        //Q: Why only vetodressed el?
        //CHANGE: A-S dR < 0.4
        Jets jets;
        foreach(const Jet& jet, all_jets) {
            bool keep = true;
            foreach (const DressedLepton& el, _vetodressedelectrons) {
                keep &= deltaR(jet, el) >= 0.2;
            }
            if (keep)  jets += jet;
        }

        bool overlap = false;
        Jets bjets, lightjets;
        for (unsigned int i = 0; i < jets.size(); ++i) {
            const Jet& jet = jets[i];
            foreach (const DressedLepton& el, _dressedelectrons)  overlap |= deltaR(jet, el) < 0.4;
            foreach (const DressedLepton& mu, _dressedmuons)      overlap |= deltaR(jet, mu) < 0.4;
            //CHANGE: Not requested for A-S analysis
            for (unsigned int j = i + 1; j < jets.size(); ++j) {
                overlap |= deltaR(jet, jets[j]) < 0.5;
            }
            //// Count the number of b-tags
            bool b_tagged = false;           //  This is closer to the
            Particles bTags = jet.bTags();   //  analysis. Something
            foreach ( Particle b, bTags ) {  //  about ghost-associated
                b_tagged |= b.pT() > 5*GeV;    //  B-hadrons
            }                                //
            if ( b_tagged )  bjets += jet;
            else lightjets += jet;
        }

        // remove events with object overlap
        if (overlap) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::overlap) );



        // Evaluate basic event selection
        bool pass_eljets = (_dressedelectrons.size() == 1) &&
                        (_vetodressedelectrons.size() < 2) &&
                        (_vetodressedmuons.empty()) &&
                        //CHANGE: met.pT() >= 35 GeV
                        (met.pT() > 30*GeV) &&
                        //CHANGE: _mT >= 25 GeV
                        (_mT(_dressedelectrons[0].momentum(), met) > 35*GeV); // &&
        //Shouldnt I check rather 2bjets and 2 lightjets?
        //(jets.size() >= 4);
        //   (bjets.size() >= 2) &&
        //   (lightjets.size() >= 2);
        bool pass_mujets = (_dressedmuons.size() == 1) &&
                        (_vetodressedmuons.size() < 2) &&
                        (_vetodressedelectrons.empty()) &&
                        //CHANGE: met.pT() >= 35 GeV
                        (met.pT() > 30*GeV) &&
                        //CHANGE: _mT >= 60 GeV
                        (_mT(_dressedmuons[0].momentum(), met) > 35*GeV); // &&

        //Shouldnt I check rather 2bjets and 2 lightjets?
        //(jets.size() >= 4);
        //   (bjets.size() >= 2) &&
        //   (lightjets.size() >= 2);


        // basic event selection requirements
        //if (!pass_eljets && !pass_mujets) vetoEvent;
        //if( pass_eljets) cut_flow->fill( which_cut( CUT_FLOW::e_phasespace) );
        //if( pass_mujets) cut_flow->fill( which_cut( CUT_FLOW::mu_phasespace) );
        if (!pass_eljets && !pass_mujets) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::emu_phasespace) );

        //@Todo check whether lightjets are order by descending pt?
        //CHeck fill at first highest pt lightjet
        _h["pt_leadinglj"]->fill( (lightjets.size() > 0) ? lightjets[0].pt() : 0 , 1 );
        _h["n_lj"]->fill(  lightjets.size() , 1 );
        foreach( Jet lj, lightjets ) _h["pt_lj"]->fill( lj.pt() , 1 );

        //Presence of at least 2bjets and at least 2lightjets with the presence of exactly one electron or one muon depending on the channel
        //if (bjets.size() < 2 || lightjets.size() < 2)  vetoEvent;
        if (bjets.size() < 2 )  vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_bjets) );
        if (lightjets.size() < 2)  vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_lightjets) );


        //Selecting lightjets from W decay
        double dummy_mass = 20;
        double min_delta_mass = dummy_mass; //default set to large number
        unsigned int position = 0;
        std::pair<unsigned int, unsigned int> ljets_position (0,0);
        //std::cout << "lightjets number: " <<lightjets.size() << std::endl;
        for( auto ljet : lightjets){
            unsigned position2 = position + 1;
            for( unsigned int lj2 = position2; lj2< lightjets.size(); ++lj2 ){
                FourMomentum light1 = ljet.momentum();
                FourMomentum light2 = lightjets[lj2].momentum();
                double delta_mass = abs(w_mass - (light1 + light2).mass());
                // std::cout << w_mass << " - " << ljet.mass() << " - " << lightjets[lj2].mass() << " " <<  (light1 + light2).mass()  <<std::endl;
                // std::cout << "mindelta: " << min_delta_mass << std::endl;
                // std::cout << "delta: " << delta_mass << std::endl;
                if( delta_mass < min_delta_mass ){
                    ljets_position.first = position;
                    ljets_position.second = position2;
                    min_delta_mass = delta_mass;
                }
            }
            ++position;
        }
        //@Todo condition if none of the jet combination is near w_mass?
        if( min_delta_mass == dummy_mass ) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::near_w_mass) );

        //Check fill at first highest pt lightjet
        //@Todo if lightjets are sorted then use
        //_h["pt_selectedlj"]->Fill( lightjets[ljets_position.first].pt() , 1 );
        _h["pt_Wlj"]->fill( (lightjets[ljets_position.first].pt() > lightjets[ljets_position.second].pt() ) ? lightjets[ljets_position.first].pt() :lightjets[ljets_position.second].pt() , 1 );
        //foreach( Jet lj,  lightjets){
        for(unsigned int ilj = 0; ilj < lightjets.size(); ++ilj){
            if( ilj != ljets_position.first && ilj != ljets_position.second ) _h["pt_restlj"]->fill( lightjets[ilj].pt()  , 1 );
        }

//        FourMomentum pseudoLEPbjet; //Momentum of bjet1
//        FourMomentum pseudoHADbjet; //Momentum of bjet2
        //Q: Are jets sorted by pT?
        //Q: Why should I take only first two if there are more?
        //A: A-S take two highest pT jets
        //Q: Assigning bjets, incorrect if ttbar are in rest
        unsigned int iHbj = 0, iLbj = 0;
        if ( deltaR(bjets[0], *lepton) <= deltaR(bjets[1], *lepton) ) {
            pseudoLEPbjet = bjets[0].momentum();
            pseudoHADbjet = bjets[1].momentum();
            iHbj = 1;
            iLbj = 0;
        } else {
            pseudoLEPbjet = bjets[1].momentum();
            pseudoHADbjet = bjets[0].momentum();
            iHbj = 0;
            iLbj = 1;
        }
        assert( iHbj != iLbj );
       // FourMomentum pjet1; // Momentum of jet1
        //Q: Why condition? This should be already covered with vetoEvent above
        //if (lightjets.size())  pjet1 = lightjets[0].momentum();
        if (lightjets.size())  pjet1 = lightjets[ljets_position.first].momentum();

       // FourMomentum pjet2; // Momentum of jet 2
        //Q: Why condition? This should be already covered with vetoEvent above
        //if (lightjets.size() > 1)  pjet2 = lightjets[1].momentum();
        if (lightjets.size() > 1)  pjet2 = lightjets[ljets_position.second].momentum();
      //  _h["lightjetmass"]->fill(  (pjet1 + pjet2).mass() , 1 );
      //  _h["lightjetmass"]->fill(  (pjet1 + pjet2).mass() , 1 );



        double pz = computeneutrinoz(lepton->momentum(), met);
        //Q: What is pseudoneutrino?
        FourMomentum ppseudoneutrino( sqrt(sqr(met.px()) + sqr(met.py()) + sqr(pz)), met.px(), met.py(), pz);

        //compute leptonic, hadronic, combined pseudo-top
        //Q: Why pseudo?
        //Q: A-S Take combination which minimizes ttbar difference, which combi? Only one lepton allowed and requirement on jets with highest pT
        FourMomentum ppseudotoplepton = lepton->momentum() + ppseudoneutrino + pseudoLEPbjet;
        FourMomentum leptonic_w = lepton->momentum() + ppseudoneutrino;
        FourMomentum ppseudotophadron = pseudoHADbjet + pjet1 + pjet2;
        FourMomentum hadronic_w = pjet1 + pjet2;
        FourMomentum pttbar = ppseudotoplepton + ppseudotophadron;

        //@Note why to keep selection after calculation of top?
        // -> Moved up
        const double weight = 1; //event.weight();

    //TTBAR
    _h["mass_ttbar"]->fill( pttbar.mass(), weight);
    _h["eta_ttbar"]->fill(  pttbar.eta(),  weight); //mass of ttbar
    _h["phi_ttbar"]->fill(  pttbar.phi(MINUSPI_PLUSPI)/M_PI,  weight); //mass of ttbar
    _h["dphi_ttbar"]->fill( deltaPhi( ppseudotophadron.phi(), ppseudotoplepton.phi() )/M_PI , weight);

    _h["deta_ttbar"]->fill( deltaEta( ppseudotophadron.eta(), ppseudotoplepton.eta() ) , weight);
    _h2["pt_tVtbar"]->fill( ppseudotophadron.pt() , ppseudotoplepton.pt(),    weight); //correlation of pt between leptonic and hadronic top
    _h2["eta_tVtbar"]->fill( ppseudotophadron.eta() , ppseudotoplepton.eta(),    weight); //correlation of pt between leptonic and hadronic top
    _h2["phi_tVtbar"]->fill( ppseudotophadron.phi()/(2*M_PI) , ppseudotoplepton.phi()/(2*M_PI),    weight); //correlation of pt between leptonic and hadronic top
    //---------------------------------------

    //TOP
    _h["mass_t"]->fill( ppseudotophadron.mass(), weight); //mass of ttbar
    _hmass["mass_t_unnorm"]->fill( ppseudotophadron.mass(), weight); //mass of ttbar
    _h["pt_t"]->fill( ppseudotophadron.pt() ,weight );
    _h["eta_t"]->fill( ppseudotophadron.eta() ,weight );
    _h["phi_t"]->fill( ppseudotophadron.phi(MINUSPI_PLUSPI)/M_PI ,weight );
    //---------------------------------------

    //ANTITOP
    _h["mass_tbar"]->fill( ppseudotoplepton.mass(), weight); //mass of ttbar
    _h["pt_tbar"]->fill( ppseudotoplepton.pt() ,weight );
    _h["eta_tbar"]->fill( ppseudotoplepton.eta() ,weight );
    _h["phi_tbar"]->fill( ppseudotoplepton.phi(MINUSPI_PLUSPI)/M_PI ,weight );
    //---------------------------------------

    //BBBAR
    _h["dr_bbbar"]->fill( deltaR( pseudoHADbjet, pseudoLEPbjet) , weight); //0.02 = bin width normalization
    //---------------------------------------

    //BOTTOM
    _h["eta_b"]->fill( pseudoHADbjet.eta() ,weight );
    _h["phi_b"]->fill( pseudoHADbjet.phi(MINUSPI_PLUSPI)/M_PI ,weight );
    _h["pt_b"]->fill( pseudoHADbjet.pt() , weight );

    //Filling jets pt and nch for specific jetpt regions for all the bjets and lightjets to produce ratio bjet/ljet in an attempt to produce observable sensitive to CR without fragmentation effect.
    if( pseudoHADbjet.pt() < 50*GeV ){
        int nch=0;
        foreach( auto p, bjets[iHbj].particles() ){
            //Take only charged particles
            if( p.charge() == 0) continue;
            _h["pt_b_25_50"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_b_25_50"]->fill( nch, weight );
    }
    if( pseudoHADbjet.pt() < 30*GeV ){
        int nch=0;
        foreach( auto p, bjets[iHbj].particles() ){
            if( p.charge() == 0) continue;
            _h["pt_b_25_30"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_b_25_30"]->fill( nch, weight );
    }
    if( pseudoLEPbjet.pt() < 50*GeV ){
        int nch=0;
        foreach( auto p, bjets[iLbj].particles() ){
            //Take only charged particles
            if( p.charge() == 0) continue;
            _h["pt_bbar_25_50"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_bbar_25_50"]->fill( nch, weight );
    }
    if( pseudoLEPbjet.pt() < 30*GeV ){
        int nch=0;
        foreach( auto p, bjets[iLbj].particles() ){
            if( p.charge() == 0) continue;
            _h["pt_bbar_25_30"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_bbar_25_30"]->fill( nch, weight );
    }
    if( pjet1.pt() < 50*GeV ){
        int nch=0;
        foreach( auto p, lightjets[ljets_position.first].particles() ){
            //Take only charged particles
            if( p.charge() == 0) continue;
            _h["pt_l1_25_50"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_l1_25_50"]->fill( nch, weight );
    }
    if( pjet1.pt() < 30*GeV ){
        int nch=0;
        foreach( auto p, lightjets[ljets_position.first].particles() ){
        //Take only charged particles
            if( p.charge() == 0) continue;
            _h["pt_l1_25_30"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_l1_25_30"]->fill( nch, weight );
    }
    if( pjet2.pt() < 50*GeV ){
        int nch=0;
        foreach( auto p, lightjets[ljets_position.second].particles() ){
        //Take only charged particles
            if( p.charge() == 0) continue;
            _h["pt_l2_25_50"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_l2_25_50"]->fill( nch, weight );
    }
    if( pjet2.pt() < 30*GeV ){
        int nch=0;
        foreach( auto p, lightjets[ljets_position.second].particles() ){
        //Take only charged particles
            if( p.charge() == 0) continue;
            _h["pt_l2_25_30"]->fill( p.pt() , weight );
            ++nch;
        }
            _h["nch_l2_25_30"]->fill( nch, weight );
    }
    //---------------------------------------

    //ANTIBOTTOM
    _h["eta_bbar"]->fill( pseudoLEPbjet.eta() ,weight );
    _h["phi_bbar"]->fill( pseudoLEPbjet.phi(MINUSPI_PLUSPI)/M_PI ,weight );
    _h["pt_bbar"]->fill( pseudoLEPbjet.pt() , weight );
    //---------------------------------------

    //W decaing in LEPTON channel
    _h["mass_Wleptonic"]->fill( leptonic_w.mass(), weight);
    _h["pt_Wleptonic"]->fill( leptonic_w.pt() ,weight );
    //---------------------------------------

    //W decaing in HADRON channel
    _h["mass_Whadronic"]->fill( hadronic_w.mass(), weight);
    _h["pt_Whadronic"]->fill( hadronic_w.pt() ,weight );
    //---------------------------------------

    //LIGHTJETS from W decay
    //HIGHER PT lighjet
    _h["eta_l1"]->fill( pjet1.eta() ,weight );
    _h["phi_l1"]->fill( pjet1.phi(MINUSPI_PLUSPI)/M_PI ,weight );
    _h["pt_l1"]->fill( pjet1.pt() ,weight );
    _h["pt_l2"]->fill( pjet2.pt() ,weight );
    //---------------------------------------

    //LOWER PT lightjet
    _h["eta_l2"]->fill( pjet2.eta() ,weight );
    _h["phi_l2"]->fill( pjet2.phi(MINUSPI_PLUSPI)/M_PI ,weight );
    //---------------------------------------
    _h["dr_lj"]->fill( deltaR( pjet1, pjet2), weight); //0.02 = bin width normalization


    //BOTTOM TTBAR
        _h2["etaphi_bttbar"]->fill(  deltaEta( pttbar.eta(), pseudoHADbjet.eta()) , deltaPhi(pttbar.phi(), pseudoHADbjet.phi())/M_PI,    weight); //correlation of pt between leptonic and hadronic top
    //---------------------------------------

    //ANTIBOTTOM TTBAR
        _h2["etaphi_bbarttbar"]->fill(  deltaEta( pttbar.eta(), pseudoLEPbjet.eta()) , deltaPhi(pttbar.phi(), pseudoLEPbjet.phi())/M_PI,    weight); //correlation of pt between leptonic and hadronic top
    //---------------------------------------

    //HIGHER PT lightjet TTBAR
        _h2["etaphi_l1ttbar"]->fill(  deltaEta( pttbar.eta(), pjet1.eta()) , deltaPhi(pttbar.phi(), pjet1.phi())/M_PI,    weight); //correlation of pt between leptonic and hadronic top
    //---------------------------------------

    //LOWER PT lightjet TTBAR
        _h2["etaphi_l2ttbar"]->fill(  deltaEta( pttbar.eta(), pjet2.eta()) , deltaPhi(pttbar.phi(), pjet2.phi())/M_PI,    weight); //correlation of pt between leptonic and hadronic top
    //---------------------------------------

    //LEPTONS
        //@Todo isEl isMu tag?
        if(pass_eljets){
            _h["pt_e"]->fill( lepton->pt() ,weight );
            _h["eta_e"]->fill( lepton->eta() ,weight );
            _h["phi_e"]->fill( lepton->phi(MINUSPI_PLUSPI)/M_PI ,weight );
        }
        if(pass_mujets){
            _h["pt_mu"]->fill( lepton->pt() ,weight );
            _h["eta_mu"]->fill( lepton->eta() ,weight );
            _h["phi_mu"]->fill( lepton->phi(MINUSPI_PLUSPI)/M_PI ,weight );
        }
        _h["pt_l"]->fill( lepton->pt() ,weight );
    //---------------------------------------
        //Comparison between truth and reco tops
        std::vector<const HepMC::GenParticle*> allParticles = particles(event.genEvent());
        double lepTopMinDeltaR = 999 ;
        double hadTopMinDeltaR = 999 ;
       // double topMinPhi = 999 ;
       // double topMinEta = 999 ;

    Particle Truth_top, Truth_antitop;
    foreach( const HepMC::GenParticle* gp , allParticles){
        //Check that tops are from the hardest subprocess
        //sharp inequality with 29?
        //std::cout << gp->status() << " " ;
        if( abs( gp->status() ) < 21 || abs( gp->status() > 29 ) ) continue;
        //if( (gp->status()  <= -21) ||  (gp->status() > -29)  ) continue;
        //Find truth top and antitop
        Particle p(gp);
        //std::cout << p.pid() << " " ;
        if( p.pid() != 6 && p.pid() != -6 ) continue;
        //std::cout << p.momentum().pt() << " lep " << ppseudotoplepton.pt() << " had "<< ppseudotophadron.pt() << std::endl;
        //Can there be more than one top? In principle yes, find smallest difference in R?

        if(p.pid() == 6 && deltaR( p.momentum(), ppseudotophadron ) < hadTopMinDeltaR){
            Truth_top = p;
            hadTopMinDeltaR = deltaR( p.momentum() , ppseudotophadron );
       // std::cout << " TOP " << std::endl ;
        }
        else if( p.pid() == -6 && deltaR( p.momentum(), ppseudotoplepton ) < lepTopMinDeltaR  ){
            Truth_antitop = p;
            lepTopMinDeltaR = deltaR( p.momentum(), ppseudotoplepton ) ;
       // std::cout << " ANTITOP " << std::endl ;
        }
    }

    //TOP truthVreco correlations
    _h["dr_tTruthReco"]->fill( hadTopMinDeltaR ,weight );
    _h2["pt_ttruVtreco"]->fill( ppseudotophadron.pt(), Truth_top.pt(),   weight);
    _h2["eta_ttruVtreco"]->fill( ppseudotophadron.eta() , Truth_top.eta(),    weight);
    _h2["phi_ttruVtreco"]->fill( ppseudotophadron.phi()/(2*M_PI) , Truth_top.phi()/(2*M_PI),    weight);
    //ANTITOP truthVreco correlations
    _h["dr_tbarTruthReco"]->fill( lepTopMinDeltaR ,weight );
    _h2["pt_tbartruVtbarreco"]->fill( ppseudotoplepton.pt(), Truth_antitop.pt(),   weight);
    _h2["eta_tbartruVtbarreco"]->fill( ppseudotoplepton.eta() , Truth_antitop.eta(),    weight);
    _h2["phi_tbartruVtbarreco"]->fill( ppseudotoplepton.phi()/(2*M_PI) , Truth_antitop.phi()/(2*M_PI),    weight);


        //Study of Differential jet shape from final jets
        //Q:After all cuts? Made out of 2 bjet and 2 light flavor jets?
        //Q: How big should the Delta_r be?
        //A: According to plot in A-S article (Delta_r / Delta_R) = 0.02
        //@Todo: need to store info from one jet and then do average over jets, is the help hist good solution?
        //Important jets for top reconstruction
        Jets FinalJets;
        //Rest of the jets in an event
        Jets RestJets;
        //@Todo indentification of B jets coming from W in a case where there are more than 2 bjets?
        for( unsigned int iJ = 0; iJ < bjets.size(); ++iJ ){
           if( iJ < 2) FinalJets.push_back( bjets[iJ] );
           else RestJets.push_back( bjets[iJ]);
        }
        for( unsigned int iJ = 0; iJ < lightjets.size(); ++iJ ){
            if( iJ != ljets_position.first && iJ != ljets_position.second) RestJets.push_back( lightjets[iJ]);
            else FinalJets.push_back( lightjets[iJ] );
        }


        //Be sure that FinalJets are sorted in a way that bjets are first and lightjets next
        for( unsigned int iJ = 0; iJ < FinalJets.size(); ++iJ){
            //Q: on the y-axis should be d_rho/d_r, normalization for bin width?
            double JetE = FinalJets[iJ].totalEnergy();
            double JetPt = FinalJets[iJ].pt();
            double JetNch = FinalJets[iJ].particles().size();
            CUT_PT cutPT = which_pt_cut( FinalJets[iJ].pt() );
            std::vector<double> diffJetPt( inAnRange.size(), 0);
            std::vector<double> diffJetNch( inAnRange.size(), 0);
            std::vector<double> diffUEPt( outAnRange.size(), 0);
            std::vector<double> diffUENch( outAnRange.size(), 0);
          //  std::cout << "Jet: "  << iJ << " Npart: " << FinalJets[iJ].size() <<std::endl;
            //Study of activity inside jets
           // int iPa = 1;

            foreach( Particle p, FinalJets[iJ].particles() ){
           //    std::cout << "Particle: " << iPa << std::endl; 
             //    ++iPa;
                //Q: move normalization to finalize?

                //Take only charged particles
                if( p.charge() == 0) continue;
                double r = deltaR( p.momentum(), FinalJets[iJ].momentum());
                //Help histogram for storing bin activities
                _h["hhist_jetshape"]->fill( r/Delta_R, weight*p.E()/JetE/0.02); //0.02 = bin width normalization
                //Fill diferential jet shape in the whole dR jet range separately for bjets and lightjets
                //@Todo comparison based on index rather than object?
                if(FinalJets[iJ].momentum() == pseudoHADbjet )_h["diffbjetshape"]->fill( r/Delta_R, weight*p.E()/JetE/0.02); //0.02 = bin width normalization
                else if(FinalJets[iJ].momentum() == pseudoLEPbjet )_h["diffabjetshape"]->fill( r/Delta_R, weight*p.E()/JetE/0.02); //0.02 = bin width normalization
                else if( iJ == 2 )_h["diffl1jetshape"]->fill( r/Delta_R, weight*p.E()/JetE/0.02); //0.02 = bin width normalization
                else if( iJ  == 3 )_h["diffl2jetshape"]->fill( r/Delta_R, weight*p.E()/JetE/0.02); //0.02 = bin width normalization
                //Find in which anulus from the jet axis is particle situated
                int inAnnulus = find_annulus( r, INNER );

                //if out of selected range do not calculate
                if (inAnnulus != -1){
                if(iJ == 0 || iJ == 1){ _diffBJptshape[inAnnulus][cutPT]->fill( p.pt(), weight );
                    _diffBJptshape[inAnnulus][noptcut]->fill( p.pt(), weight ); }

               if(iJ == 2 || iJ == 3){ _diffLJptshape[inAnnulus][cutPT]->fill( p.pt(), weight );
                    _diffLJptshape[inAnnulus][noptcut]->fill( p.pt(), weight );}
   
                diffJetPt[inAnnulus] += p.pt();
                diffJetNch[inAnnulus] += 1;
                }
            }

            //Study of activity outside jets
            foreach( Particle p, _eventparticles[k_AllCharged] ){
                double r = deltaR(p.momentum(), FinalJets[iJ].momentum());
                //Find in which anulus from the jet axis is particle situated
                int outAnnulus = find_annulus( r, OUTER );
                if (outAnnulus != -1){
                if(iJ == 0 || iJ == 1){ _diffUEBJptshape[outAnnulus][cutPT]->fill( p.pt(), weight );
                    _diffUEBJptshape[outAnnulus][cutPT]->fill( p.pt(), weight );}
                if(iJ == 2 || iJ == 3){ _diffUELJptshape[outAnnulus][cutPT]->fill( p.pt(), weight );
                    _diffUELJptshape[outAnnulus][noptcut]->fill( p.pt(), weight );}
                    diffUEPt[outAnnulus] += p.pt();
                    diffUENch[outAnnulus] += 1;
                }
            }

            //Integral shapes represent cumulative sum of differential shapes
            double intJetPt = 0;
            double intJetNch = 0;
            double intUEPt = 0;
            double intUENch = 0;
            for(unsigned int iR = 0; iR < inAnRange.size(); ++iR){
                //integral jet pt is a cumulative sum of differential jet pt
                intJetPt += diffJetPt[iR];
                intJetNch += diffJetNch[iR];
                fill_jetshape( diffJetPt[iR], diffJetNch[iR], intJetPt, intJetNch, JetPt, JetNch, FinalJets[iJ].eta(), iR, iJ, cutPT);
                fill_jetshape( diffJetPt[iR], diffJetNch[iR], intJetPt, intJetNch, JetPt, JetNch, FinalJets[iJ].eta(), iR, iJ, noptcut);
            }

            for(unsigned int iR = 0; iR < outAnRange.size(); ++iR){
                //integral jet pt is a cumulative sum of differential jet pt
                intUEPt += diffUEPt[iR];
                intUENch += diffUENch[iR];

                fill_ueshape( diffUEPt[iR], diffUENch[iR], intUEPt, intUENch, iR, iJ, cutPT);
                fill_ueshape( diffUEPt[iR], diffUENch[iR], intUEPt, intUENch, iR, iJ, noptcut);
            }
            /*
            //Differential jet shape which is averaged over all jets
           // double top_pt =  ppseudotophadron.pt();
            foreach( auto slice, top_pt_slices  ){
                //Fill all top pt
                for(unsigned int iBin = 0; iBin < _h["hhist_jetshape"]->numBins(); ++iBin){
                    _p["diffjetshape"]->fill( _h["hhist_jetshape"]->bins()[iBin].xMid()  ,_h["hhist_jetshape"]->bins()[iBin].area() );
                }
               // if( slice.second < top_pt*GeV ) continue;
               //TODO: nicer way?
               // for(unsigned int iBin = 0; iBin < _h["hhist_jetshape"]->numBins(); ++iBin){
                    //TODO: Correct handling of passing all relevant info
                    //TODO: Make it easier/better/nicer
               //     _p["diffjetshape" + slice.first + "pt"]->fill( _h["hhist_jetshape"]->bins()[iBin].xMid()  ,_h["hhist_jetshape"]->bins()[iBin].area() );
               // }
            }
            */
            for(unsigned int iBin = 0; iBin < _h["hhist_jetshape"]->numBins(); ++iBin){
              _p["diffjetshape"]->fill( _h["hhist_jetshape"]->bins()[iBin].xMid()  ,_h["hhist_jetshape"]->bins()[iBin].area() );
            }
            //Reset of help histogram
            _h["hhist_jetshape"]->reset();
        }



        //Find deltaR minimum between b jets and lightjets
        double minDeltaR = 999;
        //The same but for bjet which is connected to the same top as lightjets
        double minDeltaRcon = 999;
        int ib = 0, mib = 0, mil = 0, milc = 0;
        foreach ( Jet bjet, bjets) {
            int il = 0;
            foreach ( Jet ljet, lightjets) {
                double dR = deltaR(bjet, ljet);
                if( dR < minDeltaR){
                    minDeltaR = dR;
                    mib = ib;
                    mil = il;
                  // if( ib == 0 && deltaR( bjets[ib], ljet ) < minDeltaRcon ){
                   if( ib == 0 && deltaR( pseudoHADbjet, ljet ) < minDeltaRcon ){
                       milc = il;
                       minDeltaRcon = deltaR(pseudoHADbjet, ljet);
                   }
                }
                ++il;
            }
            ++ib;
        }
        _blJetsR[0]->fill( minDeltaR, 1 );
        _blJetsR[1]->fill( minDeltaRcon, 1 );

    //number of particle between objets
    int n_pbo = 0;
    double spt_pbo = 0;
    /*
    foreach(Particle p, _eventparticles[k_AllCharged] ){
        bool ibo = is_between_objects(p, bjets[mib], lightjets[mil]);
        if( ibo ){
            ++n_pbo;
            spt_pbo += p.pt();
        }

    }
    _blJetsNch_r[0]->fill( minDeltaR, n_pbo , weight/minDeltaR );
    _blJetsNch_phi[0]->fill( deltaPhi(bjets[mib], lightjets[mil])/M_PI, n_pbo, weight/minDeltaR);
    _blJetsMpt_r[0] ->fill( minDeltaR, ( n_pbo != 0) ?  spt_pbo/n_pbo : 0 ,weight/minDeltaR);
    //Also for bjet which is connected to the same top as lightjets
    _blJetsNch_r[1]->fill( minDeltaRcon, n_pbo , weight/minDeltaRcon );
    _blJetsNch_phi[1]->fill( deltaPhi(bjets[0], lightjets[milc])/M_PI, n_pbo, weight/minDeltaRcon);
    _blJetsMpt_r[1] ->fill( minDeltaRcon, ( n_pbo != 0) ?  spt_pbo/n_pbo : 0 ,weight/minDeltaRcon);
    */



       //Invariant mass between various system (b jet and lightjets from the same decay or closest bjet to lighjets)
        FourMomentum sbll = pseudoHADbjet + lightjets[milc].momentum() ;
        FourMomentum cbll = bjets[mib].momentum() + lightjets[milc].momentum() ;
        _h["sbll_invmass"]->fill( sbll.mass()  , weight ) ;
        _h["cbll_invmass"]->fill( cbll.mass()  , weight ) ;
       //Lambda measure calculation using closest bjet to lightjets and bjet from the same decay as lightjets
        //double inv_mass = 0, lambda_sbll = 0, lambda_cbll = 0;
        _h2["dr_sbljet"]->fill( deltaR( pseudoHADbjet, pseudoLEPbjet),  minDeltaRcon )  ;
        _h2["dr_cbljet"]->fill( deltaR( pseudoHADbjet, pseudoLEPbjet), minDeltaR ) ;
        _h2["dr_clsmbljet"]->fill( minDeltaR, minDeltaRcon ) ;

    //FourMomentum qqbar = pjet1 + pjet2;
    FourMomentum nomObject = pttbar; //nominal object (ttbar, qqbar, ...)
    //UE observables
    //std::vector< std::vector<double>> test (Nsamples, std::vector<double> (Nsamples, 0) ); //sum of particle momentum
    std::vector<double> spt (Nsamples, 0); //sum of particle momentum
    std::vector<double> nch (Nsamples, 0); //multiplicity
    std::vector<double> mpt (Nsamples, 0); //mean pt
    calculate_basic_observables( spt, nch, _eventparticles[k_AllCharged], FinalJets, RestJets, lepton->constituentLepton(), nomObject, weight );
    //Filling
    for(unsigned int iR = 0; iR < Nsamples; ++iR){
        //std::cout << "Reg: " << iR << " spt: " << spt[iR] << " nch: " << nch[iR] <<  " mpt:"<< mpt[iR] <<std::endl;
        mpt[iR] = (nch[iR] > 0) ? spt[iR]/(double)nch[iR] : 0;
        _spt[iR]->fill( spt[iR], weight);
        _nch[iR]->fill( nch[iR], weight);
        _mpt[iR]->fill( mpt[iR], weight);
        _spt_pt[iR]->fill( nomObject.pt(), weight*spt[iR]);
        _nch_pt[iR]->fill( nomObject.pt(), weight*nch[iR]);
        _mpt_pt[iR]->fill( nomObject.pt(), weight*mpt[iR]);
        _mpt_nch[iR]->fill( nch[iR], weight*mpt[iR]);
        _spt_nch[iR]->fill( nch[iR], weight*spt[iR]);
    }



        //@Jet pull vector to study colour flow
        //@Angles between pullVectors from jets and their jet partner vectors
    // pullVector(bjet1) -> axis(bjet2)
    // pullVector(bjet2) -> axis(bjet1)
    // pullVector(ljet1) -> axis(ljet2)
    // pullVector(ljet2) -> axis(ljet1)        */
     //   for(int iP = 0 ; iP < 2; ++iP){
            assert(FinalJets.size() == 4);
            //For iP = 0
            //pullVector(bjet1) -> vector(bjet2 - bjet1)
            //pullVector(ljet1) -> vector(ljet2 - ljet1)
            //For iP = 1 it is reversed
            // b_pullAngle = calculate_pullAngle( pullVectors[iP].first , pullVectors[iP].second, FinalJets[iP], FinalJets[1-iP]);
            // lj_pullAngle = calculate_pullAngle( pullVectors[iP+2].first , pullVectors[iP+2].second, FinalJets[iP+2], FinalJets[3-iP]);
            //double b_pullAngle = calculate_pullAngle(  FinalJets[iP], FinalJets[1-iP]);
            //double lj_pullAngle = calculate_pullAngle( FinalJets[iP+2], FinalJets[3-iP]);
            double b_pullAngle = calculate_pullAngle(  FinalJets[0], FinalJets[1]);
            double lj_pullAngle = calculate_pullAngle( FinalJets[2], FinalJets[3]);
            double bl1_pullAngle = calculate_pullAngle(  FinalJets[0], FinalJets[2]);
            double bl2_pullAngle = calculate_pullAngle(  FinalJets[0], FinalJets[3]);
            double barbl1_pullAngle = calculate_pullAngle(  FinalJets[1], FinalJets[2]);
            double barbl2_pullAngle = calculate_pullAngle(  FinalJets[1], FinalJets[3]);

            _h["pa_b" ]->fill( b_pullAngle , weight); //fill pT of ttbar in electron channel
            _h["pa_lj"  ]->fill( lj_pullAngle , weight); //fill pT of ttbar in electron channel
            _h["pa_bl1"]->fill( bl1_pullAngle , weight); //fill pT of ttbar in electron channel
            _h["pa_bl2"]->fill( bl2_pullAngle , weight); //fill pT of ttbar in electron channel
            _h["pa_barbl1"]->fill( barbl1_pullAngle , weight); //fill pT of ttbar in electron channel
            _h["pa_barbl2"]->fill( barbl2_pullAngle , weight); //fill pT of ttbar in electron channel

      //  }


    }
    void finalize() {
        // Normalize to cross-section
        const double scalefactor(crossSection() / sumOfWeights());
        for (map<string, Histo1DPtr>::iterator hit = _h.begin(); hit != _h.end(); ++hit) {
            double sf = scalefactor;
            //Q: only hist with uderscore scaled by 0.5 -> referring to single contribution of muons and electrons?
           // if ( (hit->first).find("_") == std::string::npos )  sf *= 0.5;
            scale(hit->second, sf);
        }

        //

        divide(_h["pt_b_25_50"], _h["pt_l1_25_50"], _s["pt_bDl1_25_50"]);
        divide(_h["pt_b_25_30"], _h["pt_l1_25_30"], _s["pt_bDl1_25_30"]);
        divide(_h["pt_b_25_50"], _h["pt_l2_25_50"], _s["pt_bDl2_25_50"]);
        divide(_h["pt_b_25_30"], _h["pt_l2_25_30"], _s["pt_bDl2_25_30"]);
        divide(_h["pt_bbar_25_50"], _h["pt_l1_25_50"], _s["pt_bbarDl1_25_50"]);
        divide(_h["pt_bbar_25_30"], _h["pt_l1_25_30"], _s["pt_bbarDl1_25_30"]);
        divide(_h["pt_bbar_25_50"], _h["pt_l2_25_50"], _s["pt_bbarDl2_25_50"]);
        divide(_h["pt_bbar_25_30"], _h["pt_l2_25_30"], _s["pt_bbarDl2_25_30"]);

        divide(_h["nch_b_25_50"], _h["nch_l1_25_50"], _s["nch_bDl1_25_50"]);
        divide(_h["nch_b_25_30"], _h["nch_l1_25_30"], _s["nch_bDl1_25_30"]);
        divide(_h["nch_b_25_50"], _h["nch_l2_25_50"], _s["nch_bDl2_25_50"]);
        divide(_h["nch_b_25_30"], _h["nch_l2_25_30"], _s["nch_bDl2_25_30"]);
        divide(_h["nch_bbar_25_50"], _h["nch_l1_25_50"], _s["nch_bbarDl1_25_50"]);
        divide(_h["nch_bbar_25_30"], _h["nch_l1_25_30"], _s["nch_bbarDl1_25_30"]);
        divide(_h["nch_bbar_25_50"], _h["nch_l2_25_50"], _s["nch_bbarDl2_25_50"]);
        divide(_h["nch_bbar_25_30"], _h["nch_l2_25_30"], _s["nch_bbarDl2_25_30"]);

    }
private:
    const double Delta_R = 0.4;
    //Q: Calculate the number of highest dif
    //A: dR = sqrt( 5^2 + (2pi)^2 )

   // double lambda_measure(){
   // }


    //Determine if a particle is within a certain shape area (e.g rectangular) where one side is given by a deltaR between two objects,
    //and the other side is set static R = 1, later on parametrization by R?
    //@Todo for now arguments are jets, change to more general object?
    bool is_between_objects(Particle& particle, Jet& bjet, Jet& lightjet){
        double rectA = 1;
        double rectB = deltaR(bjet, lightjet);
        double Rp = deltaR( bjet, particle) ;
        //End if particle is already not within one side of rectengular continue
        if( rectB < Rp  ) return false;

        //Triangle in phi-eta plane given by particle (p), bjet (b), lightjet (l), setting b position as a new origin
        //@Todo check signs
        //Vector3 b( 0.0, 0.0, 0.0);
        Vector3 p( particle.eta() - bjet.eta(), particle.phi() - bjet.phi(), 0.0);
        Vector3 l( lightjet.eta() - bjet.eta(), lightjet.phi() - bjet.phi(), 0.0);
        Vector3 pl(0.0, 0.0, 0.0);
        pl.setX( p.x()*l.x());
        pl.setY( p.y()*l.y());

        //magnitudes of vectors
        //double mb = b.mod();
        double mp = p.mod();
        double ml = l.mod();
        double mpl = pl.mod();
        //The closest distance from p to l vector, in other words triangle height from point p
        double arg = mpl/mp/ml;
        assert( abs(arg) < 1 );
        double h = mp*abs( sin( acos( arg ) ) );
        // Will sin be positive?? Does not have to be, particle can be on the other side thus has negative value
        //std::cout << "rectB:" << rectB  << " Rp:" << Rp << "  Height:"<< h << std::endl;
        if( h < rectA ) return true;
        else return false;
    }

    //Ensure that particle is not from jets with pt higher than 5 GeV but trackjet
    bool inTrackJet(Particle& p, Jets& jets){
        bool inTrJet = true;
        foreach( Jet jet, jets ){
            //trackjets are ok
            if( jet.pt() < 5*GeV ) continue; //TODO useless condition since jet sample is from 25 GeV
            foreach( Particle jet_p, jet.particles() ){
                if( p != jet_p) continue;
                else{
                    inTrJet = false;
                    break;
                }
                //Is it faster to check whether the condition is broken and end the loop or just let it iterate over the whole?
                if(!inTrJet) break;
            }
            if(!inTrJet) break;
        }
        return inTrJet;
    }

    //Check if the particle is from the main event (one of the two main bjets, one of the two main lightjets, selected lepton) or from underlying event
    bool is_UEparticle(Particle& p, Jets& FinalJets, const Particle& lepton){
        bool isUE = true;
        //if( general_particle == *dynamic_cast<Particle*>(lepton) ){
        if( p == lepton ) return false;
        foreach( Jet fj, FinalJets){
            foreach( Particle jet_p, fj.particles() ){
                //@Todo there should be is in jet function
                if( p != jet_p ) continue;
                else{
                    isUE = false;
                }
                if( !isUE ) break;
            }
            if ( !isUE ) break;
        }
        return isUE;
    }

    //Find to which annulus particle contributes
    int find_annulus(const double r, ANNULUS A){
        int whichAnnulus = -1;
        switch(A){
        case INNER:
        {
            double idR = 0.04;
            for( unsigned int iR = 0; iR < inAnRange.size(); ++iR){
                //Need to convert int to double and normalize
                double annulus= inAnRange[iR]/100.0;
                //std::cout << annulus << " - " << r << std::endl;
                if( (r > annulus- idR/2.0) &&  (r < annulus + idR/2.0) ) whichAnnulus = iR;
            }
            break;
        }
        case OUTER:
        {
            double odR = 0.1;
            //Check if particle is not within the annuluses range under study
            if( r > outAnRange.back() + odR ) break;
            //Find given outer annulus
            for( unsigned int iR = 0; iR < outAnRange.size(); ++iR){
                //Need to convert int to double and normalize
                double annulus= outAnRange[iR]/10.0;
                //std::cout << annulus << " - " << r << std::endl;
                if( (r > annulus- odR) &&  (r < annulus + odR) ) whichAnnulus = iR;
            }
            break;
        }
        }
        //if outside of range do not calculate
        //assert(whichAnnulus != -1);
        return whichAnnulus;
    }

    //Calculate UE observables spt,nch in each region/sample
    void calculate_basic_observables( std::vector<double>& spt, std::vector<double>& nch, Particles& particles, Jets& FinalJets, Jets& RestJets, const Particle& lepton, FourMomentum& nomObject, double weight ){
        //UE observables inside jets
        foreach(Jet jet, FinalJets ){
            foreach(Particle p, jet.particles() ){
                //take only charged particles
                if( p.charge() == 0) continue;
                if( jet.momentum() == pseudoHADbjet ){
                    spt[PseudoHadronic_bjet] += p.pt();
                    nch[PseudoHadronic_bjet] += 1;
                    _pt[PseudoHadronic_bjet]->fill( p.pt(), weight );
                }
                else if( jet.momentum() == pseudoLEPbjet ){
                    spt[PseudoLeptonic_bjet] += p.pt();
                    nch[PseudoLeptonic_bjet] += 1;
                    _pt[PseudoLeptonic_bjet]->fill( p.pt(), weight );
                }
                else if( jet.momentum() == pjet1 ){
                    spt[Lightjet1] += p.pt();
                    nch[Lightjet1] += 1;
                    _pt[Lightjet1]->fill( p.pt(), weight );
                }
                else if( jet.momentum() == pjet2 ){
                    spt[Lightjet2] += p.pt();
                    nch[Lightjet2] += 1;
                    _pt[Lightjet2]->fill( p.pt(), weight );
                }
            }
        }

        //std::vector<double> deltaR_cut = { 1., 2.5, 4.2 };
        foreach(Particle p, particles){
            //Exclusion on main jets and lepton
            if( !is_UEparticle( p, FinalJets, lepton ) ) continue;
            //exclude particles that are in jets with pt > 5GeV
            if( inTrackJet(p, RestJets) ){
                spt[UE_notrackjets] += p.pt();
                nch[UE_notrackjets] += 1;
                _pt[UE_notrackjets]->fill( p.pt(), weight );
                //  SumPt[0][column] += p.pt();
                //  Nch[0][column] += 1;
                // _h["pt"]->fill( p.pt(),weight);
                //  _crpt[0][column]->fill( p.pt(),weight);
            }
            //Cross check - inclusive particles
            spt[UE_inclusive] += p.pt();
            nch[UE_inclusive] += 1;
            _pt[UE_inclusive]->fill( p.pt(), weight );

            //determine region
            Particle_samples regID = select_region(p, nomObject);
            spt[rAll] += p.pt();
            nch[rAll] += 1;
            _pt[rAll]->fill( p.pt(), weight );
           // std::cout << "particle ID " << regID << std::endl;
            switch(regID){
            case rToward:
                spt[rToward] += p.pt();
                nch[rToward] += 1;
                _pt[rToward]->fill( p.pt(), weight );
           //     std::cout << "Toward" << std::endl;
                break;
            case rTransverseMin:
                spt[rTransverseMin] += p.pt();
                nch[rTransverseMin] += 1;
                _pt[rTransverseMin]->fill( p.pt(), weight );
                spt[rTransverse] += p.pt();
                nch[rTransverse] += 1;
                _pt[rTransverse]->fill( p.pt(), weight );
            //    std::cout << "TransverseMin" << std::endl;
                break;
            case rTransverseMax:
                spt[rTransverseMax] += p.pt();
                nch[rTransverseMax] += 1;
                _pt[rTransverseMax]->fill( p.pt(), weight );
                spt[rTransverse] += p.pt();
                nch[rTransverse] += 1;
                _pt[rTransverse]->fill( p.pt(), weight );
              //  std::cout << "TransverseMax" << std::endl;
                break;
            case rAway:
                spt[rAway] += p.pt();
                nch[rAway] += 1;
                _pt[rAway]->fill( p.pt(), weight );
              //  std::cout << "Away" << std::endl;
                break;
            default:
                std::cout << "Error" << std::endl;
            }
        }
        //Need to determine max and min region and swap if necessary because so far they have been treated as left right
        int which_max =  (spt[rTransverseMin] < spt[rTransverseMax] ) ? rTransverseMax : rTransverseMin;
        if( which_max == rTransverseMin ){
            double spt_tmp = spt[rTransverseMin];
            int nch_tmp = nch[rTransverseMin];
            spt[rTransverseMin] = spt[rTransverseMax];
            nch[rTransverseMin] = nch[rTransverseMax];
            spt[rTransverseMax] = spt_tmp;
            nch[rTransverseMax] = nch_tmp;
        }
    }


    //Calculate a pullangle between pull vector and vector connecting jet from which the pull was constructed and colour partner jet
    double calculate_pullAngle(Jet& jet1, Jet& jet2){
        Vector3 pull_vector = calculate_pullVector(jet1);
        //Q: Why multiplied by 1000? GeV2MeV conversion??
        pull_vector = Vector3(1000.*pull_vector.x(), 1000.*pull_vector.y(), 0.);
        //Calculating vector between jet2 and jet1 axis
        double drap = jet2.rap() - jet1.rap();
        //Q: Pullvector has phi between MINUSPI_PLUSPI, why not to convert dphi to the same interval??
        double dphi = jet2.phi(MINUSPI_PLUSPI) - jet1.phi(MINUSPI_PLUSPI);
        Vector3 j2_min_j1(drap, dphi, 0.0);
        //Angle between pullVector and vector between j2 and j1
        return mapAngleMPiToPi(deltaPhi(pull_vector, j2_min_j1));
    }

    /*
    double calculate_pullAngle(double pVector_rap, double pVector_phi, Jet jet1, Jet jet2){
       //both must be defined
       assert(pVector_rap);
       assert(pVector_phi);

    double r1 = sqrt( pow(pVector_rap, 2) + pow(pVector_phi, 2) );
    double jVector_rap = jet2.rapidity() - jet1.rapidity();
       double jVector_phi =  jet2.phi() - jet1.phi();
    double r2 = sqrt( pow( jVector_rap , 2) + pow( jVector_phi, 2) );
    //Check validity of acos domain
    if( abs(pVector_rap/r1) > 1 ) std::cout << "r1: " << r1 << " phi:" << pVector_phi << " rap:" << pVector_rap << std::endl;
    assert( abs(pVector_rap/r1) <= 1 );
    assert( abs(jVector_rap/r2) <= 1 );
    //Check validity of acos range
    assert( acos(pVector_rap/r1) >= 0 && acos(pVector_rap/r1) <= M_PI  );
    assert( acos(jVector_rap/r2) >= 0 && acos(jVector_rap/r2) <= M_PI  );
    double pangle = acos( pVector_rap/r1 ) - acos(jVector_rap/r2);
    assert( abs(pangle) < M_PI);
       return pangle;
    }
    */
    Vector3 calculate_pullVector(Jet& iJet){
        double JetPt = iJet.pt();
        FourMomentum JetAxis;
        Vector3 pullVector(0.0, 0.0, 0.0);
        //For now all particles (only charge can be studied as well)
        //Compute jetaxis from pT of all particles
        //Isnt this obtained by. iJet.phi, iJet.rap -> This is the case for only charged
        //For all is the same as iJet.pt
        foreach( Particle iPar, iJet.particles() ) JetAxis += iPar.momentum();
        Vector3 J( JetAxis.rap(), JetAxis.phi(MINUSPI_PLUSPI), 0.0);
        foreach( Particle iPar, iJet.particles() ){
            Vector3 r_i = Vector3(iPar.rap(), iPar.phi(MINUSPI_PLUSPI), 0.0) - J;
            //Q: Why while?? Is it really more than once?
            while (r_i.y() >  Rivet::PI) r_i.setY(r_i.y() - Rivet::TWOPI);
            while (r_i.y() < -Rivet::PI) r_i.setY(r_i.y() + Rivet::TWOPI);
            pullVector.setX(pullVector.x() + (r_i.mod() * r_i.x() * iPar.pT()) / JetPt);
            pullVector.setY(pullVector.y() + (r_i.mod() * r_i.y() * iPar.pT()) / JetPt);
        }
        return pullVector;
    }

    /// Set an integer region flag on the object, indicating that it's in the toward, transL, transR, or away region respectively
    Particle_samples select_region(const Particle& particle, const FourMomentum& nominalObject ) {
        //@Rivet does not provide deltaPhi()function in other ranges than 0-pi?
        double dphi = particle.phi() - nominalObject.phi(); //phi() by default in 0-2pi region
        //convert dphi to (-pi, pi) interval
        if(dphi > M_PI ) dphi -= 2*M_PI;
        else if(dphi < - M_PI) dphi += 2*M_PI;
        const double adphi = fabs(dphi);
        assert(adphi <= M_PI);

        // Identify region
        //regionID reg = Nregions;
        Particle_samples reg = Nsamples;
        if (adphi < TO_ANGLE) {
            reg = rToward; //< towards
        } else if (adphi >= TO_ANGLE && adphi < AW_ANGLE) {
            //@note!!! Here is the selection to min and max missleading
            //It is rather selected to transverseleft and transverseright
            //But to keep the same numbering as in regionID min max are used
            reg = (dphi < 0) ? rTransverseMin : rTransverseMax; //< trans (L/R)
        } else if (adphi >= AW_ANGLE) {
            reg = rAway; //< away
        }
        //std::cout << "DeltaPhi: " << dphi << " Reg: "<< reg << std::endl;
        //std::cout << "ParticlePhi: " << particle.phi() << " ttbarphi: "<< ttbar.phi() << " DeltaPhi: " << dphi << " Reg: "<< reg << std::endl;
        assert(reg != Nsamples);
        return reg;
    }
//@Todo pass Enum argument so it does have to be computed every time
   void fill_jetshape(double diffJetPt, double diffJetNch, double intJetPt, double intJetNch, double JetPt, double JetNch, double JetEta, unsigned int iR, unsigned int iJ, CUT_PT cutPT){
                //Fill bjets, ! @Todo potential problem when bjets are not first in Final
                    if( iJ == 0 || iJ == 1 ){
                        //@Add Normalization on average of b/ljets by 2?
                        _diffBJshape[iR][cutPT]->fill( diffJetPt/0.04/JetPt, 1);
                        _intBJshape[iR][cutPT]->fill( intJetPt/JetPt, 1 );
                        _diffBJnch[iR][cutPT]->fill( diffJetNch/JetNch, 1);
                        _intBJnch[iR][cutPT]->fill( intJetNch/JetNch, 1 );
                        //The most potential leakage at the end of jet
                        //@Note fill is called twice in the code, nonptcut condition assures that hist bellow is filled only for noptcut
                        if( inAnRange[iR] == 34 && cutPT == noptcut ){
                        _intBJshape_eta->fill( JetEta, 1 - intJetPt/JetPt, 1);
                        _intBJshape_pt->fill( JetPt, 1 - intJetPt/JetPt, 1);
                       }
                    }
               //Fill ligtjets, @Todo potentional problem ...
                    else if( iJ == 2 || iJ == 3 ){
                      //  std::cout<< " I " << iR <<": " << diffJetPt[iR] << " - " << intJetPt << " - "<< diffJetPt[iR]/JetPt/0.04  << std::endl;
                        _diffLJshape[iR][cutPT]->fill( diffJetPt/JetPt/0.04, 1);
                        _intLJshape[iR][cutPT]->fill( intJetPt/JetPt, 1 );
                        _diffLJnch[iR][cutPT]->fill( diffJetNch/JetNch, 1);
                        _intLJnch[iR][cutPT]->fill( intJetNch/JetNch, 1 );
                        //The most potential leakage at the end of jet
                        //@Note fill is called twice in the code, nonptcut condition assures that hist bellow is filled only for noptcut
                        if( inAnRange[iR] == 34 && cutPT == noptcut){
                        _intLJshape_eta->fill( JetEta, 1 - intJetPt/JetPt, 1);
                        _intLJshape_pt->fill( JetPt, 1 - intJetPt/JetPt, 1);
                        }
                    }
    }

   void fill_ueshape(double diffUEPt, double diffUENch, double intUEPt, double intUENch, unsigned int iR, unsigned int iJ, CUT_PT cutPT){
                //Fill bjets, ! @Todo potential problem when bjets are not first in Final
                    if( iJ == 0 || iJ == 1 ){
                        //@Add Normalization on average of b/ljets by 2?
                        _diffUEBJshape[iR][cutPT]->fill( diffUEPt, 1); //0.2 norm?
                        _intUEBJshape[iR][cutPT]->fill( intUEPt, 1 );
                        _diffUEBJnch[iR][cutPT]->fill( diffUENch, 1);
                        _intUEBJnch[iR][cutPT]->fill( intUENch, 1 );
                    }
               //Fill ligtjets, @Todo potentional problem ...
                    else if( iJ == 2 || iJ == 3 ){
                      //  std::cout<< " I " << iR <<": " << diffUEPt[iR] << " - " << intUEPt << " - "<< diffUEPt[iR]/UEPt/0.04  << std::endl;
                        _diffUELJshape[iR][cutPT]->fill( diffUEPt, 1); //0.2 norm?
                        _intUELJshape[iR][cutPT]->fill( intUEPt, 1 );
                        _diffUELJnch[iR][cutPT]->fill( diffUENch, 1);
                        _intUELJnch[iR][cutPT]->fill( intUENch, 1 );
                    }
    }



    double computeneutrinoz(const FourMomentum& lepton, FourMomentum& met) const {
        //computing z component of neutrino momentum given lepton and met
        double pzneutrino;
        double m_W = 80.399; // in GeV, given in the paper
        //Q: Check the computation!!
        double k = (( sqr( m_W ) - sqr( lepton.mass() ) ) / 2 ) + (lepton.px() * met.px() + lepton.py() * met.py());
        double a = sqr ( lepton.E() )- sqr ( lepton.pz() );
        double b = -2*k*lepton.pz();
        double c = sqr( lepton.E() ) * sqr( met.pT() ) - sqr( k );
        double discriminant = sqr(b) - 4 * a * c;
        double quad[2] = { (- b - sqrt(discriminant)) / (2 * a), (- b + sqrt(discriminant)) / (2 * a) }; //two possible quadratic solns
        if (discriminant < 0)  pzneutrino = - b / (2 * a); //if the discriminant is negative
        else { //if the discriminant is greater than or equal to zero, take the soln with smallest absolute value
            double absquad[2];
            for (int n=0; n<2; ++n)  absquad[n] = fabs(quad[n]);
            if (absquad[0] < absquad[1])  pzneutrino = quad[0];
            else                          pzneutrino = quad[1];
        }
        if ( !std::isfinite(pzneutrino) )  std::cout << "Found non-finite value" << std::endl;
        return pzneutrino;
    }

    double _mT(const FourMomentum &l, FourMomentum &nu) const {
        return sqrt( 2 * l.pT() * nu.pT() * (1 - cos(deltaPhi(l, nu))) );
    }

    /// @name Objects that are used by the event selection decisions
    vector<DressedLepton> _dressedelectrons, _vetodressedelectrons, _dressedmuons, _vetodressedmuons;
    Particles _neutrinos;

    //TODO: convert maps to arrays?

    map<string, Histo1DPtr> _h;
    map<string, Scatter2DPtr> _s;
    map<string, Histo1DPtr> _hmass; //unnormalized
    map<string, Profile1DPtr> _p;
    map<string, Histo2DPtr> _h2;
    Histo1DPtr cut_flow;
    Scatter2DPtr test;


 /*   Histo1DPtr _crspt[2][5];
    Histo1DPtr _crnch[2][5];
    Histo1DPtr _crpt[2][5];
    Profile1DPtr _crspt_nch[2][5];
    Profile1DPtr _crmpt_nch[2][5];
*/
    Histo1DPtr _pt[Nsamples];
    Histo1DPtr _spt[Nsamples];
    Histo1DPtr _nch[Nsamples];
    Histo1DPtr _mpt[Nsamples];




    Profile1DPtr _spt_pt[Nsamples];
    Profile1DPtr _nch_pt[Nsamples];
    Profile1DPtr _mpt_pt[Nsamples];
    Profile1DPtr _mpt_nch[Nsamples];
    Profile1DPtr _spt_nch[Nsamples];


    Histo1DPtr _diffBJshape[NiAn][Nptcuts];
    Histo1DPtr _diffBJptshape[NiAn][Nptcuts];
    Histo1DPtr _intBJshape[NiAn][Nptcuts];
    Histo1DPtr _diffLJshape[NiAn][Nptcuts];
    Histo1DPtr _diffLJptshape[NiAn][Nptcuts];
    Histo1DPtr _intLJshape[NiAn][Nptcuts];

    //Diferential jet multiplicities
    Histo1DPtr _diffBJnch[NiAn][Nptcuts];
    Histo1DPtr _intBJnch[NiAn][Nptcuts];
    Histo1DPtr _diffLJnch[NiAn][Nptcuts];
    Histo1DPtr _intLJnch[NiAn][Nptcuts];


    //UE shapes
    Histo1DPtr _diffUEBJshape[NoAn][Nptcuts];
    Histo1DPtr _diffUEBJptshape[NoAn][Nptcuts];
    Histo1DPtr _intUEBJshape[NoAn][Nptcuts];
    Histo1DPtr _diffUELJshape[NoAn][Nptcuts];
    Histo1DPtr _diffUELJptshape[NoAn][Nptcuts];
    Histo1DPtr _intUELJshape[NoAn][Nptcuts];

    Histo1DPtr _diffUEBJnch[NoAn][Nptcuts];
    Histo1DPtr _intUEBJnch[NoAn][Nptcuts];
    Histo1DPtr _diffUELJnch[NoAn][Nptcuts];
    Histo1DPtr _intUELJnch[NoAn][Nptcuts];


    Profile1DPtr _intBJshape_eta;
    Profile1DPtr _intBJshape_pt;
    Profile1DPtr _intLJshape_eta;
    Profile1DPtr _intLJshape_pt;
    Profile1DPtr _blJetsNch_r[2];
    Histo1DPtr _blJetsR[2];
    Profile1DPtr _blJetsNch_phi[2];
    Profile1DPtr _blJetsMpt_r[2];


    //Profile1DPtr _hist_ptavg_toppt [kNPartTypes][kNregions][NdR];
    //Profile1DPtr _hist_ptavg_nch [kNPartTypes][kNregions][NdR];



    std::vector<double> ue_ptbinning(){
        std::vector<double> binning; binning.reserve(16);
        for(unsigned int i=0; i <= 100;++i) binning.push_back(i);
        //for(unsigned int i=0; i <= 15;++i) binning.push_back( 25 + i*5);
        /*
    std::vector<double> binning; binning.reserve(27);
    for(unsigned int i=0; i <= 14;++i) binning.push_back( 1 + i*0.5);
    for(unsigned int i=1; i <= 6;++i) binning.push_back( 8 + i);
    for(unsigned int i=1; i <= 3;++i) binning.push_back(14 + i*2);
    for(unsigned int i=1; i <= 2;++i) binning.push_back(20 + i*5);
    for(unsigned int i=1; i <= 1;++i) binning.push_back(30 + i*20); //loop because of often changes in last bins
    */

        return binning;
    }


};
//Number of selected annuluses under study
//const int ATLAS_2016_UETop::NselAnn = 5;
//const int ATLAS_2016_UETop::NiAn = 9;

//@Todo why to anitialize here? could be done at the beggining
//const string ATLAS_2016_UETop::partTypeStr[] = {"allCharged", "noStrange"};
const string ATLAS_2016_UETop::ptCutStr[] = {"none", "25_50GeV", "50_100GeV", "100_hGeV"};
const string ATLAS_2016_UETop::regionStr  [] = {"hb", "lb", "l1", "l2", "inc", "ntrk", "all", "to", "trmin", "trmax", "tr", "aw"};
const int    ATLAS_2016_UETop::nBin_nch     = 50;
const double    ATLAS_2016_UETop::Bin_nchMin = -0.5;
const double    ATLAS_2016_UETop::Bin_nchMax =  100;
const double    ATLAS_2016_UETop::w_mass = 80.385;
// Angular region definition: TR_SIZE is half the angle of the total transverse region angle
const double ATLAS_2016_UETop::TR_SIDE_SIZE = M_PI/3.0; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
const double ATLAS_2016_UETop::TR_SIZE = 2*TR_SIDE_SIZE; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
const double ATLAS_2016_UETop::TO_ANGLE = M_PI/2.0 - TR_SIDE_SIZE/2.0; //< dphi at which TO region ends and TR begins
const double ATLAS_2016_UETop::TO_SIZE = 2*TO_ANGLE; // total toward size
const double ATLAS_2016_UETop::AW_ANGLE = M_PI/2.0 + TR_SIDE_SIZE/2.0; //< dphi at which TR region ends and AW begins
const double ATLAS_2016_UETop::AW_SIZE = 2*(M_PI - AW_ANGLE); // total away size


//const  int    ATLAS_2016_UETop::;


// The hook for the plugin system
DECLARE_RIVET_PLUGIN(ATLAS_2016_UETop);

}



