#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
namespace Rivet {


class ATLAS_2016_Top_dilepton : public Analysis {
public:

    DEFAULT_RIVET_ANA_CONSTRUCTOR(ATLAS_2016_Top_dilepton);



    enum CUT_FLOW{
        none,
        jet_overlap,
        charge,
        n_leptons,
        n_emu,
        n_mumu,
        n_ee,
        n_bjets,
        near_w_mass
    };

    enum CHANNEL{
        all,
        ee,
        emu,
        mumu,
        nchannels
    };


    enum Particle_samples{
        bjet,
        bjet2,
        rAll,
        rToward,
        rTransverseMin,
        rTransverseMax,
        rTransverse,
        rAway,
        Nsamples
    };


    const static string channelStr  [nchannels];

    const static string regionStr  [Nsamples];
    // Angular region definition: TR_SIZE is half the angle of the total transverse region angle
    const static double TR_SIDE_SIZE;// = M_PI/3.0; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
    const static double TR_SIZE;// = 2*TR_SIDE_SIZE; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
    const static double TO_ANGLE;// = M_PI/2.0 - TR_SIDE_SIZE/2.0; //< dphi at which TO region ends and TR begins
    const static double TO_SIZE;// = 2*TO_ANGLE; // total toward size
    const static double AW_ANGLE;// = M_PI/2.0 + TR_SIDE_SIZE/2.0; //< dphi at which TR region ends and AW begins
    const static double AW_SIZE;// = 2*(M_PI - AW_ANGLE); // total away size

    int which_cut(CUT_FLOW cut){
        std::map<CUT_FLOW, int> CF;
        CF[CUT_FLOW::none] = 1;
        CF[CUT_FLOW::jet_overlap] = 2;
        // CF[CUT_FLOW::charge] = 3;
        CF[CUT_FLOW::n_leptons] = 3;
        CF[CUT_FLOW::n_bjets] = 4;
        CF[CUT_FLOW::near_w_mass] = 5;
        for(auto c : CF){
            if( cut == c.first ) return c.second;
        }
        //if not find among CUT_FLOW return underflow bin
        return -1;
    }

    void init() {
        // Eta ranges
        Cut eta_full = Cuts::abseta < 5.0 && Cuts::pT >= 1.0*MeV;
        // Lepton cuts
        Cut lep_cuts = Cuts::abseta < 2.5 && Cuts::pT >= 25.0*GeV;

        // All final state particles
        FinalState fs(eta_full);

        // Get photons to dress leptons
        IdentifiedFinalState photons(fs);
        photons.acceptIdPair(PID::PHOTON);

        // Projection to find the electrons
        IdentifiedFinalState el_id(fs);
        el_id.acceptIdPair(PID::ELECTRON);
        PromptFinalState electrons(el_id);
        electrons.acceptTauDecays(true);
        DressedLeptons dressedelectrons(photons, electrons, 0.1, lep_cuts, true, true);
        addProjection(dressedelectrons, "DressedElectrons");

        // Projection to find the muons
        IdentifiedFinalState mu_id(fs);
        mu_id.acceptIdPair(PID::MUON);
        PromptFinalState muons(mu_id);
        muons.acceptTauDecays(true);
        DressedLeptons dressedmuons(photons, muons, 0.1, lep_cuts, true, true);
        addProjection(dressedmuons, "DressedMuons");


        IdentifiedFinalState nu_id;
        nu_id.acceptNeutrinos();
        PromptFinalState neutrinos(nu_id);


        // Projection to find jets

        DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
        declare(ewdressedelectrons, "ewdressedelectrons");
        DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
        declare(ewdressedmuons, "ewdressedmuons");

        VetoedFinalState vfs;
        //state veto on certain particles inside jets
        vfs.addVetoOnThisFinalState(ewdressedelectrons);
        vfs.addVetoOnThisFinalState(ewdressedmuons);
        vfs.addVetoOnThisFinalState(neutrinos);
        FastJets jets(vfs, FastJets::ANTIKT, 0.4);
        addProjection(jets, "jets");



        //Projection to find charged particles
        const ChargedFinalState cfs500(-2.5, 2.5, 500*MeV);
        addProjection(cfs500, "CFS500");









        //BINNING

        vector<double> pt_el_binning;//[37];
        vector<double> pt_binning;//[37];
        vector<double> mpt_binning;//[37];



        //el pt binning = mu pt binning
        for(unsigned int i = 0; i<=30 ; ++i){
            //pt_el_binning[i] = 5*i;
            pt_el_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=4 ; ++i){
            //pt_el_binning[i+31] = 160 + 10*i;
            pt_el_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=1 ; ++i){
            //pt_el_binning[i+36] = 250 + 50*i;
            pt_el_binning.push_back(250 + 50*i);
        }


        for(double i = 0.5 ; i <=15 ; i=i+0.5){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }
        for(double i = 16 ; i <=30 ; i=i+1.0){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }

        for(double i = 32 ; i <= 60 ; i=i+2.0){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }
        for(double i = 64 ; i <= 100 ; i=i+4.0){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_binning.push_back( i );
        }

        for(double i = 0 ; i <=5 ; i=i+0.1){
            //pt_lj_binning[i+36] = 250 + 50*i;
            mpt_binning.push_back( i );
        }
        for(unsigned int i = 6 ; i<=70 ; ++i){
            mpt_binning.push_back( i );
        }


        //BOOKING
        //  _hist = bookHisto1D("Passed_events", 1, 0, 1);
        cut_flow = bookHisto1D( "cut_flow", 10, -0.5, 9.5, "CUT FLOW", "individual cuts", "entries" );

        for (unsigned int iCh=0; iCh < nchannels; ++iCh) {
            //    _h["top_mass_" + channelStr[iCh] ]       = bookHisto1D( "ttbarmass_" + channelStr[iCh] , 40, 200, 1000, "", "m_{top}", "ent"  );
            _h["bl_mass_" + channelStr[iCh] ]       = bookHisto1D( "blmass_" + channelStr[iCh] , 720, 25, 205, "", "m_{lb}", "ent"  );
            _h["bal_mass_" + channelStr[iCh] ]       = bookHisto1D( "blmass_" + channelStr[iCh] ,  720, 25, 205,  "", "m_{lb}", "ent"  );


            //Simple general observables for UE but for jets as well
            for (unsigned int iSample=0; iSample < Nsamples; ++iSample) {
                int index = iSample + Nsamples*iCh;
                _spt[index]      = bookHisto1D( "spt_"  + regionStr[iSample] + "_" + channelStr[iCh] , pt_el_binning);
                _nch[index]      = bookHisto1D( "nch_"  + regionStr[iSample] + "_" + channelStr[iCh] , 200, 0.5, 200.5);
                _mpt[index]      = bookHisto1D( "mpt_"  + regionStr[iSample] + "_" + channelStr[iCh] , mpt_binning);
                _pt[index]       = bookHisto1D( "pt_"  + regionStr[iSample] + "_" + channelStr[iCh] , pt_binning);
                _spt_pt[index]   = bookProfile1D( "spt_pt_"  + regionStr[iSample] + "_" + channelStr[iCh] , pt_el_binning);
                _nch_pt[index]   = bookProfile1D( "nch_pt_"  + regionStr[iSample] + "_" + channelStr[iCh] , pt_el_binning);
                _mpt_pt[index]   = bookProfile1D( "mpt_pt_"  + regionStr[iSample] + "_" + channelStr[iCh] , pt_el_binning);
                _mpt_nch[index]  = bookProfile1D( "mpt_nch_"  + regionStr[iSample] + "_" + channelStr[iCh] , 120, 0.5, 120.5);
                _spt_nch[index]  = bookProfile1D( "spt_nch_"  + regionStr[iSample] + "_" + channelStr[iCh] , 120, 0.5, 120.5);
            }




        }
    }


    void analyze(const Event& event) {

        // Get the selected objects, using the projections.
        _dres_el = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();
        _dres_mu = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
        //  _neutrinos = apply<PromptFinalState>(event, "neutrinos").particlesByPt();
        const Jets& all_jets  = apply<FastJets>( event, "jets").jetsByPt(Cuts::pT > 25.0*GeV && Cuts::abseta < 2.5);

        const ChargedFinalState& charged500 = applyProjection<ChargedFinalState>(event, "CFS500");
        MSG_DEBUG("charged500 size : " << charged500.size() );
        Particles _eventparticles;
        _eventparticles = charged500.particlesByPt();
        MSG_DEBUG("particlesAll size : " << _eventparticles.size() );

        cut_flow->fill( which_cut( CUT_FLOW::none) );

        //SELECTION CRITERIA
        //electron jet overlap
        Jets jets;
        foreach(const Jet& jet, all_jets) {
            bool keep = true;
            foreach (const DressedLepton& el, _dres_el) {
                keep &= deltaR(jet, el) >= 0.2;
            }
            if (keep)  jets += jet;
        }

        Jets bjets, lightjets;
        vector<DressedLepton> sel_electrons, sel_muons;
        foreach (const DressedLepton& el, _dres_el){
            bool el_overlap = false;
            foreach(const Jet& jet, jets) el_overlap |= deltaR(jet, el) < 0.4;
            if(!el_overlap) sel_electrons.push_back( el );
        }
        foreach (const DressedLepton& mu, _dres_mu){
            bool mu_overlap = false;
            foreach(const Jet& jet, jets) mu_overlap |= deltaR(jet, mu) < 0.4;
            if(!mu_overlap) sel_muons.push_back( mu );
        }

        bool jet_overlap = false;
        for(unsigned int i = 0; i < jets.size(); ++i) {
            const Jet& jet = jets[i];
            //is jet overlap necessary?
            for (unsigned int j = i + 1; j < jets.size(); ++j) {
                jet_overlap |= deltaR(jet, jets[j]) < 0.5;
            }
            //// Count the number of b-tags
            bool b_tagged = false;           //  This is closer to the
            Particles bTags = jet.bTags();   //  analysis. Something
            foreach ( Particle b, bTags ) {  //  about ghost-associated
                b_tagged |= b.pT() > 5*GeV;    //  B-hadrons
            }                                //
            if ( b_tagged )  bjets += jet;
            else lightjets += jet;
        }

        if (jet_overlap) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::jet_overlap) );
        unsigned int n_el = sel_electrons.size();
        unsigned int n_mu = sel_muons.size();
        //bool two_leptons = false;
        CHANNEL Channel;
        DressedLepton* lepton;
        DressedLepton* antilepton;


        //Electron-muon channel
        if( n_el == 1 && n_mu == 1 ){
            Channel = emu ;
            //Oposite lepton charges
            if( sel_electrons[0].charge() < 0 && sel_muons[0].charge() > 0){
                lepton = &sel_electrons[0];
                antilepton = &sel_muons[0];
            }
            else if( sel_electrons[0].charge() > 0 && sel_muons[0].charge() < 0){
                lepton = &sel_muons[0];
                antilepton = &sel_electrons[0];
            }
            else vetoEvent;
        }
        //Muon-muon channel
        else if( n_el == 0 && n_mu == 2 ){
            Channel = mumu ;
            //Oposite lepton charges
            if( sel_muons[0].charge() < 0 && sel_muons[1].charge() > 0){
                lepton = &sel_muons[0];
                antilepton = &sel_muons[1];
            }
            else if( sel_muons[0].charge() > 0 && sel_muons[1].charge() < 0){
                lepton = &sel_muons[1];
                antilepton = &sel_muons[0];
            }
            else vetoEvent;
        }
        //Electron-electron channel
        else if( n_el == 2 && n_mu == 0 ){
            Channel = ee ;
            //Oposite lepton charges
            if( sel_electrons[0].charge() < 0 && sel_electrons[1].charge() > 0){
                lepton = &sel_electrons[0];
                antilepton = &sel_electrons[1];
            }
            else if( sel_electrons[0].charge() > 0 && sel_electrons[1].charge() < 0){
                lepton = &sel_electrons[1];
                antilepton = &sel_electrons[0];
            }
            else vetoEvent;
        }
        else vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_leptons) );

        if( bjets.size() < 2) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_bjets) );

        //CHeck that we have bjet and antibjet?
        //antilepton-bquark, lepton-antibquark
        //FourMomentum first_conf_bl = bjets[0].momentum() + lepton.momentum() ;
        //FourMomentum first_conf_bal = bjets[1].momentum() + antilepton.momentum();

        //first configuration
        double first_conf_bl = (bjets[0].momentum() + lepton->momentum()).mass() ;
        double first_conf_bal = (bjets[1].momentum() + antilepton->momentum()).mass() ;
        //second configuration
        double second_conf_bl = (bjets[1].momentum() + lepton->momentum()).mass() ;
        double second_conf_bal = (bjets[0].momentum() + antilepton->momentum()).mass() ;
        FourMomentum b_lepton, b_antilepton;
        if ( (first_conf_bl+first_conf_bal) < (second_conf_bl+second_conf_bal) ){
            b_lepton = bjets[0].momentum() + lepton->momentum();
            b_antilepton = bjets[1].momentum() + antilepton->momentum();
        }
        else{
            b_lepton = bjets[1].momentum() + lepton->momentum();
            b_antilepton = bjets[0].momentum() + antilepton->momentum();
        }
        //both shoudl be asigned
        //assert( b_lepton && b_antilepton);
        const double weight = 1; //event.weight();
        vector<CHANNEL> samples;
        samples.push_back( all );
        samples.push_back( Channel );

        foreach( auto iCh , samples ){
            //Linking bjets to leptons
            _h["bl_mass_" + channelStr[iCh] ]->fill(b_lepton.mass() , 1);
            _h["bal_mass_" + channelStr[iCh] ]->fill(b_antilepton.mass() , 1);

            vector<double> spt (Nsamples, 0); //sum of particle momentum
            vector<double> nch (Nsamples, 0); //multiplicity
            vector<double> mpt (Nsamples, 0); //mean pt

            FourMomentum nomObject = b_lepton; //nominal object (ttbar, qqbar, ...)
            calculate_basic_observables( spt, nch, _eventparticles, bjets, lepton->constituentLepton(), antilepton->constituentLepton(), nomObject, weight );
            //Filling
        for(unsigned int iR = 0; iR < Nsamples; ++iR){
            int index = iR +Nsamples*iCh;
            //std::cout << "Reg: " << iR << " spt: " << spt[iR] << " nch: " << nch[iR] <<  " mpt:"<< mpt[iR] <<std::endl;
            mpt[iR] = (nch[iR] > 0) ? spt[iR]/(double)nch[iR] : 0;
            _spt[index]->fill( spt[iR], weight);
            _nch[index]->fill( nch[iR], weight);
            _mpt[index]->fill( mpt[iR], weight);
            _spt_pt[index]->fill( nomObject.pt(), weight*spt[iR]);
            _nch_pt[index]->fill( nomObject.pt(), weight*nch[iR]);
            _mpt_pt[index]->fill( nomObject.pt(), weight*mpt[iR]);
            _mpt_nch[index]->fill( nch[iR], weight*mpt[iR]);
            _spt_nch[index]->fill( nch[iR], weight*spt[iR]);

        }


        }
    }
    void finalize() {
        // Normalize to cross-section
        const double sf(crossSection() / sumOfWeights());

        for (unsigned int iCh=0; iCh < nchannels; ++iCh) {
            scale(_h["bl_mass_"+ channelStr[iCh]], sf);
            scale(_h["bal_mass_"+ channelStr[iCh]], sf);
        }
    }


private:

    Histo1DPtr _hist;
    map<string, Histo1DPtr> _h;
    //Particles _neutrinos;
    vector<DressedLepton> _dres_el, _dres_mu;
    Histo1DPtr cut_flow;

    //UE Histograms/Profiles
    Histo1DPtr _pt[Nsamples*nchannels];
    Histo1DPtr _spt[Nsamples*nchannels];
    Histo1DPtr _nch[Nsamples*nchannels];
    Histo1DPtr _mpt[Nsamples*nchannels];
    Profile1DPtr _spt_pt[Nsamples*nchannels];
    Profile1DPtr _nch_pt[Nsamples*nchannels];
    Profile1DPtr _mpt_pt[Nsamples*nchannels];
    Profile1DPtr _mpt_nch[Nsamples*nchannels];
    Profile1DPtr _spt_nch[Nsamples*nchannels];

    /*
    double computeneutrinoz(const FourMomentum& lepton, FourMomentum& met) const {
    //computing z component of neutrino momentum given lepton and met
    double pzneutrino;
    double m_W = 80.399; // in GeV, given in the paper
    //Q: Check the computation!!
    double k = (( sqr( m_W ) - sqr( lepton.mass() ) ) / 2 ) + (lepton.px() * met.px() + lepton.py() * met.py());
    double a = sqr ( lepton.E() )- sqr ( lepton.pz() );
    double b = -2*k*lepton.pz();
    double c = sqr( lepton.E() ) * sqr( met.pT() ) - sqr( k );
    double discriminant = sqr(b) - 4 * a * c;
    double quad[2] = { (- b - sqrt(discriminant)) / (2 * a), (- b + sqrt(discriminant)) / (2 * a) }; //two possible quadratic solns
    if (discriminant < 0)  pzneutrino = - b / (2 * a); //if the discriminant is negative
    else { //if the discriminant is greater than or equal to zero, take the soln with smallest absolute value
        double absquad[2];
        for (int n=0; n<2; ++n)  absquad[n] = fabs(quad[n]);
        if (absquad[0] < absquad[1])  pzneutrino = quad[0];
        else                          pzneutrino = quad[1];
    }
    if ( !std::isfinite(pzneutrino) )  std::cout << "Found non-finite value" << std::endl;
    return pzneutrino;
    }

    double _mT(const FourMomentum &l, FourMomentum &nu) const {
    return sqrt( 2 * l.pT() * nu.pT() * (1 - cos(deltaPhi(l, nu))) );
    }
    */

    //Check if the particle is from the main event (one of the two main bjets, one of the two main lightjets, selected lepton) or from underlying event
    bool is_UEparticle(Particle& p, Jets& bjets, const Particle& lepton, const Particle& antilepton){
        bool isUE = true;
        //if( general_particle == *dynamic_cast<Particle*>(lepton) ){
        if( p == lepton || p == antilepton ) return false;
        foreach( Jet fj, bjets){
            foreach( Particle jet_p, fj.particles() ){
                //@Todo there should be is in jet function
                if( p != jet_p ) continue;
                else{
                    isUE = false;
                }
                if( !isUE ) break;
            }
            if ( !isUE ) break;
        }
        return isUE;
    }

    /// Set an integer region flag on the object, indicating that it's in the toward, transL, transR, or away region respectively
    Particle_samples select_region(const Particle& particle, const FourMomentum& nominalObject ) {
        //@Rivet does not provide deltaPhi()function in other ranges than 0-pi?
        double dphi = particle.phi() - nominalObject.phi(); //phi() by default in 0-2pi region
        //convert dphi to (-pi, pi) interval
        if(dphi > M_PI ) dphi -= 2*M_PI;
        else if(dphi < - M_PI) dphi += 2*M_PI;
        const double adphi = fabs(dphi);
        assert(adphi <= M_PI);

        // Identify region
        //regionID reg = Nregions;
        Particle_samples reg = Nsamples;
        if (adphi < TO_ANGLE) {
            reg = rToward; //< towards
        } else if (adphi >= TO_ANGLE && adphi < AW_ANGLE) {
            //@note!!! Here is the selection to min and max missleading
            //It is rather selected to transverseleft and transverseright
            //But to keep the same numbering as in regionID min max are used
            reg = (dphi < 0) ? rTransverseMin : rTransverseMax; //< trans (L/R)
        } else if (adphi >= AW_ANGLE) {
            reg = rAway; //< away
        }
        //std::cout << "DeltaPhi: " << dphi << " Reg: "<< reg << std::endl;
        //std::cout << "ParticlePhi: " << particle.phi() << " ttbarphi: "<< ttbar.phi() << " DeltaPhi: " << dphi << " Reg: "<< reg << std::endl;
        assert(reg != Nsamples);
        return reg;
    }
    //Calculate UE observables spt,nch in each region/sample
    void calculate_basic_observables( std::vector<double>& spt, std::vector<double>& nch, Particles& particles, Jets& bjets, const Particle& lepton, const Particle& antilepton, FourMomentum& nomObject,double weight ){
        //UE observables inside jets
        //TODO Better estimation of bjet and antibjet
        foreach(Particle p, bjets[0].particles() ){
            spt[bjet] += p.pt();
            nch[bjet] += 1;
            _pt[bjet]->fill( p.pt(), weight );
        }
        foreach(Particle p, bjets[1].particles() ){
            spt[bjet2] += p.pt();
            nch[bjet2] += 1;
            _pt[bjet2]->fill( p.pt(), weight );
        }

        foreach(Particle p, particles){
            //Exclusion on main jets and lepton
            if( !is_UEparticle( p, bjets, lepton, antilepton ) ) continue;
            //Cross check - inclusive particles

            //determine region
            Particle_samples regID = select_region(p, nomObject);
            spt[rAll] += p.pt();
            nch[rAll] += 1;
            _pt[rAll]->fill( p.pt(), weight );


            // std::cout << "particle ID " << regID << std::endl;
            switch(regID){
            case rToward:
                spt[rToward] += p.pt();
                nch[rToward] += 1;
                _pt[rToward]->fill( p.pt(), weight );
                //     std::cout << "Toward" << std::endl;
                break;
            case rTransverseMin:
                spt[rTransverseMin] += p.pt();
                nch[rTransverseMin] += 1;
                _pt[rTransverseMin]->fill( p.pt(), weight );
                spt[rTransverse] += p.pt();
                nch[rTransverse] += 1;
                _pt[rTransverse]->fill( p.pt(), weight );
                //    std::cout << "TransverseMin" << std::endl;
                break;
            case rTransverseMax:
                spt[rTransverseMax] += p.pt();
                nch[rTransverseMax] += 1;
                _pt[rTransverseMax]->fill( p.pt(), weight );
                spt[rTransverse] += p.pt();
                nch[rTransverse] += 1;
                _pt[rTransverse]->fill( p.pt(), weight );
                //  std::cout << "TransverseMax" << std::endl;
                break;
            case rAway:
                spt[rAway] += p.pt();
                nch[rAway] += 1;
                _pt[rAway]->fill( p.pt(), weight );
                //  std::cout << "Away" << std::endl;
                break;
            default:
                std::cout << "Error" << std::endl;
            }

        }

        //Need to determine max and min region and swap if necessary because so far they have been treated as left right
        int which_max =  (spt[rTransverseMin] < spt[rTransverseMax] ) ? rTransverseMax : rTransverseMin;
        if( which_max == rTransverseMin ){
            double spt_tmp = spt[rTransverseMin];
            int nch_tmp = nch[rTransverseMin];
            spt[rTransverseMin] = spt[rTransverseMax];
            nch[rTransverseMin] = nch[rTransverseMax];
            spt[rTransverseMax] = spt_tmp;
            nch[rTransverseMax] = nch_tmp;
        }

    }

  /*  string obs_name( observable){
        std::map<Particle_samples, string> Obs;
        Obs[OBS::LeadPt] = "LeadPt";

        std::map<OBS, string>::const_iterator  it = Obs.find(observable);
        if( it != Obs.end()) return it->second;
        else{
            // std::cout << " Conversion error enum observable -> string." << std::endl;
            // std::ostringstream os; os << observable;
            // throw logic_error(os.str());
            // return os.str();
            std::cout << "Conversion error enum observable -> string" << std::endl;
            return "Conversion error enum observable -> string";
        }
    } */


};
//Must be same ordering as in enum!!!
const string ATLAS_2016_Top_dilepton::channelStr[] = { "all", "ee", "mumu", "emu" };
const string ATLAS_2016_Top_dilepton::regionStr[] = { "b1", "b2", "all", "to", "trmin", "trmax", "tr", "aw"};


// Angular region definition: TR_SIZE is half the angle of the total transverse region angle


const double ATLAS_2016_Top_dilepton::TR_SIDE_SIZE = M_PI/3.0; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
const double ATLAS_2016_Top_dilepton::TR_SIZE = 2*TR_SIDE_SIZE; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
const double ATLAS_2016_Top_dilepton::TO_ANGLE = M_PI/2.0 - TR_SIDE_SIZE/2.0; //< dphi at which TO region ends and TR begins
const double ATLAS_2016_Top_dilepton::TO_SIZE = 2*TO_ANGLE; // total toward size
const double ATLAS_2016_Top_dilepton::AW_ANGLE = M_PI/2.0 + TR_SIDE_SIZE/2.0; //< dphi at which TR region ends and AW begins
const double ATLAS_2016_Top_dilepton::AW_SIZE = 2*(M_PI - AW_ANGLE); // total away size


// The hook for the plugin system
DECLARE_RIVET_PLUGIN(ATLAS_2016_Top_dilepton);

}
