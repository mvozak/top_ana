// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include <utility> //for pairs
#include <cmath> //for ceil

//TECHNICALITIES
//@Todo Separate on several jobs? FIFO?

//PLOTS
//@Todo MB plots of meanpt and nch vs nch -> will be in R. Lysak analysis
//---------------------------------------
//@Todo t_mass plots
//@Todo Check m_ttbar, large values and too wide spread -> due to the pt of the system?
//@Todo Normalization for jet differential density
void Efind(){
    static int j = 0;
    ++j;
    std::cout << j << std::endl;
    return;
}

namespace Rivet {
/// @brief ATLAS 13 TeV Top-UE analysis
///
/// @author M .Vozak <mvozak@cern.ch>
class ATLAS_2016_UETop : public Analysis {
public:
    /// Constructor
    /// Q:What for?
    DEFAULT_RIVET_ANALYSIS_CTOR(ATLAS_2016_UETop);

    enum regionID {
        rAll = 0,
        rToward =1,
        rTransverseMin = 2,
        rTransverseMax = 3,
        rTransverse =4,
        rAway = 5,
        //b1_cone = 1, //cone around jet with first b quark
        //b2_cone = 2, //cone around jet with the other b quark
        //l1_cone, //cone around the first lightjet
        //l2_cone, //cone around the second lightjet
        Nregions
        /*
    kToward  = 0,
    kAway,
    kTrans,
    kTransMax,
    kTransMin,
    kTransDiff,
    kNregions
    */
    };

    enum CUT_FLOW{
        none,
        n_elmu_neutrions,
        n_elmu,
        overlap,
        n_bjets,
        n_lightjets,
        e_phasespace,
        mu_phasespace,
        near_w_mass
    };

    int which_cut(CUT_FLOW cut){
        std::map<CUT_FLOW, int> cut_flow;
        cut_flow[CUT_FLOW::none] = 1;
        cut_flow[CUT_FLOW::n_elmu_neutrions] = 2;
        cut_flow[CUT_FLOW::n_elmu] = 3;
        cut_flow[CUT_FLOW::overlap] = 4;
        cut_flow[CUT_FLOW::n_bjets] = 5;
        cut_flow[CUT_FLOW::n_lightjets] = 6;
        cut_flow[CUT_FLOW::e_phasespace] = 7;
        cut_flow[CUT_FLOW::mu_phasespace] = 8;
        cut_flow[CUT_FLOW::near_w_mass] = 9;


        for(auto c : cut_flow){
            if( cut == c.first ) return c.second;
        }
        //if not find among CUT_FLOW return underflow bin
        return -1;
    }


    const static int    nBin_nch   ;
    const static double Bin_nchMin ;
    const static double Bin_nchMax ;
    //Q: Best way to initialize?
    std::vector<double> UE_Delta_Rs; // { 0.8, 1.4, 1.8, 2.2 }; // limit for a cone in which ue contribution is studied
    //particle types included
    enum partTypes {
        k_AllCharged,
        k_NoStrange,
        kNPartTypes
    };

    //@Todo: Use cut class?
    std::vector< std::pair< std::string, double > > top_pt_slices;
    //labels for different particle types
    const static string partTypeStr[kNPartTypes];
    const static string regionStr  [Nregions];
    const static unsigned int NdR = 4; // number of different UE_deltaR in study
    const static double w_mass;

    // Angular region definition: TR_SIZE is half the angle of the total transverse region angle
    const static double TR_SIDE_SIZE;// = M_PI/3.0; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
    const static double TR_SIZE;// = 2*TR_SIDE_SIZE; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
    const static double TO_ANGLE;// = M_PI/2.0 - TR_SIDE_SIZE/2.0; //< dphi at which TO region ends and TR begins
    const static double TO_SIZE;// = 2*TO_ANGLE; // total toward size
    const static double AW_ANGLE;// = M_PI/2.0 + TR_SIDE_SIZE/2.0; //< dphi at which TR region ends and AW begins
    const static double AW_SIZE;// = 2*(M_PI - AW_ANGLE); // total away size




    void init() {
        // Top pt cuts
        // double pt_lthresh = 40;
        double pt_hthresh = 100;
        double pt_step = 10;
        for( unsigned int iSlice = 1; iSlice <= 5; ++iSlice){
            //Q: Use Cut class?
            std::pair < std::string, double > top_pt_slice;
            top_pt_slice = make_pair( std::to_string( (int)(pt_hthresh - pt_step*iSlice) ) + "GeV", (pt_hthresh - pt_step *iSlice)*GeV  );
            top_pt_slices.push_back( top_pt_slice );
        }
        for( unsigned int dR = 0; dR <= NdR; ++dR) UE_Delta_Rs.push_back( 0.8 + 4*dR );

        // Eta ranges
        //Q:Really from 1MeV?
        Cut eta_full = (Cuts::abseta < 5.0) & (Cuts::pT >= 1.0*MeV);
        Cut eta_lep = (Cuts::abseta < 2.5);

        // All final state particles
        //Q: No need for addProjection?
        //A: Already inside FinalState constructor

        //Q: What does addProjection really do?
        //Q: How is the final state defined? What about strange baryons?
        FinalState fs(eta_full);

        //Q: Identification of final states? Splitting into categories (electrons, muons, neutrinos, ...)?
        IdentifiedFinalState photons(fs);
        //Q:Conteiner of particles ID?
        photons.acceptIdPair(PID::PHOTON);

        // Projection to find the electrons
        IdentifiedFinalState el_id(fs);
        el_id.acceptIdPair(PID::ELECTRON);

        //Q: Why do I need to introduce Prompt?
        PromptFinalState electrons(el_id);
        //Q: Accept electrons decayed from muon from tau except directly produced ones?
        electrons.acceptTauDecays(true);
        //Q: what does it do? Why do I need to do this?
        //A: Declare projection -> register projection
        //Q: What is meant by register?
        declare(electrons, "electrons");

        //A: Clustered Object where photons are assigned to leptons
        //Get photons to dress leptons
        DressedLeptons dressedelectrons(photons, electrons, 0.1, eta_lep & (Cuts::pT >= 25.0*GeV), true, true);
        declare(dressedelectrons, "dressedelectrons");

        DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
        declare(ewdressedelectrons, "ewdressedelectrons");

        DressedLeptons vetodressedelectrons(photons, electrons, 0.1, eta_lep & (Cuts::pT >= 15.0*GeV), true, true);
        declare(vetodressedelectrons, "vetodressedelectrons");

        // Projection to find the muons
        IdentifiedFinalState mu_id(fs);
        mu_id.acceptIdPair(PID::MUON);
        PromptFinalState muons(mu_id);
        muons.acceptTauDecays(true);
        declare(muons, "muons");
        DressedLeptons dressedmuons(photons, muons, 0.1, eta_lep & (Cuts::pT >= 25.0*GeV), true, true);
        declare(dressedmuons, "dressedmuons");
        DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
        declare(ewdressedmuons, "ewdressedmuons");
        DressedLeptons vetodressedmuons(photons, muons, 0.1, eta_lep & (Cuts::pT >= 15.0*GeV), true, true);
        declare(vetodressedmuons, "vetodressedmuons");

        // Projection to find neutrinos and produce MET
        IdentifiedFinalState nu_id;
        nu_id.acceptNeutrinos();
        PromptFinalState neutrinos(nu_id);
        //neutrinos.acceptTauDecays(true);
        neutrinos.acceptTauDecays(false); //small effect on n_neutrino cut?

        declare(neutrinos, "neutrinos");

        // Jet clustering.
        VetoedFinalState vfs;
        vfs.addVetoOnThisFinalState(ewdressedelectrons);
        vfs.addVetoOnThisFinalState(ewdressedmuons);
        vfs.addVetoOnThisFinalState(neutrinos);
        FastJets jets(vfs, FastJets::ANTIKT, Delta_R);
        //CHANGE: A-S article exclude neutrinos from forming a jet!!
        jets.useInvisibles();
        declare(jets, "jets");


        /////UE
        //Q: Diff cut for pt of final charged particle
        const ChargedFinalState cfs500(-2.5, 2.5, 500*MeV);
        addProjection(cfs500, "CFS500");
        //Q: Init outside and pass as a argument, or assigned through function?
        std::vector<double> ptbinning = ue_ptbinning();
        for (int iType=0; iType < kNPartTypes; ++iType) {
            for (unsigned int iRegion=0; iRegion < Nregions; ++iRegion) {
                for( unsigned int dR = 0; dR < NdR; ++dR ){  //Looping over different UE cones
                    //@Todo Select better binning
                    // _hist_ptavg_toppt [iType][iRegion][dR]  = bookProfile1D( partTypeStr[iType] + "_ptavgpt_"  + regionStr[iRegion] + "_ueR" + std::to_string(dR)  ,ptbinning);
                    // _hist_ptavg_nch[iType][iRegion]  = bookProfile1D( partTypeStr[iType] + "_ptavgnch_"    + regionStr[iRegion] , nBin_nch, rangeMin_nch, rangeMax_nch);

                }
            }
        }
        //@Todo similar cut flow as in UE analysis
        cut_flow = bookHisto1D( "cut_flow", 10, 0, 10  );

        //@Todo binning?
        vector<double> pt_w_binning;//[41];
        vector<double> pt_t_binning;//[39];
        vector<double> pt_el_binning;//[37];
        //double pt_mu_binning[];
        vector<double> pt_lj_binning;//[37];
        vector<double> mpt_binning;
        // double pt_slj_binning[];

        //w pt binning
        for(unsigned int i = 0; i<=30 ; ++i){
            //pt_w_binning[i] = 5*i;
            pt_w_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=9 ; ++i){
            //pt_w_binning[i+31] = 160 + 10*i;
            pt_w_binning.push_back(160 + 10*i);
        }

        //top pt binning
        for(unsigned int i = 0; i<=5 ; ++i){
            //pt_t_binning[i] = 10*i;
            pt_t_binning.push_back(10*i);
        }
        for(unsigned int i = 0 ; i<=19 ; ++i){
            //pt_t_binning[i+6] = 55 + 5*i;
            pt_t_binning.push_back( 55 + 5*i);
        }
        for(unsigned int i = 0 ; i<=9; ++i){
            //pt_t_binning[i+26] = 160 + 10*i;
            pt_t_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=2; ++i){
            //pt_t_binning[i+37] = 260 + 20*i;
            pt_t_binning.push_back( 260 + 20*i);
        }

        //el pt binning = mu pt binning
        for(unsigned int i = 0; i<=30 ; ++i){
            //pt_el_binning[i] = 5*i;
            pt_el_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=4 ; ++i){
            //pt_el_binning[i+31] = 160 + 10*i;
            pt_el_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=1 ; ++i){
            //pt_el_binning[i+36] = 250 + 50*i;
            pt_el_binning.push_back(250 + 50*i);
        }


        //lj pt binning
        for(unsigned int i = 0; i<=30 ; ++i){
            //pt_lj_binning[i] = 5*i;
            pt_lj_binning.push_back(5*i);
        }
        for(unsigned int i = 0 ; i<=4 ; ++i){
            //pt_lj_binning[i+31] = 160 + 10*i;
            pt_lj_binning.push_back(160 + 10*i);
        }
        for(unsigned int i = 0 ; i<=1 ; ++i){
            //pt_lj_binning[i+36] = 250 + 50*i;
            pt_lj_binning.push_back(250 + 50*i);
        }
        for(unsigned int i = 0 ; i<=25 ; ++i){
            //pt_lj_binning[i+36] = 250 + 50*i;
            mpt_binning.push_back( 1*i);
        }


        //_h["ttbarmass"]                = bookHisto1D( "ttbarmass", 100, 100, 250, "", "m_{top}", "ent"  );
        _h["ttbarmass"]                = bookHisto1D( "ttbarmass", 40, 200, 1000, "", "m_{top}", "ent"  );
        _h["tmass"]                = bookHisto1D( "tmass", 100, 100, 250, "", "m_{top}", "ent"  );
        _h["tmass_leptonic"]                = bookHisto1D( "tmass_leptonic", 100, 100, 250, "", "m_{top}", "ent"  );
        _h["tmass_hadronic"]                = bookHisto1D( "tmass_hadronic", 100, 100, 250, "", "m_{top}", "ent"  );
        // _h["ptw_leptonic"]                = bookHisto1D( "ptw_l", 500, 0, 250, "", "pT_{wl}", "ent"  );
        // _h["ptw_hadronic"]                = bookHisto1D( "ptw_h", 500, 0, 250, "", "pT_{wh}", "ent"  );
        // _h["ptT_leptonic"]                = bookHisto1D( "ptT_l", 600, 0, 300, "", "pT_{Tl}", "ent"  );
        // _h["ptT_hadronic"]                = bookHisto1D( "ptT_h", 600, 0, 300, "", "pT_{Th}", "ent"  );
        _h["ptw_leptonic"]                = bookHisto1D( "ptw_l",  pt_w_binning , "", "pT_{wl}", "ent"  );
        _h["ptw_hadronic"]                = bookHisto1D( "ptw_h",  pt_w_binning, "", "pT_{wh}", "ent"  );
        _h["ptT_leptonic"]                = bookHisto1D( "ptT_l",  pt_t_binning, "", "pT_{Tl}", "ent"  );
        _h["ptT_hadronic"]                = bookHisto1D( "ptT_h", pt_t_binning, "", "pT_{Th}", "ent"  );



        _h["lightjetmass"]                = bookHisto1D( "mass_lj",600, 0, 300, "", "m_{2lj}", "ent"  );
        _h["lightjetmultiplicity"]                = bookHisto1D( "n_lj",11, -0.5, 10.5, "", "n_{lj}", "ent"  );


        //Added
        _h["hhist_jetshape"]       = bookHisto1D( "helphist", 50, 0, 1, "", "r/R", "#rho");
        _h["lightjetcorrelation"]   = bookHisto1D( "lightjetcorrelation", 40, 0, 4, "", "#Delta R", "1/N dN /d #Delta R");
        _h["bjetwcorrelation"]   = bookHisto1D( "bjetwcorrelation", 40, 0, 4, "", "#Delta R", "1/N dN /d #Delta R");

        _h2["topptcorrelation"]                = bookHisto2D( "topptmatrix", 100, 0, 100, 100, 0, 100, "", "pT_{W->qq}", "pT_{W->ll}", "");

        //Dummy hist for storing the diferential jet shape from individual jets
        //TODO: Try to plot for different pT slices
        _p["diffjetshape" ]     = bookProfile1D( "difjetshape", 50, 0, 1,"", "r/R", "#rho"  );
        foreach( auto slice,  top_pt_slices  ){
            _p["diffjetshape" + slice.first + "pt"]     = bookProfile1D( "difjetshape" + slice.first, 50, 0, 1,"", "r/R", "#rho"  );
            //_p["diffjetshape"]     = bookProfile1D( "difjetshape", 25, 0, 1,"", "r/R", "#rho"  );
        }

        //Added 8.12.2016
        // _h["pt_e"]                = bookHisto1D( "pt_e", 600, 0, 300, "", "pT_{e}", "ent"  );
        // _h["pt_mu"]                = bookHisto1D( "pt_mu", 600, 0, 300, "", "pT_{mu}", "ent"  );
        // _h["pt_l"]                = bookHisto1D( "pt_mu", 600, 0, 300, "", "pT_{mu}", "ent"  );
        _h["pt_e"]                = bookHisto1D( "pt_e", pt_el_binning, "", "pT_{e}", "ent"  );
        _h["pt_mu"]                = bookHisto1D( "pt_mu", pt_el_binning, "", "pT_{mu}", "ent"  );
        _h["pt_l"]                = bookHisto1D( "pt_mu", pt_el_binning, "", "pT_{lepton}", "ent"  );


        // _h["pt_leadinglj"]                = bookHisto1D( "pt_leadinglj", 600, 0, 300, "", "pT_{lj}", "ent"  );
        // _h["pt_selectedlj"]                = bookHisto1D( "pt_lj", 600, 0, 300, "", "pT_{lj}", "ent"  );
        // _h["pt_restljs"]                = bookHisto1D( "pt_restljs", 600, 0, 300, "", "pT_{lj}", "ent"  );
        _h["pt_leadinglj"]                = bookHisto1D( "pt_leadinglj", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_selectedlj"]                = bookHisto1D( "pt_lj", pt_lj_binning, "", "pT_{lj}", "ent"  );
        _h["pt_restljs"]                = bookHisto1D( "pt_restljs", pt_lj_binning, "", "pT_{lj}", "ent"  );

        _p["sumpt_nch"]     = bookProfile1D( "sumpt_nch", 80, 0.5, 80.5,"", "nch", "sum_pt"  );
        //_p["sumpt_pt"]     = bookProfile1D( "sumpt_pt", 300, 0, 150,"", "pt", "sum_pt"  );

        //Pull Angle
        _h["pa_b1"]                = bookHisto1D( "pullangle_b1", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_b2"]                = bookHisto1D( "pullangle_b2", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_lj1"]                = bookHisto1D( "pullangle_lj1", 32, -3.2, 3.2 , "", " \theta []", "ent"  );
        _h["pa_lj2"]                = bookHisto1D( "pullangle_lj2", 32, -3.2, 3.2 , "", " \theta []", "ent"  );


        //UE studies  -> Fig.8  http://home.thep.lu.se/~torbjorn/preprints/lutp1423.pdf
        _h["sumpt"]     = bookHisto1D( "sumpt", 50, 0, 500,"", "sumpt", "ent"  );
        _h["nch"]     = bookHisto1D( "nch", 200, 0.5, 200.5,"", "nch", "ent"  );
        _h["pt"]     = bookHisto1D( "pt", 50, 0, 5,"", "sumpt", "ent"  );
        _p["mpt_nch"]     = bookProfile1D( "mpt_nch", 80, 0.5, 80.5,"", "nch", "<pT>"  );

        //SpT, Nch, meanpT constructed in different regions defined by azimuthal angle from p(ttbar) (CMS plots)
        for (unsigned int iRegion=0; iRegion < Nregions; ++iRegion) {
                _spt[iRegion]  = bookHisto1D( "spt_"  + regionStr[iRegion] , pt_el_binning);
                _nch[iRegion]  = bookHisto1D( "nch_"  + regionStr[iRegion] , 200, 0.5, 200.5);
                _mpt[iRegion]  = bookHisto1D( "mpt_"  + regionStr[iRegion] , mpt_binning);
                _spt_pt[iRegion]  = bookProfile1D( "spt_pt_"  + regionStr[iRegion] , pt_el_binning);
                _nch_pt[iRegion]  = bookProfile1D( "nch_pt_"  + regionStr[iRegion] , pt_el_binning);
                _mpt_pt[iRegion]  = bookProfile1D( "mpt_pt_"  + regionStr[iRegion] , pt_el_binning);
        }







    }

    void analyze(const Event& event) {
        // Get the selected objects, using the projections.
        //Q: The connection to projections defined in init are via strings ("neutrinos", ...)?
        _dressedelectrons     = apply<DressedLeptons>(  event, "dressedelectrons").dressedLeptons();
        _vetodressedelectrons = apply<DressedLeptons>(  event, "vetodressedelectrons").dressedLeptons();
        _dressedmuons         = apply<DressedLeptons>(  event, "dressedmuons").dressedLeptons();
        _vetodressedmuons     = apply<DressedLeptons>(  event, "vetodressedmuons").dressedLeptons();
        _neutrinos            = apply<PromptFinalState>(event, "neutrinos").particlesByPt();
        // These are the charged particles (tracks) with pT > 500 MeV
        //Q: difference between apply and applyProjection?
        const ChargedFinalState& charged500 = applyProjection<ChargedFinalState>(event, "CFS500");
        MSG_DEBUG("charged500 size : " << charged500.size() );

        //CHANGE A-S eta < 4.5
        //Q: Cuts are taken with respect to what? To jet as the whole (sum of pT, Eta of the highest particle? Or weighted?)?
        const Jets& all_jets  = apply<FastJets>(        event, "jets").jetsByPt(Cuts::pT > 25.0*GeV && Cuts::abseta < 2.5);



        Particles _eventparticles[kNPartTypes];
        _eventparticles[k_AllCharged] = charged500.particlesByPt();
        MSG_DEBUG("particlesAll size : " << _eventparticles[k_AllCharged].size() );

        MSG_DEBUG("removing strange baryons : "  );
        //remove strange baryons, pt ordering should be preserved
        if (_eventparticles[k_AllCharged].size() > 0) {
            //@Todo: include in particle selection? Create separate funct?
            foreach (const Particle& p, _eventparticles[k_AllCharged]) {
                PdgId pdg = p.abspid ();
                if ( pdg == 3112 || // Sigma-
                                pdg == 3222 || // Sigma+
                                pdg == 3312 || // Xi-
                                pdg == 3334 )  // Omega-
                    continue;
                _eventparticles[k_NoStrange].push_back( p );
            }
        }
        MSG_DEBUG("noStrange baryons size: " <<  _eventparticles[k_NoStrange].size() );

        //Join with above part of the code?
        //MB study
        for (int iType=0; iType < kNPartTypes; ++iType){
            double AveragePt = 0, Multiplicity = 0;
            foreach(Particle general_particle, _eventparticles[iType]){ //already cuts on pt and eta
                //Q: Any other cuts?
                AveragePt += general_particle.pt();
                ++Multiplicity;
            }
        }



        //-----Selection criterions-------------------
        cut_flow->fill( which_cut( CUT_FLOW::none) );
        //get true l+jets events by removing events with more than 1 electron||muon neutrino
        //CHANGE A-S cut on neutrinos? if expected them to be only the one coming from W decay
        unsigned int n_elmu_neutrinos = 0;
        foreach (const Particle p, _neutrinos) {
            if (p.abspid() == 12 || p.abspid() == 14)  ++n_elmu_neutrinos;
        }
        if (n_elmu_neutrinos != 1)  vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_elmu_neutrions) );

        DressedLepton *lepton;
        //Q: what about the case that there are both dressed el and mu?
        if ( _dressedelectrons.size())  lepton = &_dressedelectrons[0];
        else if (_dressedmuons.size())  lepton = &_dressedmuons[0];
        else vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_elmu) );

        // Calculate the missing ET, using the prompt neutrinos only (really?)
        /// @todo Why not use MissingMomentum?
        FourMomentum met;
        //Q: Should be only one contribution from neutrinos due to the upper cut, right?
        foreach (const Particle& p, _neutrinos)  met += p.momentum();

        //remove jets if they are within dR < 0.2 of lepton
        //Q: Why only vetodressed el?
        //CHANGE: A-S dR < 0.4
        Jets jets;
        foreach(const Jet& jet, all_jets) {
            bool keep = true;
            foreach (const DressedLepton& el, _vetodressedelectrons) {
                keep &= deltaR(jet, el) >= 0.2;
            }
            if (keep)  jets += jet;
        }

        bool overlap = false;
        Jets bjets, lightjets;
        for (unsigned int i = 0; i < jets.size(); ++i) {
            const Jet& jet = jets[i];
            foreach (const DressedLepton& el, _dressedelectrons)  overlap |= deltaR(jet, el) < 0.4;
            foreach (const DressedLepton& mu, _dressedmuons)      overlap |= deltaR(jet, mu) < 0.4;
            //CHANGE: Not requested for A-S analysis
            for (unsigned int j = i + 1; j < jets.size(); ++j) {
                overlap |= deltaR(jet, jets[j]) < 0.5;
            }
            //// Count the number of b-tags
            bool b_tagged = false;           //  This is closer to the
            Particles bTags = jet.bTags();   //  analysis. Something
            //CHANGE: Request for B hadron to have eta < 2.5 and pT > 5 GeV -> Is this covered in constructor of jets?
            foreach ( Particle b, bTags ) {  //  about ghost-associated
                b_tagged |= b.pT() > 5*GeV;    //  B-hadrons
            }                                //
            if ( b_tagged )  bjets += jet;
            else lightjets += jet;
        }

        // remove events with object overlap
        if (overlap) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::overlap) );

        //Presence of at least 2bjets and at least 2lightjets with the presence of exactly one electron or one muon depending on the channel
        //if (bjets.size() < 2 || lightjets.size() < 2)  vetoEvent;
        if (bjets.size() < 2 )  vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_bjets) );
        if (lightjets.size() < 2)  vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::n_lightjets) );

        FourMomentum pbjet1; //Momentum of bjet1
        FourMomentum pbjet2; //Momentum of bjet2
        //Q: Are jets sorted by pT?
        //Q: Why should I take only first two if there are more?
        //A: A-S take two highest pT jets
        //Q: Assigning bjets, incorrect if ttbar are in rest
        if ( deltaR(bjets[0], *lepton) <= deltaR(bjets[1], *lepton) ) {
            pbjet1 = bjets[0].momentum();
            pbjet2 = bjets[1].momentum();
        } else {
            pbjet1 = bjets[1].momentum();
            pbjet2 = bjets[0].momentum();
        }



        // Evaluate basic event selection
        bool pass_eljets = (_dressedelectrons.size() == 1) &&
                        (_vetodressedelectrons.size() < 2) &&
                        (_vetodressedmuons.empty()) &&
                        //CHANGE: met.pT() >= 35 GeV
                        (met.pT() > 30*GeV) &&
                        //CHANGE: _mT >= 25 GeV
                        (_mT(_dressedelectrons[0].momentum(), met) > 35*GeV) &&
                        //Shouldnt I check rather 2bjets and 2 lightjets?
                        //(jets.size() >= 4);
                        (bjets.size() >= 2) &&
                        (lightjets.size() >= 2);
        bool pass_mujets = (_dressedmuons.size() == 1) &&
                        (_vetodressedmuons.size() < 2) &&
                        (_vetodressedelectrons.empty()) &&
                        //CHANGE: met.pT() >= 35 GeV
                        (met.pT() > 30*GeV) &&
                        //CHANGE: _mT >= 60 GeV
                        (_mT(_dressedmuons[0].momentum(), met) > 35*GeV) &&
                        //Shouldnt I check rather 2bjets and 2 lightjets?
                        //(jets.size() >= 4);
                        (bjets.size() >= 2) &&
                        (lightjets.size() >= 2);




        // basic event selection requirements
        //if (!pass_eljets && !pass_mujets) vetoEvent;
        if( pass_eljets) cut_flow->fill( which_cut( CUT_FLOW::e_phasespace) );
        if( pass_mujets) cut_flow->fill( which_cut( CUT_FLOW::mu_phasespace) );
        if (!pass_eljets && !pass_mujets) vetoEvent;

        //@Todo check whether lightjets are order by descending pt?
        //CHeck fill at first highest pt lightjet
        _h["pt_leadinglj"]->fill( lightjets[0].pt() , 1 );


        //Selecting lightjets from W decay
        double dummy_mass = 20;
        double min_delta_mass = dummy_mass; //default set to large number
        unsigned int position = 0;
        std::pair<unsigned int, unsigned int> ljets_position (0,0);
        //std::cout << "lightjets number: " <<lightjets.size() << std::endl;
        for( auto ljet : lightjets){
            unsigned position2 = position + 1;
            for( unsigned int lj2 = position2; lj2< lightjets.size(); ++lj2 ){
                FourMomentum light1 = ljet.momentum();
                FourMomentum light2 = lightjets[lj2].momentum();
                double delta_mass = abs(w_mass - (light1 + light2).mass());
                // std::cout << w_mass << " - " << ljet.mass() << " - " << lightjets[lj2].mass() << " " <<  (light1 + light2).mass()  <<std::endl;
                // std::cout << "mindelta: " << min_delta_mass << std::endl;
                // std::cout << "delta: " << delta_mass << std::endl;
                if( delta_mass < min_delta_mass ){
                    ljets_position.first = position;
                    ljets_position.second = position2;
                    min_delta_mass = delta_mass;
                }
            }
            ++position;
        }
        //@Todo condition if none of the jet combination is near w_mass?
        if( min_delta_mass == dummy_mass ) vetoEvent;
        cut_flow->fill( which_cut( CUT_FLOW::near_w_mass) );
        //Check fill at first highest pt lightjet
        _h["pt_selectedlj"]->fill( (lightjets[ljets_position.first].pt() > lightjets[ljets_position.second].pt() ) ? lightjets[ljets_position.first].pt() :lightjets[ljets_position.second].pt() , 1 );
        //@Todo if lightjets are sorted then use
        //_h["pt_selectedlj"]->Fill( lightjets[ljets_position.first].pt() , 1 );




        FourMomentum pjet1; // Momentum of jet1
        //Q: Why condition? This should be already covered with vetoEvent above
        //if (lightjets.size())  pjet1 = lightjets[0].momentum();
        if (lightjets.size())  pjet1 = lightjets[ljets_position.first].momentum();

        FourMomentum pjet2; // Momentum of jet 2
        //Q: Why condition? This should be already covered with vetoEvent above
        //if (lightjets.size() > 1)  pjet2 = lightjets[1].momentum();
        if (lightjets.size() > 1)  pjet2 = lightjets[ljets_position.second].momentum();

        double pz = computeneutrinoz(lepton->momentum(), met);
        //Q: What is pseudoneutrino?
        FourMomentum ppseudoneutrino( sqrt(sqr(met.px()) + sqr(met.py()) + sqr(pz)), met.px(), met.py(), pz);

        //compute leptonic, hadronic, combined pseudo-top
        //Q: Why pseudo?
        //Q: A-S Take combination which minimizes ttbar difference, which combi? Only one lepton allowed and requirement on jets with highest pT
        FourMomentum ppseudotoplepton = lepton->momentum() + ppseudoneutrino + pbjet1;
        FourMomentum leptonic_w = lepton->momentum() + ppseudoneutrino;
        FourMomentum ppseudotophadron = pbjet2 + pjet1 + pjet2;
        FourMomentum hadronic_w = pjet1 + pjet2;
        FourMomentum pttbar = ppseudotoplepton + ppseudotophadron;

        //@Note why to keep selection after calculation of top?
        // -> Moved up



        const double weight = 1; //event.weight();

        _h["ttbarmass"]->fill(            pttbar.mass(),             weight); //mass of ttbar
        _h["tmass_leptonic"]->fill(            ppseudotoplepton.mass(),             weight); //mass of ttbar
        _h["tmass_hadronic"]->fill(            ppseudotophadron.mass(),             weight); //mass of ttbar
        _h["tmass"]->fill(         ( ppseudotoplepton.mass() + ppseudotophadron.mass() )/2.0,   weight); //mass of ttbar
        _h["ptw_leptonic"]->fill( leptonic_w.pt() ,weight );
        _h["ptw_hadronic"]->fill( hadronic_w.pt() ,weight );
        _h["ptT_leptonic"]->fill( ppseudotoplepton.pt() ,weight );
        _h["ptT_hadronic"]->fill( ppseudotophadron.pt() ,weight );
        //@Todo isEl isMu tag?
        if(pass_eljets) _h["pt_e"]->fill( lepton->pt() ,weight );
        if(pass_mujets) _h["pt_mu"]->fill( lepton->pt() ,weight );
        _h["pt_l"]->fill( lepton->pt() ,weight );


        //foreach( Jet lj,  lightjets){
        for(unsigned int ilj = 0; ilj < lightjets.size(); ++ilj){
            if( ilj != ljets_position.first && ilj != ljets_position.second ) _h["pt_restljs"]->fill( lightjets[ilj].pt()  , weight );
        }
        _h["lightjetmass"]->fill(  (pjet1 + pjet2).mass() ,weight );
        _h["lightjetmultiplicity"]->fill(  lightjets.size() ,weight );


        _h2["topptcorrelation"]->fill( ppseudotophadron.pt() , ppseudotoplepton.pt(),    weight); //correlation of pt between leptonic and hadronic top

        //Study of Differential jet shape from final jets
        //Q:After all cuts? Made out of 2 bjet and 2 light flavor jets?
        //Q: How big should the Delta_r be?
        //A: According to plot in A-S article (Delta_r / Delta_R) = 0.02
        //@Todo: need to store info from one jet and then do average over jets, is the help hist good solution?
        Jets FinalJets;
        //Selection only from main evnt jets?
        // foreach( Jet j, bjets) FinalJets.push_back( j );
        // foreach( Jet j, lightjets) FinalJets.push_back( j );
        FinalJets.push_back( bjets[0] );
        FinalJets.push_back( bjets[1] );
        FinalJets.push_back( lightjets[ljets_position.first] );
        FinalJets.push_back( lightjets[ljets_position.second] );

        foreach( Jet fj, FinalJets){
            //Q: on the y-axis should be d_rho/d_r, normalization for bin width?
            double JetE = fj.totalEnergy();
            //std::cout << "number of particles in jet "<<  fj.particles().size() << std::endl;
            foreach( Particle p, fj.particles() ){
                //Q: move normalization to finalize?
                _h["hhist_jetshape"]->fill( deltaR( p.momentum(), fj.momentum() )/Delta_R, weight*p.E()/JetE/0.02); //0.02 = bin width normalization
            }

            //Q: top pt from lepton or hadron?
            //A: We are investigating dif jet shape, which comes from jets made by hadrons
            double top_pt =  ppseudotophadron.pt();
            foreach( auto slice, top_pt_slices  ){
                //Fill all top pt
                for(unsigned int iBin = 0; iBin < _h["hhist_jetshape"]->numBins(); ++iBin){
                    _p["diffjetshape"]->fill( _h["hhist_jetshape"]->bins()[iBin].xMid()  ,_h["hhist_jetshape"]->bins()[iBin].area() );
                }
                //Fill slices
                //Q: MeV2GeV?
                //TODO: use cut class?
                if( slice.second < top_pt*GeV ) continue;
                //TODO: nicer way?
                for(unsigned int iBin = 0; iBin < _h["hhist_jetshape"]->numBins(); ++iBin){
                    //TODO: Correct handling of passing all relevant info
                    //TODO: Make it easier/better/nicer
                    _p["diffjetshape" + slice.first + "pt"]->fill( _h["hhist_jetshape"]->bins()[iBin].xMid()  ,_h["hhist_jetshape"]->bins()[iBin].area() );
                }
            }
            _h["hhist_jetshape"]->reset();
        }




        //Q: what about weight??
        _h["lightjetcorrelation"]->fill( deltaR( lightjets[0], lightjets[1]), weight); //0.02 = bin width normalization
        _h["bjetwcorrelation"]->fill( deltaR( pbjet2, hadronic_w) , weight); //0.02 = bin width normalization


        //"Effect of color reconnection on ttbar final states at the LHC" (Sjostrand, Argyropoulos)
        //Plots from Figure 8
        double SumPt = 0;
        double SumPtAll = 0;
        int Nch = 0;
        //@Todo, For now only all charged (strange baryons?)
        foreach(Particle general_particle, _eventparticles[k_AllCharged]){
            bool isUE = true;
            SumPtAll += general_particle.pt();
            //Conditions to assure that the particle is not from the main (e, mu, 2x bjets, 2xlightjets) evnt but underlying event
            if( general_particle == *dynamic_cast<Particle*>(lepton) ) continue;
            foreach( Jet fj, FinalJets){
                foreach( Particle p, fj.particles() ){
                    //@Todo there should be is in jet function
                    if( general_particle != p ) continue;
                    else{
                        isUE = false;
                    }
                    if( !isUE ) break;
                }
                if ( !isUE ) break;
            }
            if( !isUE ) continue;
            SumPt += general_particle.pt();
            Nch += 1;
            _h["pt"]->fill( general_particle.pt(),weight);
        }
        double mean_pt = ( Nch != 0 ) ? SumPt/double(Nch) : 0;
        //std::cout << "SPT: " << SumPt <<std::endl;
        //std::cout << "SPT_All: " << SumPtAll <<std::endl;
        //std::cout << "Nch: " << Nch <<std::endl;
        //std::cout << "Nch_All: " << _eventparticles[k_AllCharged].size() <<std::endl;

        _h["sumpt"]->fill( SumPt, weight);
        _h["nch"]->fill( Nch, weight);
        _p["mpt_nch"]->fill( Nch, mean_pt, weight);
        _p["sumpt_nch"]->fill( Nch, SumPt, weight);
        //_p["sumpt_pt"]->Fill( ,weight);
        //--------------------------------------------------------------------------------------------------------


        //Study of the underlying event,b-quark fragmentation and hadronization properties in ttbar events (CMS collaboration)
        //note: Above (Sjostr.) analysis studied UE as particles not clustered in jets, CMS are introducing regions using pT(ttbar)
        std::vector<double> spt (Nregions, 0); //sum of particle momentum
        std::vector<double> nch (Nregions, 0); //multiplicity
        std::vector<double> mpt (Nregions, 0); //mean pt
        foreach(Particle p, _eventparticles[k_AllCharged]){
            //determine region
            regionID regID = select_region(p, pttbar);
            spt[rAll] += p.pt();
            nch[rAll] += 1;
            switch(regID){
            case rToward:
                spt[rToward] += p.pt();
                nch[rToward] += 1;
                //std::cout << "Toward" << std::endl;
                break;
            case rTransverseMin:
                spt[rTransverseMin] += p.pt();
                nch[rTransverseMin] += 1;
                spt[rTransverse] += p.pt();
                nch[rTransverse] += 1;
                //std::cout << "TransverseMin" << std::endl;
                break;
            case rTransverseMax:
                spt[rTransverseMax] += p.pt();
                nch[rTransverseMax] += 1;
                spt[rTransverse] += p.pt();
                nch[rTransverse] += 1;
                //std::cout << "TransverseMax" << std::endl;
                break;
            case rAway:
                spt[rAway] += p.pt();
                nch[rAway] += 1;
                //std::cout << "Away" << std::endl;
                break;
                //Blank, only to get rid of warnings
                // case rTransverse:
                //    std::cout << "a" << std::endl;
                // case rAll:
                //    std::cout << "b" << std::endl;
                // case Nregions:
                //     std::cout << "c" << std::endl;
            default:
                std::cout << "Error" << std::endl;
            }
        }
        //Need to determine max and min region and swap if necessary because so far they have been treated as left right
        int which_max =  (spt[rTransverseMin] < spt[rTransverseMax] ) ? rTransverseMax : rTransverseMin;
        if( which_max == rTransverseMin ){
            double spt_tmp = spt[rTransverseMin];
            int nch_tmp = nch[rTransverseMin];
            spt[rTransverseMin] = spt[rTransverseMax];
            nch[rTransverseMin] = nch[rTransverseMax];
            spt[rTransverseMax] = spt_tmp;
            nch[rTransverseMax] = nch_tmp;
        }
        //Fill transverse from both contributions (max and min)
        //spt[rTransverse] = spt[rTransverseMax] + spt[rTransverseMin];
        //nch[rTransverse] = nch[rTransverseMax] + nch[rTransverseMin];

        for(int iR = 0; iR < Nregions; ++iR){
            mpt[iR] = (nch[iR] > 0) ? spt[iR]/(double)nch[iR] : 0;
            //std::cout << "Reg: " << iR << " spt: " << spt[iR] << " nch: " << nch[iR] <<  " mpt:"<< mpt[iR] <<std::endl;
            _spt[iR]->fill( spt[iR], weight);
            _nch[iR]->fill( nch[iR], weight);
            _mpt[iR]->fill( mpt[iR], weight);

            _spt_pt[iR]->fill( pttbar.pt(), weight*spt[iR]);
            _nch_pt[iR]->fill( pttbar.pt(), weight*nch[iR]);
            _mpt_pt[iR]->fill( pttbar.pt(), weight*mpt[iR]);
        }
        //--------------------------------------------------------------------------------------------------------

        //@Jet pull vector to study colour flow
        //Pull vectors of bjets and lightjets
        std::vector< std::pair<double, double> > pullVectors;
        foreach( auto iJet,  FinalJets){
            std::pair<double, double> pullVector (0, 0);
            //@Todo try to use angle function in ParticleBase
            //@Todo is there 2D space (eta, phi) in Rivet?
            // Should I rather try to create pullVector as a Fourvector/Vector(2)?
            // -> Motivation use of inside functions(deltaR, ... )
            for( auto iPar : iJet.particles() ){
                double pDRap = deltaRap( iPar, iJet);
                double pDPhi = deltaPhi( iPar, iJet );
                //Magnitude of vector between particle and jet axis in rapidiry scheme
                double pR = deltaR( iPar, iJet, RAPIDITY );
                //@Todo check what is the deal with r magnitude |r_i| in equation
                pullVector.first +=  (iPar.pt()/iJet.pt())*(pR*pDRap);
                pullVector.second +=  (iPar.pt()/iJet.pt())*(pR*pDPhi);
            }
            pullVectors.push_back( pullVector );
        }
        std::vector<double> pullAngles;
        //@Angles between pullVectors from jets and their jet partner vectors
        // pullVector(bjet1) -> axis(bjet2)
        // pullVector(bjet2) -> axis(bjet1)
        // pullVector(ljet1) -> axis(ljet2)
        // pullVector(ljet2) -> axis(ljet1)
        double pullAngle = 0;
        pullAngle = calculate_pullAngle( pullVectors[0].first , pullVectors[0].second, FinalJets[1]);
        pullAngles.push_back(pullAngle);
        _h["pa_b1"]->fill( pullAngle , weight); //fill pT of ttbar in electron channel

        pullAngle = calculate_pullAngle( pullVectors[1].first , pullVectors[1].second, FinalJets[0]);
        pullAngles.push_back(pullAngle);
        _h["pa_b2"]->fill( pullAngle , weight); //fill pT of ttbar in electron channel

        pullAngle = calculate_pullAngle( pullVectors[2].first , pullVectors[2].second, FinalJets[3]);
        pullAngles.push_back(pullAngle);
        _h["pa_lj1"]->fill( pullAngle , weight); //fill pT of ttbar in electron channel

        pullAngle = calculate_pullAngle( pullVectors[3].first , pullVectors[3].second, FinalJets[2]);
        pullAngles.push_back(pullAngle);
        _h["pa_lj2"]->fill( pullAngle , weight); //fill pT of ttbar in electron channel
        //--------------------------------------------------------------------------------------------------------
        //Study of kinematic correlations between jets
        //Q:Are there more than 2 light jets? Check
        // Fill histograms
        //std::cout << pttbar.mass() << std::endl;


        // std::cout << "ERR" << std::endl;
        //UE studies in cone regions around b jets
        //foreach( auto UE_Delta_R, UE_Delta_Rs ){  //Looping over different UE cones
        /*
    for( unsigned int dR = 0; dR < NdR; ++dR ){  //Looping over different UE cones
        for (int iType=0; iType < kNPartTypes; ++iType){
        std::vector<double> Multiplicity(kNregions, 0), SumPt(kNregions, 0.0), AveragePt(kNregions, 0.0);
        foreach(Particle general_particle, _eventparticles[iType]){
            int regID = -1;
            // To assure that particle is within the cone (dR < 1.5) but not inside the jet (dR > 0.4)
            // @Todo check that particles are not within other jets
            if( deltaR( bjets[0], general_particle ) < UE_Delta_Rs[dR] && deltaR( bjets[0], general_particle ) > Delta_R) regID = 1;
            if( deltaR( bjets[1], general_particle ) < UE_Delta_Rs[dR] && deltaR( bjets[1], general_particle ) > Delta_R) regID = 2;
            if(regID == -1) continue;
            //if not killed by previous condition then its clearly in one of the b region
            //First put in "all" region
            ++Multiplicity[0];
            SumPt[0] += general_particle.pT();
            //Separation of regions
            ++Multiplicity[regID];
            SumPt[regID] += general_particle.pT();
            //Q: In GeV or MeV?
            //Q: Which objects states lead pT? hadronic/leptonic top?
        }

        //Compute average pt in event
        //@Todo, iteration over enumerate? Could be also done in a loop of int 0,1,2, ...
        AveragePt[all] = (Multiplicity[all] != 0) ? SumPt[all]/Multiplicity[all] : 0;
        AveragePt[b1_cone] = (Multiplicity[b1_cone] != 0) ? SumPt[b1_cone]/Multiplicity[b1_cone] : 0;
        AveragePt[b2_cone] = (Multiplicity[b2_cone] != 0) ? SumPt[b2_cone]/Multiplicity[b2_cone] : 0;
        for(unsigned int iJetRegion = 0; iJetRegion < kNregions; ++iJetRegion){
            _hist_ptavg_toppt [iType][iJetRegion][dR]->fill( ppseudotophadron.pt()/GeV, AveragePt[iJetRegion]/GeV, weight);
            //_hist_ptavg_nch [iType][iJetRegion][dR]->fill( Multiplicity[iJetRegion], AveragePt[iJetRegion]/GeV, weight);
        }
        }
    }*/
        //--------------------------------------------------------------------------------------------------------

        /*
    //pseudotop hadrons and leptons fill histogram
    _h["ptpseudotoplepton"]->fill(    ppseudotoplepton.pt(),     weight); //pT of pseudo top lepton
    _h["absrappseudotoplepton"]->fill(ppseudotoplepton.absrap(), weight); //absolute rapidity of pseudo top lepton
    _h["ptpseudotophadron"]->fill(    ppseudotophadron.pt(),     weight); //pT of pseudo top hadron
    _h["absrappseudotophadron"]->fill(ppseudotophadron.absrap(), weight); //absolute rapidity of pseudo top hadron
    _h["absrapttbar"]->fill(          pttbar.absrap(),           weight); //absolute rapidity of ttbar
    _h["ttbarmass"]->fill(            pttbar.mass(),             weight); //mass of ttbar
    _h["ptttbar"]->fill(              pttbar.pt(),               weight); //fill pT of ttbar in combined channel

    if (pass_eljets) { // electron channel fill histogram
    _h["ptpseudotoplepton_el"]->fill(    ppseudotoplepton.pt(),     weight); //pT of pseudo top lepton
    _h["absrappseudotoplepton_el"]->fill(ppseudotoplepton.absrap(), weight); //absolute rapidity of pseudo top lepton
    _h["ptpseudotophadron_el"]->fill(    ppseudotophadron.pt(),     weight); //pT of pseudo top hadron
    _h["absrappseudotophadron_el"]->fill(ppseudotophadron.absrap(), weight); //absolute rapidity of pseudo top hadron
    _h["absrapttbar_el"]->fill(          pttbar.absrap(),           weight); //absolute rapidity of ttbar
    _h["ttbarmass_el"]->fill(            pttbar.mass(),             weight); // fill electron channel ttbar mass
    _h["ptttbar_el"]->fill(              pttbar.pt(),               weight); //fill pT of ttbar in electron channel
    }
    else { // muon channel fill histogram
    _h["ptpseudotoplepton_mu"]->fill(    ppseudotoplepton.pt(),     weight); //pT of pseudo top lepton
    _h["absrappseudotoplepton_mu"]->fill(ppseudotoplepton.absrap(), weight); //absolute rapidity of pseudo top lepton
    _h["ptpseudotophadron_mu"]->fill(    ppseudotophadron.pt(),     weight); //pT of pseudo top hadron
    _h["absrappseudotophadron_mu"]->fill(ppseudotophadron.absrap(), weight); //absolute rapidity of pseudo top hadron
    _h["absrapttbar_mu"]->fill(          pttbar.absrap(),           weight); //absolute rapidity of ttbar
    _h["ttbarmass_mu"]->fill(            pttbar.mass(),             weight); //fill muon channel histograms
    _h["ptttbar_mu"]->fill(              pttbar.pt(),               weight); //fill pT of ttbar in electron channel
    }
     */





    }
    void finalize() {
        // Normalize to cross-section
        const double scalefactor(crossSection() / sumOfWeights());
        for (map<string, Histo1DPtr>::iterator hit = _h.begin(); hit != _h.end(); ++hit) {
            double sf = scalefactor;
            //Q: only hist with uderscore scaled by 0.5 -> referring to single contribution of muons and electrons?
            if ( (hit->first).find("_") == std::string::npos )  sf *= 0.5;
            scale(hit->second, sf);
        }
    }
private:
    const double Delta_R = 0.4;
    //Q: Calculate the number of highest dif
    //A: dR = sqrt( 5^2 + (2pi)^2 )

    //Calculate a pullangle between pull vector and partner jet
    double calculate_pullAngle(double rap, double phi, Jet jet){
        double r1 = sqrt( pow(rap, 2) + pow(phi, 2) );
        double r2 = sqrt( pow(jet.rapidity(), 2) + pow(jet.phi(), 2) );
        return acos( rap/r1 ) - acos(jet.rapidity()/r2);
    }


    /// Set an integer region flag on the object, 0-3 indicating that it's in the toward, transL, transR, or away region respectively
    regionID select_region(const Particle particle, const FourMomentum ttbar ) {
        //@Rivet does not provide deltaPhi()function in other ranges than 0-pi?
        double dphi = particle.phi() - ttbar.phi(); //phi() by default in 0-2pi region
        //convert dphi to (-pi, pi) interval
        if(dphi > M_PI ) dphi -= 2*M_PI;
        else if(dphi < - M_PI) dphi += 2*M_PI;
        const double adphi = fabs(dphi);
        assert(adphi <= M_PI);

        // Identify region
        regionID reg = Nregions;
        if (adphi < TO_ANGLE) {
            reg = rToward; //< towards
        } else if (adphi >= TO_ANGLE && adphi < AW_ANGLE) {
            //@note!!! Here is the selection to min and max missleading
            //It is rather selected to transverseleft and transverseright
            //But to keep the same numbering as in regionID min max are used
            reg = (dphi < 0) ? rTransverseMin : rTransverseMax; //< trans (L/R)
        } else if (adphi >= AW_ANGLE) {
            reg = rAway; //< away
        }
        //std::cout << "DeltaPhi: " << dphi << " Reg: "<< reg << std::endl;
        //std::cout << "ParticlePhi: " << particle.phi() << " ttbarphi: "<< ttbar.phi() << " DeltaPhi: " << dphi << " Reg: "<< reg << std::endl;
        assert(reg != Nregions);
        return reg;
    }


    double computeneutrinoz(const FourMomentum& lepton, FourMomentum& met) const {
        //computing z component of neutrino momentum given lepton and met
        double pzneutrino;
        double m_W = 80.399; // in GeV, given in the paper
        //Q: Check the computation!!
        double k = (( sqr( m_W ) - sqr( lepton.mass() ) ) / 2 ) + (lepton.px() * met.px() + lepton.py() * met.py());
        double a = sqr ( lepton.E() )- sqr ( lepton.pz() );
        double b = -2*k*lepton.pz();
        double c = sqr( lepton.E() ) * sqr( met.pT() ) - sqr( k );
        double discriminant = sqr(b) - 4 * a * c;
        double quad[2] = { (- b - sqrt(discriminant)) / (2 * a), (- b + sqrt(discriminant)) / (2 * a) }; //two possible quadratic solns
        if (discriminant < 0)  pzneutrino = - b / (2 * a); //if the discriminant is negative
        else { //if the discriminant is greater than or equal to zero, take the soln with smallest absolute value
            double absquad[2];
            for (int n=0; n<2; ++n)  absquad[n] = fabs(quad[n]);
            if (absquad[0] < absquad[1])  pzneutrino = quad[0];
            else                          pzneutrino = quad[1];
        }
        if ( !std::isfinite(pzneutrino) )  std::cout << "Found non-finite value" << std::endl;
        return pzneutrino;
    }

    double _mT(const FourMomentum &l, FourMomentum &nu) const {
        return sqrt( 2 * l.pT() * nu.pT() * (1 - cos(deltaPhi(l, nu))) );
    }

    /// @name Objects that are used by the event selection decisions
    vector<DressedLepton> _dressedelectrons, _vetodressedelectrons, _dressedmuons, _vetodressedmuons;
    Particles _neutrinos;

    //TODO: convert maps to arrays?
    map<string, Histo1DPtr> _h;
    map<string, Profile1DPtr> _p;
    map<string, Histo2DPtr> _h2;
    Histo1DPtr cut_flow;


    Histo1DPtr _spt[Nregions];
    Histo1DPtr _nch[Nregions];
    Histo1DPtr _mpt[Nregions];
    Profile1DPtr _spt_pt[Nregions];
    Profile1DPtr _nch_pt[Nregions];
    Profile1DPtr _mpt_pt[Nregions];

    //Profile1DPtr _hist_ptavg_toppt [kNPartTypes][kNregions][NdR];
    //Profile1DPtr _hist_ptavg_nch [kNPartTypes][kNregions][NdR];



    std::vector<double> ue_ptbinning(){
        std::vector<double> binning; binning.reserve(16);
        for(unsigned int i=0; i <= 100;++i) binning.push_back(i);
        //for(unsigned int i=0; i <= 15;++i) binning.push_back( 25 + i*5);
        /*
    std::vector<double> binning; binning.reserve(27);
    for(unsigned int i=0; i <= 14;++i) binning.push_back( 1 + i*0.5);
    for(unsigned int i=1; i <= 6;++i) binning.push_back( 8 + i);
    for(unsigned int i=1; i <= 3;++i) binning.push_back(14 + i*2);
    for(unsigned int i=1; i <= 2;++i) binning.push_back(20 + i*5);
    for(unsigned int i=1; i <= 1;++i) binning.push_back(30 + i*20); //loop because of often changes in last bins
    */

        return binning;
    }

    //Q: Better to pass the whole event (all particles) or passing just one particle
    //Create new jet with and then check if the particle is in only one of them,
    // or just checking deltaR?
    void assing_to_region(Particle& p){


        return;
    }

};


const string ATLAS_2016_UETop::partTypeStr[] = {"allCharged", "noStrange"};
const string ATLAS_2016_UETop::regionStr  [] = {"all", "to", "tr", "trmin", "trmax", "aw"};
const int    ATLAS_2016_UETop::nBin_nch     = 50;
const double    ATLAS_2016_UETop::Bin_nchMin = -0.5;
const double    ATLAS_2016_UETop::Bin_nchMax =  100;
const double    ATLAS_2016_UETop::w_mass = 80.385;
// Angular region definition: TR_SIZE is half the angle of the total transverse region angle
const double ATLAS_2016_UETop::TR_SIDE_SIZE = M_PI/3.0; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
const double ATLAS_2016_UETop::TR_SIZE = 2*TR_SIDE_SIZE; // total transverse size i.e. 60 degrees/side => 120 degrees transverse region
const double ATLAS_2016_UETop::TO_ANGLE = M_PI/2.0 - TR_SIDE_SIZE/2.0; //< dphi at which TO region ends and TR begins
const double ATLAS_2016_UETop::TO_SIZE = 2*TO_ANGLE; // total toward size
const double ATLAS_2016_UETop::AW_ANGLE = M_PI/2.0 + TR_SIDE_SIZE/2.0; //< dphi at which TR region ends and AW begins
const double ATLAS_2016_UETop::AW_SIZE = 2*(M_PI - AW_ANGLE); // total away size


//const  int    ATLAS_2016_UETop::;


// The hook for the plugin system
DECLARE_RIVET_PLUGIN(ATLAS_2016_UETop);

}



