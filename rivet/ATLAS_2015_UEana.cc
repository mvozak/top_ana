// -*- C++ -*-
#include <sstream> 

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"

namespace Rivet {


  class ATLAS_2015_UEana : public Analysis {
  public:

     //particle types included 
    enum partTypes {
      k_AllCharged,
   //   k_NoStrange,
      kNPartTypes
    };

   enum regionID {
      kToward  = 0,
      kAway,
      kTrans,
      kTransMax,
      kTransMin,
      kTransDiff,
      kNregions
    };

    //labels for different particle types
    const static string partTypeStr[kNPartTypes];
    const static string regionStr  [kNregions];

    const static int    kNmoments    = 4;
    const static int    kNptleadCuts = 4;
    //const static string ptCutStr   [kNptleadCuts];
    const static double ptCut      [kNptleadCuts];

    const static double convRadDeg;

    const static int    nBin_pt     ; 
    const static double rangeMin_pt ;
    const static double rangeMax_pt ;
    
    const static int    nBin_nch     ;
    const static double rangeMin_nch ;
    const static double rangeMax_nch ;
    
    const static int    nBin_dphi     ;
    const static double rangeMin_dphi ;
    const static double rangeMax_dphi ;//in degrees Rivet::pi;
    
    const static int    nBin_eta     ;
    const static double rangeMin_eta ;
    const static double rangeMax_eta ;
        

    ATLAS_2015_UEana() : Analysis("ATLAS_2015_UEana") {    
      for (int iT=0; iT < kNPartTypes; ++iT) _sumw500[iT] = 0.0 ;
    }


    void init() {

      const ChargedFinalState cfs500(-2.5, 2.5, 500*MeV);
      addProjection(cfs500, "CFS500");

      //const ChargedFinalState cfslead(-2.5, 2.5, 1.0*GeV);
      //addProjection(cfslead, "CFSlead");

      for (int iT=0; iT < kNPartTypes; ++iT) {

	for (int iR=0; iR < kNregions; ++iR) {
	  // Nch profiles vs. pT_lead (Fig.3)
	  _hist_nch  [iT][iR][0]    = bookProfile1D( partTypeStr[iT] + "_nchpt_"   + regionStr[iR]   , nBin_pt, rangeMin_pt, rangeMax_pt);
	  // pTsum profiles vs. pT_lead (Fig.4)
	  _hist_ptsum[iT][iR][0]    = bookProfile1D( partTypeStr[iT] + "_sumptpt_" + regionStr[iR]   , nBin_pt, rangeMin_pt, rangeMax_pt);

	  // Standard deviation profiles vs. pT_lead
	  // First the higher moments of main profiles to calculate variance and error on variance...
	  // need 4 orders of moments for stddev with errors
	  for (int iMom = 1; iMom < kNmoments; ++iMom) {
	    stringstream ss; 
	    ss << iMom ;
	    _hist_nch     [iT][iR][iMom] = bookProfile1D( partTypeStr[iT] + "_nchpt_"   + regionStr[iR] + "_mom" + ss.str() , nBin_pt, rangeMin_pt, rangeMax_pt);
	    _hist_ptsum   [iT][iR][iMom] = bookProfile1D( partTypeStr[iT] + "_sumptpt_" + regionStr[iR] + "_mom" + ss.str() , nBin_pt, rangeMin_pt, rangeMax_pt);
	  }
	  // Then the data point sets into which the results will be inserted (Fig.5)
	  _dps_sdnch   [iT][iR]  = bookScatter2D( partTypeStr[iT] + "_sdnch_"    + regionStr[iR]  , nBin_pt, rangeMin_pt, rangeMax_pt);
	  _dps_sdptsum [iT][iR]  = bookScatter2D( partTypeStr[iT] + "_sdptsum_"  + regionStr[iR]  , nBin_pt, rangeMin_pt, rangeMax_pt);

	  // <pT> profiles vs pT_lead (Fig.6)
	  _hist_ptavg  [iT][iR]  = bookProfile1D( partTypeStr[iT] + "_ptavgpt_"  + regionStr[iR]  , nBin_pt, rangeMin_pt, rangeMax_pt);
	  
	  // <pT> profiles vs. Nch (Fig.7)
	  for (int iC=0; iC < kNptleadCuts; ++iC) {
	    stringstream cs; 
	    cs << int(ptCut[iC])  ;
	    _hist_dn_dpt        [iT][iR][iC]  = bookProfile1D( partTypeStr[iT] + "_ptavgnch_"    + regionStr[iR] + "_" + cs.str() , nBin_nch, rangeMin_nch, rangeMax_nch);
	    _hist_dn_dpt2       [iT][iR][iC]  = bookProfile1D( partTypeStr[iT] + "_ptavgnch2_"   + regionStr[iR] + "_" + cs.str() , nBin_nch, rangeMin_nch, rangeMax_nch);
       // _hist_dn_dpt_nchTot [iT][iR][iC]  = bookProfile1D( partTypeStr[iT] + "_ptavgnchTot_" + regionStr[iR] + "_" + cs.str() , nBin_nch, rangeMin_nch, rangeMax_nch);
	  }

          ////vs. nch total
	  for (int iC=0; iC < kNptleadCuts; ++iC) {
	    stringstream cs; 
	    cs << int(ptCut[iC])  ;
           // _hist_nch_nchTot   [iT][iR][iC] = bookProfile1D( partTypeStr[iT] + "_nch_nchtot_"   + regionStr[iR]   + "_" + cs.str() , nBin_nch, rangeMin_nch, rangeMax_nch);
           // _hist_ptsum_nchTot [iT][iR][iC] = bookProfile1D( partTypeStr[iT] + "_sumpt_nchtot_" + regionStr[iR]   + "_" + cs.str() , nBin_nch, rangeMin_nch, rangeMax_nch);
          }

	  // pT/Nch profiles vs. eta_lead (Fig.12)
     // _hist_nch_vs_eta  [iT][iR] = bookProfile1D( partTypeStr[iT] + "_ncheta_"   + regionStr[iR], nBin_eta, rangeMin_eta, rangeMax_eta);
    //  _hist_ptsum_vs_eta[iT][iR] = bookProfile1D( partTypeStr[iT] + "_sumpteta_" + regionStr[iR], nBin_eta, rangeMin_eta, rangeMax_eta);
	} //end loop over regions

	for (int iC=0; iC < kNptleadCuts; ++iC) {
	  stringstream cs; 
	  cs << int(ptCut[iC])  ;
	  // Nch vs. Delta(phi) profiles (Fig.8)
	  _hist_N_vs_dPhi[iT][iC]  = bookProfile1D( partTypeStr[iT] + "_nchdphi_"   + cs.str(), nBin_dphi, rangeMin_dphi, rangeMax_dphi);

	  // pT vs. Delta(phi) profiles (Fig.9)
	  _hist_pT_vs_dPhi[iT][iC] = bookProfile1D( partTypeStr[iT] + "_sumptdphi_" + cs.str(), nBin_dphi, rangeMin_dphi, rangeMax_dphi);
	}

	_hist_ptLead   [iT] = bookHisto1D ( partTypeStr[iT] + "_ptLead" , nBin_pt, rangeMin_pt, rangeMax_pt);
      } //end loop over particle types
      
      _hist_NevTotal   = bookHisto1D  ( "NevTotal"    ,  1, 0., 1.);
      _hist_sumwTotal  = bookHisto1D  ( "sumwTotal"   ,  1, 0., 1.);
	
    } 


    void analyze(const Event& event) {

      const double weight = event.weight();

      _hist_NevTotal ->fill( 0.5, 1.0);
      _hist_sumwTotal->fill( 0.5, weight);

      // These are the charged particles (tracks) with pT > 500 MeV
      const ChargedFinalState& charged500 = applyProjection<ChargedFinalState>(event, "CFS500");

      MSG_DEBUG("charged500 size : " << charged500.size() );

      Particles particles[kNPartTypes];
      particles[k_AllCharged] = charged500.particlesByPt();

      MSG_DEBUG("particlesAll size : " << particles[k_AllCharged].size() );
 
      MSG_DEBUG("removing strange baryons : "  );
      //remove strange baryons, pt ordering should be preserved
      /*
      if (particles[k_AllCharged].size() > 0) {
	foreach (const Particle& p, particles[k_AllCharged]) {
	  PdgId pdg = p.abspid ();
	  if ( pdg == 3112 || // Sigma-
	       pdg == 3222 || // Sigma+
	       pdg == 3312 || // Xi-
	       pdg == 3334 )  // Omega-
	    continue;
	  particles[k_NoStrange].push_back( p );
	}
      }

      MSG_DEBUG("noStrange baryons size: " <<  particles[k_NoStrange].size() );
        */
      //--------------------------------------------------------
      //run full procedure for both all and noStrange particles
      //--------------------------------------------------------
      for (int iPartType = 0; iPartType < kNPartTypes; ++iPartType)  {

	//plot pT lead already from 500MeV
	if (particles[iPartType].size() >= 1) {
	  _sumw500[iPartType] += weight;
	  _hist_ptLead[iPartType]->fill( particles[iPartType][0].perp()/GeV, weight);
	}

	// Require at least one track in the event with pT >= 1 GeV
	if (particles[iPartType].size() < 1)        continue;
	if (particles[iPartType][0].perp()/GeV < 1) continue;

	// Identify leading track and its phi and pT 
	Particle p_lead      = particles[iPartType][0];
	const double philead = p_lead.phi();
	const double etalead = p_lead.eta();
	const double pTlead  = p_lead.perp();
	MSG_DEBUG("Leading track: pT = " << pTlead << ", eta = " << etalead << ", phi = " << philead);

	// Iterate over all particles and count particles and scalar pTsum in three basic regions
	vector<double> num(kNregions, 0), ptSum(kNregions, 0.0), avgpt(kNregions, 0.0);
	// Temporary histos that bin Nch and pT in dPhi.
	Histo1D hist_num_dphi(nBin_dphi, rangeMin_dphi, rangeMax_dphi,"","hist_num_dphi");
	Histo1D hist_pt_dphi (nBin_dphi, rangeMin_dphi, rangeMax_dphi,"","hist_pt_dphi" );

	int tmpnch[2] = {0,0};
	double tmpptsum[2] = {0,0};

	foreach (const Particle& p, particles[iPartType]) {
	  const double pT   = p.pT();
	  const double dPhi = deltaPhi(philead, p.phi());  //inrange (0,pi)
	  const int ir = region_index(dPhi); //gives just toward/away/trans

	  //toward/away/trans region: just count 
	  num  [ir] += 1;
	  ptSum[ir] += pT;
	  
	  //determine which transverse side
	  if (ir == kTrans) {
	    const size_t iside = (mapAngleMPiToPi(p.phi() - philead) > 0) ? 0 : 1;
	    tmpnch  [iside] += 1;
	    tmpptsum[iside] += p.pT();
	  }
	  
	  // Fill temp histos to bin Nch and pT in dPhi
	  if (p.genParticle() != p_lead.genParticle()) { // We don't want to fill all those zeros from the leading track...
	    hist_num_dphi.fill(dPhi * 180. / Rivet::pi, 1);
	    hist_pt_dphi .fill(dPhi * 180. / Rivet::pi, pT);
	  }
	}

	//construct max/min/diff regions
	int numTotal = particles[iPartType].size() ;
	num[kTransMax ]   = std::max(tmpnch[0], tmpnch[1]);
	num[kTransMin ]   = std::min(tmpnch[0], tmpnch[1]);
	num[kTransDiff]   = num[kTransMax ] - num[kTransMin ];
	ptSum[kTransMax ] = std::max(tmpptsum[0], tmpptsum[1]);
	ptSum[kTransMin ] = std::min(tmpptsum[0], tmpptsum[1]);
	ptSum[kTransDiff] = ptSum[kTransMax ] - ptSum[kTransMin ];
	avgpt[kToward]    = (num[kToward] > 0 ) ? ptSum[kToward] / num[kToward] : 0. ;
	avgpt[kAway]      = (num[kAway  ] > 0 ) ? ptSum[kAway]   / num[kAway]   : 0. ;
	avgpt[kTrans]     = (num[kTrans ] > 0 ) ? ptSum[kTrans]  / num[kTrans]  : 0. ;
	//avg pt max/min regions determined according sumpt max/min
	int sumptMaxRegID = (tmpptsum[0] >  tmpptsum[1]) ? 0 : 1 ; 
	int sumptMinRegID = (sumptMaxRegID == 0) ? 1 : 0; 
	avgpt[kTransMax ] = (tmpnch[sumptMaxRegID] > 0) ? tmpptsum[sumptMaxRegID] / tmpnch[sumptMaxRegID] : 0.;
	avgpt[kTransMin ] = (tmpnch[sumptMinRegID] > 0) ? tmpptsum[sumptMinRegID] / tmpnch[sumptMinRegID] : 0.;
	avgpt[kTransDiff] = ((tmpnch[sumptMaxRegID] > 0) && (tmpnch[sumptMinRegID] > 0)) ? avgpt[kTransMax ] - avgpt[kTransMin ] : 0.;
	
	// Now fill underlying event histograms

	// The densities are calculated by dividing the UE properties by dEta*dPhi
	// -- each basic region has a dPhi of 2*PI/3 and dEta is two times 2.5
	// min/max/diff regions are only half of that
	//be carefull when changing the order of regions??
	const double dEtadPhi[kNregions] = { 2*2.5 * 2*PI/3.0, 2*2.5 * 2*PI/3.0, 2*2.5 * 2*PI/3.0, 
					     2*2.5 *   PI/3.0, 2*2.5 *   PI/3.0, 2*2.5 *   PI/3.0 };

	for (int iR=0; iR < kNregions; ++iR) {
	  
	  for (int iM = 0; iM < kNmoments; ++iM) {
	    _hist_nch  [iPartType][iR][iM]->fill(pTlead/GeV, intpow(  num[iR]    /dEtadPhi[iR]      , iM+1), weight);
	    _hist_ptsum[iPartType][iR][iM]->fill(pTlead/GeV, intpow(ptSum[iR]    /GeV/dEtadPhi[iR]  , iM+1), weight);
	  }

	  // <pT> profiles vs. pT_lead
	  // <pT> profiles vs. Nch
	  //two different definitions possible for transMin/Max regions (well, actually 3 when using whole event Nch :) )
	  //using for now as a function of num of particles in total transverse region
	  switch (iR) 
	    {
	      //first 3 are the same!!!
	    case kToward    : 
	    case kAway      : 
	    case kTrans     : 
	      if (num[iR] > 0) {
		_hist_ptavg [iPartType][iR] ->fill(pTlead/GeV, avgpt[iR] /GeV, weight); 
	      }
	      break;
	    case kTransMax  : 
	      if (tmpnch[sumptMaxRegID] > 0) {
		_hist_ptavg [iPartType][iR] ->fill(pTlead/GeV , avgpt[iR] /GeV, weight); 
	      }
	      break;
	    case kTransMin  : 
	      if (tmpnch[sumptMinRegID] > 0) {
		_hist_ptavg [iPartType][iR] ->fill(pTlead/GeV , avgpt[iR] /GeV, weight);
	      }
	      break;
	    case kTransDiff : 
	      if ((tmpnch[sumptMaxRegID] > 0) && (tmpnch[sumptMinRegID] > 0)) {
		_hist_ptavg[iPartType][iR]  ->fill(pTlead/GeV , avgpt[iR] /GeV, weight); 
	      }
	      break;
	    default : //should not get here!!!
	      MSG_INFO("unknown region in <pT> profiles vs.pt lead switch!!! : " <<  iR);
	    } //end switch over regions

	  //the same as above dependence on pTlead but for Nch
	  //here, using different cuts on ptlead (does not make sense in the above)
	  for (int iC=0; iC < kNptleadCuts; ++iC) {

	    if (pTlead/GeV >= ptCut[iC]) {

         // _hist_nch_nchTot   [iPartType][iR][iC] ->fill(numTotal, num  [iR], weight);
         // _hist_ptsum_nchTot [iPartType][iR][iC] ->fill(numTotal, ptSum[iR], weight);

	      switch (iR) 
		{
		  //first 3 are the same!!!
		case kToward    : 
		case kAway      : 
		case kTrans     : 
		  if (num[iR] > 0) {
		    _hist_dn_dpt       [iPartType][iR][iC] ->fill(num[iR]   , avgpt[iR] /GeV, weight); 
           // _hist_dn_dpt_nchTot[iPartType][iR][iC] ->fill(numTotal  , avgpt[iR] /GeV, weight);
		  }
		  break;
		case kTransMax  : 
		  if (tmpnch[sumptMaxRegID] > 0) {
		    _hist_dn_dpt       [iPartType][iR][iC] ->fill(num[kTrans]          , avgpt[iR] /GeV, weight); 
		    _hist_dn_dpt2      [iPartType][iR][iC] ->fill(tmpnch[sumptMaxRegID], avgpt[iR] /GeV, weight); 
           // _hist_dn_dpt_nchTot[iPartType][iR][iC] ->fill(numTotal             , avgpt[iR] /GeV, weight);
		  }
		  break;
		case kTransMin  : 
		  if (tmpnch[sumptMinRegID] > 0) {
		    _hist_dn_dpt       [iPartType][iR][iC] ->fill(num[kTrans]          , avgpt[iR] /GeV, weight);
		    _hist_dn_dpt2      [iPartType][iR][iC] ->fill(tmpnch[sumptMinRegID], avgpt[iR] /GeV, weight);
           // _hist_dn_dpt_nchTot[iPartType][iR][iC] ->fill(numTotal             , avgpt[iR] /GeV, weight);
		  }
		  break;
		case kTransDiff : 
		  if ((tmpnch[sumptMaxRegID] > 0) && (tmpnch[sumptMinRegID] > 0)) {
		    _hist_dn_dpt       [iPartType][iR][iC] ->fill(num[kTrans]          , avgpt[iR] /GeV, weight); 
		    _hist_dn_dpt2      [iPartType][iR][iC] ->fill(tmpnch[sumptMinRegID], avgpt[iR] /GeV, weight);
            //_hist_dn_dpt_nchTot[iPartType][iR][iC] ->fill(numTotal             , avgpt[iR] /GeV, weight);
		  }
		  break;
		default : //should not get here!!!
		  MSG_INFO("unknown region in <pT> profiles vs. nch switch!!! : " <<  iR);
		} //end switch over regions
	    } //end if ptlead cut
	  } //end loop over pt lead cuts

	  //Nch and pT vs eta_lead profiles   
	  if (pTlead/GeV > ptCut[kNptleadCuts-1]) {
      //  _hist_nch_vs_eta   [iPartType][iR]  ->fill(etalead, num  [iR]      /dEtadPhi[iR], weight);
       // _hist_ptsum_vs_eta [iPartType][iR]  ->fill(etalead, ptSum[iR]  /GeV/dEtadPhi[iR], weight);
	  }
	} //end loop over regions
	
      
	// Update the "proper" dphi profile histograms
	// Note that we fill dN/dEtadPhi: dEta = 2*2.5, dPhi = 2*PI/nBins
	// The values tabulated in the note are for an (undefined) signed Delta(phi) rather than
	// |Delta(phi)| and so differ by a factor of 2: we have to actually norm for angular range = 2pi
	const double dEtadPhi2 = (2*2.5 * 2);
      
	for (int i = 0; i < nBin_dphi; ++i) {  

	  // First Nch
	  double mean = hist_num_dphi.bin(i).xMid() ;
	  double value = 0.;
	  if (hist_num_dphi.bin(i).numEntries() > 0) {
	    mean  = hist_num_dphi.bin(i).xMean() ;
	    value = hist_num_dphi.bin(i).area()/hist_num_dphi.bin(i).xWidth()/dEtadPhi2;
	}
	  for (int iC=0; iC < kNptleadCuts; ++iC) {
	    if (pTlead/GeV >= ptCut[iC]) _hist_N_vs_dPhi[iPartType][iC] ->fill(mean, value, weight);
	  }
	
	  // Then pT
	  mean = hist_pt_dphi.bin(i).xMid() ;
	  value = 0.;
	  if (hist_pt_dphi.bin(i).numEntries() > 0) {
	    mean  = hist_pt_dphi.bin(i).xMean() ;
	    value = hist_pt_dphi.bin(i).area()/hist_pt_dphi.bin(i).xWidth()/dEtadPhi2;
	  }
	  for (int iC=0; iC < kNptleadCuts; ++iC) {
	    if (pTlead/GeV >= ptCut[iC]) _hist_pT_vs_dPhi[iPartType][iC] ->fill(mean, value, weight);
	  }
	} //end loop over bins
	
      }//end loop over particle types

    }


    void finalize() {

      // Convert the various moments of the pT and Nch distributions to std devs with correct error
      for (int iT=0; iT < kNPartTypes; ++iT) {

	for (int iR=0; iR < kNregions; ++iR) {
	  _moments_to_stddev(_hist_nch  [iT][iR]  , _dps_sdnch  [iT][iR]);
	  _moments_to_stddev(_hist_ptsum[iT][iR]  , _dps_sdptsum[iT][iR]);
	}
	
	//pt lead histos
	_hist_ptLead_notscaled[iT] = Histo1DPtr( new Histo1D( *_hist_ptLead[iT], "/" + this->name() + "/" + partTypeStr[iT] + "_ptLead_notscaled") );
	addAnalysisObject( _hist_ptLead_notscaled[iT] );
	
	scale(_hist_ptLead[iT], 1.0/_sumw500[iT]);
      }
    }


  private:


    // Little helper function to identify basic Delta(phi) regions: toward/away/trans
    inline int region_index(double dphi) {
      assert(inRange(dphi, 0.0, PI, CLOSED, CLOSED));
      if (dphi < PI/3.0)   return kToward;
      if (dphi < 2*PI/3.0) return kTrans;
      return kAway;
    }


    inline void _moments_to_stddev(Profile1DPtr moment_profiles[], Scatter2DPtr target_dps) {

      for (size_t b = 0; b < moment_profiles[0]->numBins(); ++b) { // loop over points
        /// @todo Assuming unit weights here! Should use N_effective = sumW**2/sumW2??
        const double numentries = moment_profiles[0]->bin(b).numEntries();
        const double x  = moment_profiles[0]->bin(b).xMid();
        const double ex = moment_profiles[0]->bin(b).xWidth()/2.;
        double var = 0.;
        double sd  = 0.;
        if (numentries > 0) {
          var = moment_profiles[1]->bin(b).mean() - intpow(moment_profiles[0]->bin(b).mean(), 2);
          sd = fuzzyLessEquals(var,0.) ? 0 : sqrt(var); ///< Numerical safety check
        }
        if (sd == 0 || numentries < 3) {
          MSG_WARNING("Need at least 3 bin entries and a non-zero central value to calculate "
                      << "an error on standard deviation profiles (bin " << b << ")");
          target_dps->addPoint(x, sd, ex, 0);
          continue;
        }
        // c2(y) = m4(x) - 4 m3(x) m1(x) - m2(x)^2 + 8 m2(x) m1(x)^2 - 4 m1(x)^4
        const double var_on_var = moment_profiles[3]->bin(b).mean()
          - 4 * moment_profiles[2]->bin(b).mean() * moment_profiles[0]->bin(b).mean()
          - intpow(moment_profiles[1]->bin(b).mean(), 2)
          + 8 * moment_profiles[1]->bin(b).mean() * intpow(moment_profiles[0]->bin(b).mean(), 2)
          - 4 * intpow(moment_profiles[0]->bin(b).mean(), 4);
        const double stderr_on_var = sqrt(var_on_var/(numentries-2.0));
        const double stderr_on_sd = stderr_on_var / (2.0*sd);
        target_dps->addPoint(x, sd, ex, stderr_on_sd);
      }
    }


  private:

    //vs. pt lead
    Profile1DPtr _hist_nch   [kNPartTypes][kNregions][kNmoments];
    Profile1DPtr _hist_ptsum [kNPartTypes][kNregions][kNmoments];
    Profile1DPtr _hist_ptavg [kNPartTypes][kNregions];
    //vs. nch
    Profile1DPtr _hist_dn_dpt       [kNPartTypes][kNregions][kNptleadCuts];
    Profile1DPtr _hist_dn_dpt2      [kNPartTypes][kNregions][kNptleadCuts];
//    Profile1DPtr _hist_dn_dpt_nchTot[kNPartTypes][kNregions][kNptleadCuts];
    //vs. nch total
 //   Profile1DPtr _hist_nch_nchTot   [kNPartTypes][kNregions][kNptleadCuts];
 //   Profile1DPtr _hist_ptsum_nchTot [kNPartTypes][kNregions][kNptleadCuts];

    //vs. eta lead
 //   Profile1DPtr _hist_nch_vs_eta  [kNPartTypes][kNregions];
 //   Profile1DPtr _hist_ptsum_vs_eta[kNPartTypes][kNregions];

    Scatter2DPtr  _dps_sdnch  [kNPartTypes][kNregions];
    Scatter2DPtr  _dps_sdptsum[kNPartTypes][kNregions];

    Profile1DPtr _hist_N_vs_dPhi [kNPartTypes][kNptleadCuts];
    Profile1DPtr _hist_pT_vs_dPhi[kNPartTypes][kNptleadCuts];

    Histo1DPtr   _hist_ptLead[kNPartTypes];
    Histo1DPtr   _hist_ptLead_notscaled[kNPartTypes];

    //useful hists
    Histo1DPtr   _hist_sumwTotal;
    Histo1DPtr   _hist_NevTotal;

    double _sumw500[kNPartTypes];

    

  };

  //constants
  //const string ATLAS_2015_UEana::partTypeStr[] = {"allCharged", "noStrange"};
  const string ATLAS_2015_UEana::partTypeStr[] = {"allCharged"};

  const string ATLAS_2015_UEana::regionStr  [] = {"toward", "away", "trans", "transMax", "transMin", "transDiff"};
  //const string ATLAS_2015_UEana::ptCutStr   [] = {"1", "3", "5", "10"};
  const double ATLAS_2015_UEana::ptCut      [] = { 1., 3., 5., 10.};

  //const double ATLAS_2015_UEana::convRadDeg =  180. / Rivet::pi;

  const  int    ATLAS_2015_UEana::nBin_pt     = 200; 
  const  double ATLAS_2015_UEana::rangeMin_pt = 0.;
  const  double ATLAS_2015_UEana::rangeMax_pt = 50.;
  
  const  int    ATLAS_2015_UEana::nBin_nch     = 50;
  const  double ATLAS_2015_UEana::rangeMin_nch = -0.5;
  const  double ATLAS_2015_UEana::rangeMax_nch = 49.5;
  
  const  int    ATLAS_2015_UEana::nBin_dphi     = 180;
  const  double ATLAS_2015_UEana::rangeMin_dphi = 0.0;
  const  double ATLAS_2015_UEana::rangeMax_dphi = 180.;//in degrees Rivet::pi;
  
  const  int    ATLAS_2015_UEana::nBin_eta     = 100;
  const  double ATLAS_2015_UEana::rangeMin_eta = 0.;
  const  double ATLAS_2015_UEana::rangeMax_eta = 2.5;

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2015_UEana);
}
