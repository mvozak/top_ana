#!/usr/bin/env python
#This script purpose is to export finished jobs to plot directory and rename them according to colour reconnection schemes
import os
from os import listdir  #to list directories
from os.path import isdir, join  #check existence of dir, join paths


#This can be done already in pythia8 generation
def get_name( outname ):
   modeDict[ rivOutname + '0' ] = 'cr_off'
   modeDict[ rivOutname + '1' ] = 'default_resOff'
   modeDict[ rivOutname + '2' ] = 'default_resOn'
   modeDict[ rivOutname + '3' ] = 'swap'
   modeDict[ rivOutname + '4' ] = 'swap_flip1'
   modeDict[ rivOutname + '5' ] = 'swap_flip2'
   modeDict[ rivOutname + '6' ] = 'move'
   modeDict[ rivOutname + '7' ] = 'move_flip1'
   modeDict[ rivOutname + '8' ] = 'move_flip2'
   modeDict[ rivOutname + '9' ] = 'forced_random'
   modeDict[ rivOutname + '10' ] = 'forced_nearest'
   modeDict[ rivOutname + '11' ] = 'forced_farthest'
   modeDict[ rivOutname + '12' ] = 'forced_minDlambda'
   modeDict[ rivOutname + '13' ] = 'forced_Dlambda'
   #@Todo check whether it is in 
   if modeDict.has_key(outname):
      return modeDict[outname]
   else:
      print 'Error, not in the directory'
      return False

rivet_dir = "/home/vozak/top_ana/rivet"
setup_dir = "/home/vozak/setup"
rivetjob_dir = 'rivetJobs'
#plot_dir = "/home/vozak/top_ana/plots"
plot_dir = "/home/vozak/top_ana/plots/ue/"
version = 'v1'
rivet_output = '.yoda'
rivOutname = 'Rivet_m'
modeID = [i for i in range(14)]
export_dir = join( plot_dir, version)
#Check whether versioned directory already exist
if not os.path.isdir(export_dir):
   print('Creating %s' % export_dir)
   os.mkdir( export_dir)
else:
   print( '%s already exists' % export_dir )
#@Todo continueFlag?

rivet_working_dir = join( rivet_dir, rivetjob_dir)
if os.path.isdir( rivet_working_dir ): 
   print('retrieving rivet yoda files')
else:
   print( '%s directory does not exist, exit')
   quit()

work_dir = os.listdir(  rivet_working_dir )  
#Get all the yoda files
cr_modes = [mode for mode in work_dir if rivet_output in mode] 
print cr_modes
#Initialization of dictionary
modeDict = {}
for ID in modeID:
   dict = { (rivOutname + '%i' % ID) : 'default' }   
   modeDict.update(dict)

#Change modeDict to selected names

for mode in cr_modes:
   #Get rid of .yoda extension
   m = mode.split( '.' ) 
   if(get_name(m[0])): 
      os.system( 'cp %s %s' %( join( rivet_working_dir, m[0] + '.yoda' ), join( export_dir, get_name(m[0]) + '.yoda' ) )  )
      print ( 'cp %s %s' %( join( rivet_working_dir, m[0] + '.yoda' ), join( export_dir, get_name(m[0]) + '.yoda' ) )  )
