#!/usr/bin/env python
#Macro to create version.tex file containing the relevant info about a given version, this is then included to the plot.tex file
import os
from os.path import isdir, join  #check existence of dir, join paths
import copy

working_dir = '/home/matous/work/top/plots/'
version = raw_input(" Select a version (e.g v8) \n")
file_name = 'ver'
#Check existence of working dir
if not isdir( working_dir ):
    print '%s is not created' % working_dir
    quit()
#Load list of available cr schemes for selected version (only names without .yoda extension)
#cr_schemes = [mode.split('.')[0] for mode in dir_entries if rivet_output in mode]
vline = []
with open( join( working_dir, "VERSIONS" ) ,  "r") as ver_file:
      for ver_line in ver_file:
         line = ver_line.strip().split("]")
         if( version in line[0] ):
             vline = line         
texFile = open( join( working_dir, join(version,file_name)) + '.tex', 'w') #rewrite an existing file
#data_line = map( float, line.strip().split())
for l in vline: 
   texFile.write( '%s ' % l )


