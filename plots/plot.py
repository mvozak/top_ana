#!/usr/bin/env python
#Macro to create plots
import os
from os.path import isdir, join  #check existence of dir, join paths

#@Todo create dict of observables
#@Todo dict of cr schemes

#This can be done already in pythia8 generation
#This function implemented in export.py function
# -> Move to external file that both plot and export python files can import?
def get_name( outname ):
   modeDict[ rivOutname + '0' ] = 'cr_off'
   modeDict[ rivOutname + '1' ] = 'default_resOff'
   modeDict[ rivOutname + '2' ] = 'default_resOn'
   modeDict[ rivOutname + '3' ] = 'swap'
   modeDict[ rivOutname + '4' ] = 'swap_flip1'
   modeDict[ rivOutname + '5' ] = 'swap_flip2'
   modeDict[ rivOutname + '6' ] = 'move'
   modeDict[ rivOutname + '7' ] = 'move_flip1'
   modeDict[ rivOutname + '8' ] = 'move_flip2'
   modeDict[ rivOutname + '9' ] = 'forced_random'
   modeDict[ rivOutname + '10' ] = 'forced_nearest'
   modeDict[ rivOutname + '11' ] = 'forced_farthest'
   modeDict[ rivOutname + '12' ] = 'forced_minDlambda'
   modeDict[ rivOutname + '13' ] = 'forced_Dlambda'
   #@Todo check whether it is in
   if modeDict.has_key(outname):
      return modeDict[outname]
   else:
      print 'Error, not in the directory'
      return False


class plot:
    # <0|1> options -> <off|on>
        xLabel = 'def'
        yLabel = 'def'
        title = ''
        normSum = '1'  #normalization to sum of entries
        legend = '1' #print legend
        logScale = '0'

class compare_plot(plot):
        def __init__(self):
                self.legends = ''
                self.to_draw  = ''
                self.ratio = '1' #draw ratio plot
                self.xRLabel = ''
                self.yRLabel = 'def'
        def add_plot(self,new_plotID):
            self.legends +=  " " + new_plotID
            self.to_draw +=  " " + new_plotID


plot_dir = '/home/matous/work/top/plots/'
rivet_output = '.yoda'
version = raw_input(" Select a version (e.g v8) \n")
working_dir = join(plot_dir, version)
analysis_name = 'test'

all_plots = []
print 'hi'
#Check existence of working dir
if not isdir( working_dir ):
    print '%s is not created' & working_dir
    quit()

#Load list of available cr schemes for selected version
dir_entries = os.listdir( working_dir )
cr_schemes = [mode.split('.')[0] for mode in dir_entries if rivet_output in mode]
print cr_schemes
_cr_schemes = []  #Full path
for scheme in cr_schemes:
    _cr_schemes.append( "/" + scheme + '.yoda/' + analysis_name + '/')

#CMS plots
ue_observables = [ 'spt', 'mpt', 'nch', 'spt_pt', 'mpt_pt', 'nch_pt']
regions = [ 'all', 'to', 'tr', 'aw']
#for iP in ue_observables:
#    for iR in regions:
#        cms_plots.append( iP + '_' + iR )
#print cms_plots

#Create a .plot file with given settings
plotFile = open( join( working_dir, analysis_name) + '.plot', 'w') #rewrite an existing file

#Comparison plots in regions

Cplot = compare_plot()


cms_plots = []
for iP in ue_observables:
        for iR in regions:
        plt = copy.deepcopy( Cplot )
                for iC in _cr_schemes:
                        plt.add_plot( cr + iP + '_' + iR  )
                cms_plots.append( plt )


for iP in cms_plots:
    print iP.to_draw
