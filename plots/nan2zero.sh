#!/bin/sh
#VERSIONS="11 12"
VERSIONS="v4"
#no brackets $()
cd dilepton/$VERSIONS/cr_comparison
for NUM in $VERSIONS 
do
   VER="v"$NUM
   #rivet-cmphistos $VER/ATLAS_2011_I929691.yoda $VER/cr_off.yoda  $VER/default_resOff.yoda -o/home/matous/work/top/plots/frag/$VER/cr_comparison
   #rivet-cmphistos  $VER/cr_off.yoda  $VER/default_resOff.yoda $VER/forced_random.yoda -o/home/matous/work/top/plots/$VER/cr_comparison
#   sed -i -- 's/\[0\]//g' $VER/cr_comparison/*.dat
   sed -i -- 's/nan/0.001/g' *.dat
   #make-plots $VER/cr_comparison/*.dat
done


