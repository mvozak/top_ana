#! /usr/bin/env python
# -*- python -*-
import optparse, os
from os import listdir, system
import ROOT
from ROOT import TFile, TH1D, TGraphErrors, TF1
from os.path import isdir, join  #check existence of dir, join paths
#Plotting
import matplotlib.pylab as pl
import matplotlib.patches as mpatches
import matplotlib.mlab as mlab
import numpy as np
import math
from array import array
#from subprocess import call -> possible change for os
#Wut? How does this works?
op = optparse.OptionParser(usage=__doc__)
opts, args = op.parse_args()
#f = ROOT.TFile(args[0])


#Sort Colour reconnection models
def sort_cr(CR_LIST, SORTED_LIST):
 schemes = ["cr_off", "default_resOff", "default_resOn", "forced_random", "forced_nearest", "forced_farthest", "forced_minDlambda", "forced_smallest", "swap", "swap_flip1", "swap_flip2", "move", "move_flip1", "move_flip2"]
 #@Todo check if sorted list is empty
 for iS in range(len(schemes)):
        for iC in range(len(CR_LIST)):
              # print schemes[iS]
              # print yoda_files[iC]
               if schemes[iS] == CR_LIST[iC]:
                        SORTED_LIST.append(CR_LIST[iC])


def get_mean(VERSION, cr, OBS, MeanDict, ErrDict):
           #@Todo Check if f exists
           f = TFile( join( VERSION, cr) + '.root' )
           if f.IsZombie():
               print 'file %s not found' % cr+ '.root'
           if not f.IsOpen():
               print 'file %s not open' % cr+ '.root'
           #f.cd("ATLAS_2016_UETop")
           full_path = "ATLAS_2016_UETop" + "/" +OBS
           #@Note potentional problem if name of the analysis is changed
           h = f.Get(full_path)
           MeanDict[cr] = float( h.GetMean() )
           ErrDict[cr] = float( h.GetMeanError() )
           f.Close
           del f

def compute_rescaling(cr, MeanDict, ErrDict, Rescaling):
    WNominal = 80.370    #80370 +- 19 MeV http://atlas.cern/updates/physics-briefing/measuring-w-boson-mass
    Rescaling[cr] = MeanDict[cr]/WNominal


#Open root files and fit mass
def fit_mass( VERSION, mode, OBS, mass, mass_err, mean_mass):
        #@Todo Check if f exists
        f = TFile( join( VERSION, mode) + '.root' )
        if f.IsZombie():
            print 'file %s not found' % mode+ '.root'
        if not f.IsOpen():
            print 'file %s not open' % mode+ '.root'
        #f.cd("ATLAS_2016_UETop")
        full_path = "ATLAS_2016_UETop" + "/" +OBS
        #@Note potentional problem if name of the analysis is changed
        h = f.Get(full_path)
        hist_rebin(h, 4)
        g = TGraphErrors( h )
        if not g:
            print 'problem'
        x = []
        y = []
        ex = []
        ey = []
        for i in range(g.GetN() ):
         #   if g.GetX()[i] > 250.:
          #      break
            x.append( g.GetX()[i] )
            y.append( g.GetY()[i] )
            ex.append( g.GetEX()[i] )
            ey.append( g.GetEY()[i] )
        if not mean_mass:
            print "mean mass not defined!!"
            return
        else:
            #fit_downedge = mean_mass - 10
            #fit_upedge = mean_mass + 10
            fit_downedge = (mean_mass  -8) - 10
            fit_upedge = (mean_mass -8) + 10
            print "Mean %s" %mean_mass
            print "FIT RANGES"
            print fit_downedge
            print fit_upedge
            #Fit mass spectrum with gaussian
            temp = TF1( "gaussfit", "gaus(0)", fit_downedge, fit_upedge)
            temp.SetParameters( 1., (mean_mass - 8), 40.);
            g.Fit(temp, "RN" );

            #Getting parameters from fit
            normalization = temp.GetParameter(0)
            norm_err = temp.GetParError(0)
            mean = temp.GetParameter(1)
            mean_err = temp.GetParError(1)
            sigma = temp.GetParameter(2)
            sigma_err = temp.GetParError(2)
            print normalization, mean, sigma
            print norm_err, mean_err, sigma_err

            #Storing fit parameter
            mass[mode] = mean
            mass_err[mode] = mean_err


            #Plotting part
            fig = pl.figure()
            sp = fig.add_subplot(111)
            #Finding limits for plotting
            xmax = max( x )
            xmin = min( x )
            ymax = max( y )
            ymin = min( y )

            x_diff = xmax - xmin
            y_diff = ymax - ymin
            x_range = [ xmin - x_diff/2. , xmax + x_diff/2., x_diff  ]
            #x_range = [ 100. , 250. ]
            y_range = [ ymin - y_diff/2. , ymax + y_diff/2., y_diff  ]
            print 'X Range max: %s min: %s' %(x_range[0], x_range[1])
            print 'Y Range max: %s min: %s' %(y_range[0], y_range[1])

            #sp.xlim(x_range[0], x_range[1])
            #sp.ylim(y_range[0], y_range[1])
            sp.axis([x_range[0], x_range[1], y_range[0], y_range[1]])
            #pl.axvline(-1, ls="--", c="grey", zorder=-30)
            #pl.hold(True)
            sp.errorbar( x, y, xerr=ex, yerr=ey, marker="o", ls="none", c="red", ecolor="red")
            print 'e5'

            up_patch = mpatches.Patch(color='red', label='%s' %(mode) )
            sp.legend(handles=[up_patch])
            sp.text(0.8, 0.8, 'N = (%0.2f $\pm$ %0.2f)' %(normalization, norm_err), horizontalalignment='center', verticalalignment='center', transform= sp.transAxes, fontsize=12)
            sp.text(0.8, 0.75, '$\mu$ = (%0.2f $\pm$ %0.2f)' %(mean, mean_err), horizontalalignment='center', verticalalignment='center', transform= sp.transAxes, fontsize=12)
            sp.text(0.8, 0.7, '$\sigma$ = (%0.2f $\pm$ %0.2f)' %(sigma, sigma_err), horizontalalignment='center', verticalalignment='center', transform= sp.transAxes, fontsize=12)

            sp.set_ylabel("1/N /$\Delta m_{Top}$ [GeV$^{-1}$]")
            sp.set_xlabel("m$_{Top}$ [GeV]")
            x =  np.arange(x_range[0], x_range[1], 0.001)
            #  pl.plot( x, par[0]*x , c="red")
            normfactor = normalization*math.sqrt(2*sigma*sigma*math.pi)
            #normfactor = math.sqrt(2*sigma*sigma*math.pi)
            sp.plot( x, normfactor*mlab.normpdf( x, mean, sigma))
            #@Todo add text with modulus number
            # pl.text(x_range[0] + 0.5*abs(x_range[2]), y_range[1] - 0.4*abs(y_range[2]),'E = %.1f $\pm$ %.1f [GPa]' % (1/parameters_up[0], parameters_up[1]/(math.pow(parameters_up[0],2))), horizontalalignment='center', verticalalignment='center', fontsize=16, color = 'red')
            pl.savefig( VERSION + "/massfit_%s.pdf" %(mode))
        del g

def hist_rebin(h, factor=1):
        if not h:
            print "Error histogram not found"
            return
        #Rebinning, to wider bins so far
        if factor >= 1:
                h.Rebin(factor)
        # Equal bin width
        elif factor > 0:
                oldN = h.GetNbinsX()
                newN = factor*( oldN ) -1
                xmin = h.GetBinLowEdge(1)
                xmax = h.GetBinLowEdge(oldN) + h.GetBinWidth(oldN)
                newWidth = h.GetBinWidth(1)*factor
                print xmin, xmax
                binning = []
                for i in range(int(newN)):
                        binning.append( xmax + i*newWidth )
                bins = array.array.fromlist(binning)
                h.Rebin( int(newN), "rebin_tmass", binning)


def create_simplemass_table(VERSION, CR_MODES, OBS, MeanDict, ErrDict, Rescaling ):
#Todo potential problems if name is changed
    CRnom = 'default_resOff'
    nom = CRnom.replace( '_', ' ')
    MeanNominal = MeanDict[ CRnom ]
    MeanNominalRes = MeanDict[ CRnom ]*1/Rescaling[ CRnom ]

    tmassFile = open( (VERSION + "/" +OBS + '_table.tex') , 'w') #rewrite an existing file
    tmassFile.write( '\\begin{table}[htp] \n')
    tmassFile.write( '\\begin{center} \n')
    tmassFile.write( '\\begin{tabular}{|l|l|l|l|} \n')
    tmassFile.write( '\\toprule \n')
    whichMass =  'top' if 'tmass' in OBS else 'w'
    tmassFile.write( 'CR model & \m%s [GeV] & \dm%s [MeV] & \m%s(cr)/\m%s(nominal)  \\\ \n' %(whichMass, whichMass, whichMass, whichMass))
    tmassFile.write( '\midrule \n')
    for cr in CR_MODES:
        DeltaM = 1000*(MeanDict[cr]-MeanNominal)
        MeanCRRes = (MeanDict[cr]*1/Rescaling[cr])
        DeltaMRescaled = 1000*( MeanCRRes -MeanNominalRes)
        cr_name = cr.replace('_', ' ')
        tmassFile.write( ' %s & %0.3f $\pm$ %0.3f & %0.3f & %0.3f  \\\ \n' %(cr_name, MeanDict[cr], ErrDict[cr], DeltaM, Rescaling[cr] ) )
    #    print 'CR MODE: %s' %(cr)
    #    print 'MeanCRoff: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanNominal, Rescaling[cr], MeanNominalRes)
    #    print 'MeanCR: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanDict[cr], Rescaling[cr], MeanCRRes)
    tmassFile.write( '\\bottomrule \n')
    tmassFile.write( '\end{tabular} \n')
    tmassFile.write( '\caption{ The last column represents the difference in mass between a given model and  %s model } \n' %(nom))
    tmassFile.write( '\end{center} \n')
    tmassFile.write( '\end{table} \n')

#Create a table with mean values of top masses and rescalling in .tex file
def create_resmass_table(VERSION, CR_MODES, OBS, MeanDict, ErrDict, Rescaling ):
#Todo potential problems if name is changed
    CRnom = 'default_resOff'
    nom = CRnom.replace( '_', ' ')
    MeanNominal = MeanDict[ CRnom ]
    MeanNominalRes = MeanDict[ CRnom ]*1/Rescaling[ CRnom ]

    tmassFile = open( (VERSION + "/" + OBS + '_table.tex') , 'w') #rewrite an existing file
    tmassFile.write( '\\begin{table}[htp] \n')
    tmassFile.write( '\\begin{center} \n')
    tmassFile.write( '\\begin{tabular}{|l|l|l|l|l|l|} \n')
    tmassFile.write( '\\toprule \n')
    whichMass =  'top' if 'tmass' in OBS else 'w'
    tmassFile.write( 'CR model & \m%s [GeV] & \dm%s [MeV] & \m%s(cr)/\m%s(nominal) & \m%s(res) & \dm%s(res) [MeV]\\\ \n' %(whichMass, whichMass, whichMass, whichMass, whichMass, whichMass))
    tmassFile.write( '\midrule \n')
    for cr in CR_MODES:
        DeltaM = 1000*(MeanDict[cr]-MeanNominal)
        MeanCRRes = (MeanDict[cr]*1/Rescaling[cr])
        DeltaMRescaled = 1000*( MeanCRRes -MeanNominalRes)
        cr_name = cr.replace('_', ' ')
        tmassFile.write( ' %s & %0.3f $\pm$ %0.3f & %0.3f & %0.3f & %0.3f & %0.3f \\\ \n' %(cr_name, MeanDict[cr], ErrDict[cr], DeltaM, Rescaling[cr], MeanCRRes, DeltaMRescaled ) )
   #     print 'CR MODE: %s' %(cr)
   #     print 'MeanCRoff: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanNominal, Rescaling[cr], MeanNominalRes)
   #     print 'MeanCR: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanDict[cr], Rescaling[cr], MeanCRRes)
   #     print 'Difference %0.4f' % (DeltaMRescaled)
    tmassFile.write( '\\bottomrule \n')
    tmassFile.write( '\end{tabular} \n')
    tmassFile.write( '\caption{ Comparison of mean top mass reconstructed from various CR schemes. The columns go as follows: under first are names of CR scheme, second includes mean values of top mass with its RMS, third represent comparison to %s model, fourth rescalling factor, fifth mean value of top mass after rescalling and the last is again comparison of mean top mass but after rescalling.} \n' %(nom))
    tmassFile.write( '\end{center} \n')
    tmassFile.write( '\end{table} \n')

#Create a table with fitted values of top masses  in .tex file
def create_fitmass_table(VERSION, CR_MODES, OBS, FitDict, FitErrDict ):
#Todo potential problems if name is changed
    CRnom = 'default_resOff'
    nom = CRnom.replace( '_', ' ')
    FitNominal = FitDict[ CRnom ]

    tmassFile = open( (VERSION + "/" +OBS + '_fitmass_table.tex') , 'w') #rewrite an existing file
    tmassFile.write( '\\begin{table}[htp] \n')
    tmassFile.write( '\\begin{center} \n')
    tmassFile.write( '\\begin{tabular}{|l|l|l|} \n')
    tmassFile.write( '\\toprule \n')
    whichMass =  'top' if 'tmass' in OBS else 'w'
    tmassFile.write( 'CR model &  \m%s(fit) [GeV] & \dm%s(fit) [MeV]\\\ \n' %(whichMass, whichMass))
    tmassFile.write( '\midrule \n')
    for cr in CR_MODES:
        #DeltaM = 1000*(MeanDict[cr]-MeanNominal)
        DeltaFitM = 1000*(FitDict[cr]-FitNominal)
        cr_name = cr.replace('_', ' ')
        tmassFile.write( ' %s &  %0.2f $\pm$ %0.2f & %0.2f \\\ \n' %(cr_name, FitDict[cr], FitErrDict[cr], DeltaFitM) )
    #    print 'CR MODE: %s' %(cr)
    #    print 'MeanCRoff: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanNominal, Rescaling[cr], MeanNominalRes)
    #    print 'MeanCR: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanDict[cr], Rescaling[cr], MeanCRRes)
    #    print 'Difference %0.4f' % (DeltaMRescaled)
    tmassFile.write( '\\bottomrule \n')
    tmassFile.write( '\end{tabular} \n')
    tmassFile.write( '\caption{ Comparison of fit values of top mass reconstructed in various CR schemes. The columns go as follows: in the first are names of CR scheme, second includes fit values of top mass with fit uncertainty, third represents comparison to %s model.} \n' %(nom))
    tmassFile.write( '\label{t:tmass} \n')
    tmassFile.write( '\end{center} \n')
    tmassFile.write( '\end{table} \n')


#Create a table with comparison between fitted and mean values of top masses  in .tex file
def create_meanfitmass_table(VERSION, CR_MODES, OBS, MeanDict, ErrDict, FitDict, FitErrDict ):
#Todo potential problems if name is changed
    CRnom = 'default_resOff'
    nom = CRnom.replace( '_', ' ')
    MeanNominal = MeanDict[ CRnom ]
    FitNominal = FitDict[ CRnom ]

    tmassFile = open( ( VERSION + "/" + OBS + '_meanVfit_table.tex') , 'w') #rewrite an existing file
    tmassFile.write( '\\begin{table}[htp] \n')
    tmassFile.write( '\\begin{center} \n')
    tmassFile.write( '\\begin{tabular}{|l|l|l|l|l|} \n')
    tmassFile.write( '\\toprule \n')
    whichMass =  'top' if 'tmass' in OBS else 'w'
    tmassFile.write( 'CR model & \m%s [GeV] & \dm%s [MeV] & \m%s(fit) [GeV] & \dm%s(fit) [MeV]\\\ \n' %(whichMass, whichMass, whichMass, whichMass))
    tmassFile.write( '\midrule \n')
    for cr in CR_MODES:
        DeltaM = 1000*(MeanDict[cr]-MeanNominal)
        DeltaFitM = 1000*(FitDict[cr]-FitNominal)
        cr_name = cr.replace('_', ' ')
        tmassFile.write( ' %s & %0.3f $\pm$ %0.3f & %0.3f & %0.3f $\pm$ %0.3f & %0.3f \\\ \n' %(cr_name, MeanDict[cr], ErrDict[cr], DeltaM, FitDict[cr], FitErrDict[cr], DeltaFitM) )
    #    print 'CR MODE: %s' %(cr)
    #    print 'MeanCRoff: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanNominal, Rescaling[cr], MeanNominalRes)
    #    print 'MeanCR: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanDict[cr], Rescaling[cr], MeanCRRes)
    #    print 'Difference %0.4f' % (DeltaMRescaled)
    tmassFile.write( '\\bottomrule \n')
    tmassFile.write( '\end{tabular} \n')
    tmassFile.write( '\caption{ Comparison of fit and mean values of top mass reconstructed from various CR schemes. The columns go as follows: under first are names of CR scheme, second includes mean values of top mass with RMS, third represent comparison to %s model, fourth fit value with its uncertainty and the last comparison to %s model} \n' %(nom,nom))
    tmassFile.write( '\end{center} \n')
    tmassFile.write( '\end{table} \n')

##############BEGIN##########
VERSION = 'v26'
OBS = ['tmass_hadronic', 'wmass_hadronic']
WORKING_DIR = VERSION + '/'
#CR_MODES = [ m for m in LIST if '.yoda' in m ]
#CR_YODA = [ m for m in os.listdir( WORKING_DIR ) if '.yoda' in m ]
CR_YODA = [ m for m in os.listdir( WORKING_DIR ) if '.yoda' in m ]
UNSORTED_MODES = []
CR_MODES = []
for yoda in CR_YODA:
        m = yoda.split('.')
        UNSORTED_MODES.append( m[0] )

#Create .root files
#for cr in CR_MODES:
#        os.system( "yoda2root %s.yoda %s.root" % (join(WORKING_DIR,cr),join(WORKING_DIR,cr)) )

#Do modes ordering
#print CR_MODES
sort_cr(UNSORTED_MODES, CR_MODES)
#W mass table
WMassDict = {}
WMassErrDict = {}
Rescaling = {}
#Top mass table
TMassDict = {}
TMassErrDict = {}
TFitMassDict = {}
TFitMassErrDict = {}


for cr in CR_MODES:
        get_mean(VERSION, cr, 'wmass_hadronic', WMassDict, WMassErrDict)
        compute_rescaling( cr, WMassDict, WMassErrDict, Rescaling)
        get_mean(VERSION, cr, 'tmass_hadronic', TMassDict, TMassErrDict)
        fit_mass(VERSION, cr, "tmass_hadronic", TFitMassDict, TFitMassErrDict, TMassDict[cr])
create_resmass_table(VERSION, CR_MODES, 'tmass_hadronic', TMassDict, TMassErrDict,Rescaling)
create_fitmass_table(VERSION, CR_MODES, 'tmass_hadronic', TFitMassDict, TFitMassErrDict)
create_meanfitmass_table(VERSION, CR_MODES, 'tmass_hadronic', TMassDict, TMassErrDict, TFitMassDict, TFitMassErrDict)
create_simplemass_table(VERSION, CR_MODES, 'wmass_hadronic', WMassDict, WMassErrDict, Rescaling)
