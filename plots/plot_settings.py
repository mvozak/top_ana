#!/usr/bin/env python
class plot:
    # <0|1> options -> <off|on>
        name = ''
        xLabel = 'def'
        yLabel = 'def'
        title = 'def'
        normSum = '1'  #normalization to sum of entries
        legend = '1' #print legend
        logX = '0'
        logY = '0'
        rebin = 0
        xMin = 0
        xMax = 1
        yRMin = 0.5
        yRMax = 1.5
        xLeg = 0.5
        yLeg = 0.5

class compare_plot(plot):
        def __init__(self):
                self.legends = ''
                self.to_draw  = ''
                self.ratio = '1' #draw ratio plot
                self.ratioTitle = 'CR scheme/Default resOn'
                self.ratioRef = ''
                self.xRLabel = 'def'
                self.yRLabel = 'def'
        def add_plot(self,new_plotID):
            self.legends +=  " " + new_plotID
            self.to_draw +=  " " + new_plotID

#According to the name of variable set plot class
def set_plot(plt):
        #@Todo create aliases for normalization
        IntNorm = '$1/\mathrm{N}\mathrm{dN}$'
        regs = ['all', 'toward', 'transverse', 'away']
        pt =  '$\mathrm{p}_{\mathrm{T}}$'
        ptttbar = '$ \mathrm{p_{Tt\\bar{t}}} $'
        mpt = '$\langle$' + pt + '$\\rangle$'
        dpt =  '$\mathrm{dp}_{\mathrm{T}}$'
        GeV = '$[\mathrm{GeV}]$'
        invGeV = '$[\mathrm{GeV}^{-1}]$'
        nch = '$\mathrm{n}_{\mathrm{ch}}$'
        mnch = '$\langle$' + nch + '$\\rangle$'
        spt = '$\Sigma \mathrm{p}_{\mathrm{T}}$'
        dspt = 'd' + spt
        mspt = '$\langle$' + spt+ '$\\rangle$'
        topmassNorm = '$\mathrm{d} \mathrm{m_{top}}$'
        dict =	'/default_resOff.yoda/ATLAS_2016_UETop/'
        dr_tVr = '$\Delta R$ (t(truth), t(reco))'
        dr_bVl = '$\Delta R$ (bjet,ljet)'
        dr_bVb = '$\Delta R$ (bjet,abjet)'
        ddr = 'd$\Delta R$'
        t_cbl = 'b/ljets sel. as min' + dr_bVl
        t_sbl = 'bjet from the same decay as ljets'
        pangle = '$\\theta_p$'
        eta = '$\eta$'
        djshape = '$\\rho (\mathrm{r})$'
        ijshape = '$\Psi (\mathrm{r})$'
        topetareco = '$\eta_{reco}$'
        topetatruth = '$\eta_{truth}$'
        topetalep = '$\eta_{lep}$'
        topetahad = '$\eta_{had}$'
        topphireco = '$\phi_{reco}$'
        topphitruth = '$\phi_{truth}$'
        topptreco = '$\mathrm{p}_{\mathrm{T}(reco)}$'
        toppttruth = '$\mathrm{p}_{\mathrm{T}(truth)}$'

        #Lists of observables which are used in if statements
        #@Todo set up globally? So it can be shared with the main programme?
        Opt = []
        Onch = []
        Ospt = []
        Ompt = []
        Ospt_nch = []
        Ompt_nch = []
        Ompt_pt = []
        Ospt_pt = []
        Onch_pt = []
        #Do globally
        suffix = [ 'hb', 'lb', 'l1', 'l2', 'inc', 'ntrk', 'all', 'to', 'tr', 'trmax', 'trmin', 'aw' ]
        for i in suffix:
                Opt.append( 'pt_' + ('%s' %i) )
                Ompt.append( 'mpt_' + ('%s' %i) )
                Onch.append( 'nch_' + ('%s' %i) )
                Onch_pt.append( 'nch_pt_' + ('%s' %i) )
                Ospt.append( 'spt_' + ('%s' %i) )
                Ospt_pt.append( 'spt_pt_' + ('%s' %i) )
                Ospt_nch.append( 'spt_nch_' + ('%s' %i) )
                Ompt_nch.append( 'mpt_nch_' + ('%s' %i) )
                Ompt_pt.append( 'mpt_pt_' + ('%s' %i) )

        #Diferential jet shapes
        difJshape = []
        intJshape = []
        difJnch = []
        intJnch = []
        #r_annulus = [ (2+4*i) for i in range(10) ]
        for tjet in ['BJ', 'LJ']:
                for ptc in ['none', '25_50GeV', '50_100GeV', '100_hGeV']:
                        for i in [(2+4*i) for i in range(10)]:
                                difJshape.append( 'dif' + tjet + 'shape_%i_%s' %(i, ptc) )
                                intJshape.append( 'int' +tjet+ 'shape_%i_%s' %(i, ptc) )
                                difJnch.append( 'dif' + tjet+ 'nch_%i_%s' %(i, ptc) )
                                intJnch.append( 'int' + tjet + 'nch_%i_%s' %(i, ptc) )
        djs = [ '', 'b', 'ab', 'l1', 'l2' ]
        djss = []
        for i in djs:
                djss.append( 'dif' + i + 'jetshape' )
        djss.append('difjetshape90GeV')


        # nch_pt =
        #@TODO potential problem pt plot must be first otherwise can be associated e.g with pt_e
        if plt.name == 'bbjetcorrelation':
                plt.xLabel = '$\Delta \mathrm{R(bj1,bj2)}$'
                plt.yLabel = IntNorm + '/\mathrm{d} \Delta \mathrm{R}$'
                plt.title = 'bjet correlation'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 1
                plt.xMin = 0.5
                plt.xMax = 4.2
                plt.xLeg = 0.35
                plt.yLeg = 0.8
        elif plt.name == 'lightjetcorrelation':
                plt.xLabel = '$\Delta \mathrm{R(lj1,lj2)}$'
                plt.yLabel = '$1/\mathrm{N}\mathrm{dN}/\mathrm{d} \Delta \mathrm{R}$'
                plt.title = 'lightjet correlation'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 1
                plt.xMin = 0.5
                plt.xMax = 4.2
                plt.xLeg = 0.35
                plt.yLeg = 0.7
        elif plt.name in djss:
                plt.xLabel = '$\mathrm{r}/ \mathrm{R_{jet}}$'
                plt.yLabel = '$\langle$' + djshape  + '$\\rangle$'
                if plt.name == 'difbjetshape':
                        plt.title = 'differential bjet shape'
                elif plt.name == 'difabjetshape':
                        plt.title = 'differential antibjet shape'
                elif plt.name == 'difl1jetshape':
                        plt.title = 'differential light1jet shape'
                elif plt.name == 'difl2jetshape':
                        plt.title = 'differential light2jet shape'
                else:
                        plt.title = 'differential jet shape'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 1
                plt.xMin = 0
                plt.xMax = 1
                plt.xLeg = 0.1
                plt.yLeg = 0.6
        elif plt.name in Opt:
                plt.xLabel = pt + ' ' + GeV
                plt.yLabel = IntNorm + '/' + dpt
                plt.logY = 1
                plt.xMin = 0
                plt.xMax = 300
                tit = 'Transverse momentum of charged particles '
                if plt.name == 'pt_e':
                        plt.title = pt + 'of electron from W'
                elif plt.name == 'pt_mu':
                        plt.title = pt + ' of muon from W'
                elif plt.name == 'pt_w_l':
                        plt.title = pt + ' of W decaying leptonicaly'
                elif plt.name == 'pt_w_h':
                        plt.title = pt + ' of W decaying hadronicaly'
                elif plt.name == 'pt_lj':
                        plt.title = pt + ' of jets from W'
                elif plt.name == 'pt_leadinglj':
                        plt.title = 'jet with highest ' + pt
                elif plt.name == 'pt_restljs':
                        plt.title = pt + ' of lightjets (jets from W exc.)'
                elif plt.name == 'ptT_l':
                        plt.title = pt + ' of electron from W'
                elif plt.name == 'ptT_h':
                        plt.title = pt + ' of electron from W'
                elif plt.name == 'pt_inc':
                    plt.title = 'Charged particles transverse momentum'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_ntrk':
                    plt.title = 'From particles not clustered in jets'
                    plt.xMin = 1
                    plt.xMax = 10
                    plt.yRMin = 0.8
                    plt.yRMax = 1.8
                    plt.xLeg = 0.05
                    plt.yLeg = 0.75
                elif plt.name == 'pt_hb':
                    plt.title = tit + 'in bjet'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_lb':
                    plt.title = tit + 'in antibjet'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_l1':
                    plt.title = tit + 'in lightjet1 '
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                    plt.yRMin = 0.9
                    plt.yRMax = 1.1
                    plt.xLeg = 0.05
                    plt.yLeg = 0.75
                    plt.rebin = 2
                elif plt.name == 'pt_l2':
                    plt.title = tit + 'in lightjet2'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_all':
                    plt.title = tit + 'in all regions'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_to':
                    plt.title = tit + 'in toward'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_tr':
                    plt.title = tit + 'in transverse'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_trmax':
                    plt.title = tit + 'in trmax'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_trmin':
                    plt.title = tit + 'in trmin'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                elif plt.name == 'pt_aw':
                    plt.title = tit + 'in away'
                    plt.xMin = 1
                    plt.xMax = 100
                    plt.logX = 1
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name in Onch:
                plt.xLabel = nch
                plt.yLabel = IntNorm
                plt.normSum = 1
                plt.logY = 1
                plt.xMin = 0
                plt.xMax = 150
                plt.xLeg = 0.28
                plt.yLeg = 0.83
                tit = 'Multiplicity of charged particles '
                if plt.name == 'nch_inc':
                    plt.title = 'Charged particles multiplicity'
                elif plt.name == 'nch_ntrk':
                    plt.title = 'From particles not clustered in jets'
                    plt.xMin = 0.5
                    plt.xMax = 120
                    plt.yRMin = 0.3
                    plt.yRMax = 1.4
                    plt.rebin = 2
                    plt.xLeg = 0.3
                    plt.yLeg = 0.9
                elif plt.name == 'nch_hb':
                    plt.title = tit + ' in bjet'
                    plt.xMin = 2.5 #shape starts from three particles
                    plt.xMax = 60 #
                    plt.yRMin = 0.3
                    plt.yRMax = 1.7
                    plt.xLeg = 0.3
                    plt.yLeg = 0.7
                    plt.rebin = 2
                elif plt.name == 'nch_lb':
                    plt.title = tit +' in antibjet'
                    plt.xMin = 0
                    plt.xMax = 80
                elif plt.name == 'nch_l1':
                    plt.title = tit +' in lightjet1 '
                    plt.xMin = 0
                    plt.xMax = 80
                elif plt.name == 'nch_l2':
                    plt.title = tit +' in lightjet2'
                    plt.xMin = 0
                    plt.xMax = 80
                elif plt.name == 'nch_all':
                    plt.title = tit +' in all regions'
                elif plt.name == 'nch_to':
                    plt.title = tit +' in toward'
                    plt.xMin = 0
                    plt.xMax = 80
                elif plt.name == 'nch_tr':
                    plt.title = tit +' in transverse'
                    plt.xMin = 0
                    plt.xMax = 80
                elif plt.name == 'nch_trmax':
                    plt.title = tit +' in trmax'
                    plt.xMin = 0
                    plt.xMax = 50
                elif plt.name == 'nch_trmin':
                    plt.title = tit +' in trmin'
                    plt.xMin = 0
                    plt.xMax = 50
                elif plt.name == 'nch_aw':
                    plt.title = tit +' in away'
                    plt.xMin = 0
                    plt.xMax = 100
                plt.ratioRef = dict + plt.name
        elif plt.name in Ospt:
                plt.xLabel = spt + ' ' + GeV
                plt.yLabel = IntNorm + "/" + dspt
                plt.ratioRef = dict + plt.name
                plt.xMin = 0
                plt.xMax = 200
                tit = 'Scalar sum of tr momentum of charged particles '
                if plt.name == 'spt_inc':
                    plt.title = 'Charged particles scalar sum of tr momentum'
                    plt.yRMin = 0.6
                    plt.yRMax = 1.8
                    plt.xLeg = 0.25
                    plt.yLeg = 0.8
                elif plt.name == 'spt_ntrk':
                    plt.title = 'From particles not clustered in jets'
                    plt.xMin = 0
                    plt.xMax = 200
                    plt.yRMin = 0.6
                    plt.yRMax = 1.3
                    plt.xLeg = 0.25
                    plt.yLeg = 0.8
                elif plt.name == 'spt_hb':
                    plt.title = tit +' in bjet'
                    plt.xMin = 25
                    plt.xMax = 200
                elif plt.name == 'spt_lb':
                    plt.title = tit +' in antibjet'
                    plt.xMin = 25
                    plt.xMax = 200
                elif plt.name == 'spt_l1':
                    plt.title = tit +' in lightjet1 '
                    plt.xMin = 25
                    plt.xMax = 200
                elif plt.name == 'spt_l2':
                    plt.title = tit +' in lightjet2'
                    plt.xMin = 25
                    plt.xMax = 200
                elif plt.name == 'spt_all':
                    plt.title = tit +' in all regions'
                elif plt.name == 'spt_to':
                    plt.title = tit +' in toward'
                elif plt.name == 'spt_tr':
                    plt.title = tit +' in transverse'
                elif plt.name == 'spt_trmax':
                    plt.title = tit +' in trmax'
                elif plt.name == 'spt_trmin':
                    plt.title = tit +' in trmin'
                elif plt.name == 'spt_aw':
                    plt.title = tit +' in away'
                plt.normSum = 1
                plt.logY = 1
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name in Ompt:
                plt.xLabel = mpt + ' ' + GeV
                plt.yLabel = IntNorm
                plt.ratioRef = dict + plt.name
                plt.xMin = 0
                plt.xMax = 2.5
                tit = 'Average tr momentum of charged particles '
                if plt.name == 'mpt_inc':
                    plt.title = 'Charged particles scalar sum of tr momentum'
                elif plt.name == 'spt_ntrk':
                    plt.title = 'From particles not clustered in jets'
                    plt.yRMin = 0.0
                    plt.yRMax = 3.0
                elif plt.name == 'mpt_hb':
                    plt.title = tit +' in bjet'
                    plt.xMin = 0.6
                    plt.xMax = 10
                    plt.yRMin = 0.0
                    plt.yRMax = 3.0
                    plt.yLeg=0.8
                    plt.xLeg=0.2
                elif plt.name == 'mpt_lb':
                    plt.title = tit +' in antibjet'
                    plt.xMin = 0
                    plt.xMax = 10
                elif plt.name == 'mpt_l1':
                    plt.title = tit +' in lightjet1 '
                    plt.xMin = 0
                    plt.xMax = 10
                elif plt.name == 'spt_l2':
                    plt.title = tit +' in lightjet2'
                    plt.xMin = 0
                    plt.xMax = 10
                elif plt.name == 'spt_all':
                    plt.title = tit +' in all regions'
                elif plt.name == 'spt_to':
                    plt.title = tit +' in toward'
                elif plt.name == 'spt_tr':
                    plt.title = tit +' in transverse'
                elif plt.name == 'spt_trmax':
                    plt.title = tit +' in trmax'
                elif plt.name == 'spt_trmin':
                    plt.title = tit +' in trmin'
                elif plt.name == 'spt_aw':
                    plt.title = tit +' in away'
                plt.normSum = 1
                plt.logY = 1
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name in Ospt_nch:
                plt.xLabel = nch
                plt.yLabel = mspt
                plt.ratioRef = dict + plt.name
                plt.xMin = 0
                plt.xMax = 80
                tit = 'Mean scalar sum of tr momentum of charged particles '
                if plt.name == 'spt_nch_inc':
                    plt.title = 'Charged particles scalar sum of tr momentum'
                elif plt.name == 'spt_nch_ntrk':
                    plt.title = 'From particles not clustered in jets with ' + pt +  '$>$ 5' + GeV
                elif plt.name == 'spt_nch_hb':
                    plt.title = tit +' in bjet'
                    plt.xMax = 60
                elif plt.name == 'spt_nch_lb':
                    plt.title = tit +' in antibjet'
                    plt.xMax = 60
                elif plt.name == 'spt_nch_l1':
                    plt.title = tit +' in lightjet1 '
                    plt.xMax = 60
                elif plt.name == 'spt_nch_l2':
                    plt.title = tit +' in lightjet2'
                    plt.xMax = 60
                elif plt.name == 'spt_nch_all':
                    plt.title = tit +' in all regions'
                elif plt.name == 'spt_nch_to':
                    plt.title = tit +' in toward'
                    plt.xMax = 60
                elif plt.name == 'spt_nch_tr':
                    plt.title = tit +' in transverse'
                    plt.xMax = 50
                elif plt.name == 'spt_nch_trmax':
                    plt.title = tit +' in trmax'
                    plt.xMax = 40
                elif plt.name == 'spt_nch_trmin':
                    plt.title = tit +' in trmin'
                    plt.xMax = 30
                elif plt.name == 'spt_nch_aw':
                     plt.title = tit +' in away'
                     plt.xMax = 60
                plt.normSum = 0
                plt.logY = 1
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name in Ompt_nch:
                plt.xLabel = nch
                plt.yLabel = mpt + GeV
                tit = 'Mean tr momentum of charged particles '
                if plt.name == 'mpt_nch_inc':
                    plt.title = 'Charged particle mean transverse momentum '
                elif plt.name == 'mpt_nch_ntrk':
                    plt.title = 'From particles not clustered in jets'
                    plt.yRMin = 0.6
                    plt.yRMax = 1.3
                    plt.yLeg=0.77
                    plt.xLeg=0.2
                elif plt.name == 'mpt_nch_hb':
                    plt.title = tit +' in bjet'
                elif plt.name == 'mpt_nch_lb':
                    plt.title = tit +' in antibjet'
                elif plt.name == 'mpt_nch_l1':
                    plt.title = tit +' in lightjet1 '
                    plt.XMax=50
                    plt.yLeg=0.6
                    plt.xLeg=0.97
                elif plt.name == 'mpt_nch_l2':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'mpt_nch_all':
                    plt.title = tit +' in all regions'
                elif plt.name == 'mpt_nch_to':
                    plt.title = tit +' in toward'
                elif plt.name == 'mpt_nch_tr':
                    plt.title = tit +' in transverse'
                elif plt.name == 'mpt_nch_trmax':
                    plt.title = tit +' in trmax'
                elif plt.name == 'mpt_nch_trmin':
                    plt.title = tit +' in trmin'
                elif plt.name == 'mpt_nch_aw':
                     plt.title = tit +' in away'
                plt.ratioRef = dict + plt.name
                plt.normSum = 0
                plt.logY = 0
                plt.xMin = 0
                plt.xMax = 80
                plt.xLeg = 0.6
                plt.yLeg = 0.5
        elif plt.name == 'tmass':
                plt.xLabel = '$\langle \mathrm{m_{t\\bar{t}}} \\rangle [\mathrm{GeV}]$'
                plt.yLabel = IntNorm + topmassNorm + invGeV
                plt.title = '(tmass(l) + tmass(h))/2'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 0
                plt.xMax = 300
                plt.xLeg = 0.6
                plt.yLeg = 0.8

        elif plt.name == 'tmass_leptonic':
                plt.xLabel = '$\mathrm{m_{top}} [\mathrm{GeV}] $'
                plt.yLabel = '$1/\mathrm{N}\mathrm{dN}/\mathrm{dm_{top}} [\mathrm{GeV}^{-1}]$'
                plt.title = 'leptonic top mass'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 100
                plt.xMax = 250
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name == 'tmass_hadronic':
                plt.xLabel = '$\mathrm{m_{top}} [\mathrm{GeV}] $'
                plt.yLabel = '$1/\mathrm{N}\mathrm{dN}/\mathrm{dm_{top}} [\mathrm{GeV}^{-1}]$'
                plt.title = 'hadronic top mass'
                plt.ratioRef = dict + plt.name
                plt.rebin = 4
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 100
                plt.xMax = 250
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name == 'wmass_leptonic':
                plt.xLabel = '$\mathrm{m_{W}} [\mathrm{GeV}] $'
                plt.yLabel = '$1/\mathrm{N}\mathrm{dN}/\mathrm{dm_{W}} [\mathrm{GeV}^{-1}]$'
                plt.title = 'Mass of W decaying leptonicaly'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 60
                plt.xMax = 100
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name == 'wmass_hadronic':
                plt.xLabel = '$\mathrm{m_{W}} [\mathrm{GeV}] $'
                plt.yLabel = '$1/\mathrm{N}\mathrm{dN}/\mathrm{dm_{W}} [\mathrm{GeV}^{-1}]$'
                plt.title = 'Mass of W decaying hadronicaly'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 60
                plt.xMax = 100
                plt.xLeg = 0.6
                plt.yLeg = 0.8
        elif plt.name in Ospt_pt:
                plt.xLabel = ptttbar + ' ' + GeV
                plt.yLabel = spt + ' ' + GeV
                tit = 'Scalar sum charged particle tr momentum'
                if plt.name == 'spt_pt_inc':
                    plt.title = 'Charged particle mean transverse momentum '
                elif plt.name == 'spt_pt_ntrk':
                    plt.title = 'From particles not clustered in jets'
                elif plt.name == 'spt_pt_hb':
                    plt.title = tit +' in bjet'
                elif plt.name == 'spt_pt_lb':
                    plt.title = tit +' in antibjet'
                elif plt.name == 'spt_pt_l1':
                    plt.title = tit +' in lightjet1 '
                elif plt.name == 'spt_pt_l2':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'spt_pt_all':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'spt_pt_to':
                    plt.title = tit +' in toward'
                elif plt.name == 'spt_pt_tr':
                    plt.title = tit +' in transverse'
                elif plt.name == 'spt_pt_trmax':
                    plt.title = tit +' in trmax'
                elif plt.name == 'spt_pt_trmin':
                    plt.title = tit +' in trmin'
                elif plt.name == 'spt_pt_aw':
                     plt.title = tit +' in away'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 0
                plt.xMax = 300
                plt.xLeg = 0.6
                plt.yLeg = 0.5
        elif plt.name in Ompt_pt:
                plt.xLabel = ptttbar + ' ' + GeV
                plt.yLabel = mpt + ' ' + GeV
                tit = 'Mean tr momentum of charged particles'
                if plt.name == 'mpt_pt_inc':
                    plt.title = 'Charged particle mean transverse momentum '
                elif plt.name == 'mpt_pt_ntrk':
                    plt.title = 'From particles not clustered in jets'
                elif plt.name == 'mpt_pt_hb':
                    plt.title = tit +' in bjet'
                elif plt.name == 'mpt_pt_lb':
                    plt.title = tit +' in antibjet'
                elif plt.name == 'mpt_pt_l1':
                    plt.title = tit +' in lightjet1 '
                elif plt.name == 'mpt_pt_l2':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'mpt_pt_all':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'mpt_pt_to':
                    plt.title = tit +' in toward'
                elif plt.name == 'mpt_pt_tr':
                    plt.title = tit +' in transverse'
                elif plt.name == 'mpt_pt_trmax':
                    plt.title = tit +' in trmax'
                elif plt.name == 'mpt_pt_trmin':
                    plt.title = tit +' in trmin'
                elif plt.name == 'mpt_pt_aw':
                     plt.title = tit +' in away'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 0
                plt.xMin = 0
                plt.xMax = 300
                plt.xLeg = 0.6
                plt.yLeg = 0.5
        elif plt.name in Onch_pt:
                plt.xLabel =  ptttbar + ' '+ GeV
                plt.yLabel = nch
                tit = 'Multiplicity of charged particles'
                if plt.name == 'nch_pt_inc':
                    plt.title = 'Charged particle multiplicity '
                elif plt.name == 'nch_pt_ntrk':
                    plt.title = 'From particles not clustered in jets'
                elif plt.name == 'nch_pt_hb':
                    plt.title = tit +' in bjet'
                elif plt.name == 'nch_pt_lb':
                    plt.title = tit +' in antibjet'
                elif plt.name == 'nch_pt_l1':
                    plt.title = tit +' in lightjet1 '
                elif plt.name == 'nch_pt_l2':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'nch_pt_all':
                    plt.title = tit +' in lightjet2'
                elif plt.name == 'nch_pt_to':
                    plt.title = tit +' in toward'
                elif plt.name == 'nch_pt_tr':
                    plt.title = tit +' in transverse'
                elif plt.name == 'nch_pt_trmax':
                    plt.title = tit +' in trmax'
                elif plt.name == 'nch_pt_trmin':
                    plt.title = tit +' in trmin'
                elif plt.name == 'nch_pt_aw':
                     plt.title = tit +' in away'
                plt.ratioRef = dict + plt.name
                plt.normSum = 1
                plt.logY = 1
                plt.xMin = 0
                plt.xMax = 300
                plt.xLeg = 0.6
                plt.yLeg = 0.5
                plt.logY = 0
        elif plt.name in difJshape:
                        plt.xLabel = djshape
                        plt.yLabel = IntNorm
                        tjet = 'bjet' if 'BJ' in plt.name else 'lightjet'
                        plt.title = 'differential ' + tjet + ' shape'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0
                        plt.xMax = 25
                        #plt.xMax = 200
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
        elif plt.name in intJshape:
                        plt.xLabel = ijshape
                        plt.yLabel = IntNorm
                        tjet = 'bjet' if 'BJ' in plt.name else 'lightjet'
                        plt.title = 'integral ' + tjet + ' shape'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0
                        plt.xMax = 1
                        #plt.xMax = 200
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name in difJnch:
                        plt.xLabel = nch + '(r)/' + nch + '(tot)'
                       #  plt.xLabel = nch + '(r)'
                        plt.yLabel = IntNorm
                        tjet = 'bjet' if 'BJ' in plt.name else 'lightjet'
                        plt.title = 'differential ' + tjet + ' ' + nch
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 0
                        plt.xMax = 1
                        #plt.xMax = 10.5
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
        elif plt.name in intJnch:
                        plt.xLabel = '$\Sigma$' + nch + '(r)/' + nch + '(tot)'
                        #plt.xLabel = '$\Sigma$' + nch + '(r)'
                        plt.yLabel = IntNorm
                        tjet = 'bjet' if 'BJ' in plt.name else 'lightjet'
                        plt.title = 'integral ' + tjet + ' ' + nch
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 0
                        plt.xMax = 1
                        #plt.xMax = 80.5
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name in 'intBJshape_2_eta':
                        plt.xLabel = eta
                        plt.title = 'integral bjet shape contr. from the edge'
                        plt.yLabel = '\langle 1 - ' + ijshape + '\rangle'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 0
                        plt.xMax = 2.5
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name in 'intLJshape_2_eta':
                        plt.xLabel = eta
                        plt.yLabel = '\langle 1 - ' + ijshape + '\rangle'
                        plt.title = 'integral lightjet shape contr. from the edge'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 0
                        plt.xMax = 2.5
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name in 'intBJshape_2_pt':
                        plt.xLabel = pt + ' ' + GeV
                        plt.yLabel = '\langle 1 - ' + ijshape + '\rangle'
                        plt.title = 'integral bjet shape contr. from the edge'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 25
                        plt.xMax = 200
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name in 'intLJshape_2_pt':
                        plt.xLabel = pt + ' ' + GeV
                        plt.yLabel = '\langle 1 - ' + ijshape + '\rangle'
                        plt.title = 'integral lightjet shape contr. from the edge'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 25
                        plt.xMax = 200
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name == 'had_truthVreco_r':
                        plt.xLabel = dr_tVr
                        plt.yLabel = IntNorm
                        plt.title = 'hadronic channel'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0
                        plt.xMax = 4.2
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif plt.name == 'lep_truthVreco_r':
                        plt.xLabel = dr_tVr
                        plt.yLabel = IntNorm
                        plt.title = 'leptonic channel'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0
                        plt.xMax = 4.2
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        #includes also same_bljets_nch_r
        elif 'bljets_nch_r' in plt.name:
                        plt.xLabel = dr_bVl
                        plt.yLabel = mnch + '/' +ddr
                        if 'same' in plt.name:
                            plt.title = t_sbl
                        else:
                            plt.title = t_cbl
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.5
                        plt.xMax = 4.2
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif 'bljets_mpt_r' in plt.name:
                        plt.xLabel = dr_bVl
                        plt.yLabel = mpt + '/' + ddr + ' ' + GeV
                        if 'same' in plt.name:
                            plt.title = t_sbl
                        else:
                            plt.title = t_cbl
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.5
                        plt.xMax = 4.2
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif 'bljets_r' in plt.name:
                        plt.xLabel = dr_bVl
                        plt.yLabel = IntNorm
                        if 'same' in plt.name:
                            plt.title = t_sbl
                        else:
                            plt.title = t_cbl
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.5
                        plt.xMax = 4.2
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
                        plt.yLeg = 0.8
        elif 'pullangle' in plt.name:
                        plt.yLabel = IntNorm
                        if 'b1' in plt.name:
                            plt.xLabel = pangle + '(b1,b2)'
                            plt.title = 'calculated from bjet1'
                        elif 'b2' in plt.name:
                            plt.xLabel = pangle + '(b2,b1)'
                            plt.title = 'calculated from bjet2'
                        elif 'lj1' in plt.name:
                            plt.xLabel = pangle + '(l1,l2)'
                            plt.title = 'calculated from lightjet1'
                        elif 'lj2' in plt.name:
                            plt.xLabel = pangle + '(l2,l1)'
                            plt.title = 'calculated from lightjet2'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 0
                        plt.xMin = 0
                        plt.xMax = 3.2
                        plt.xLeg = 0.6
                        plt.yLeg = 0.8
        elif 'topetaRvT' in plt.name:
                        if 'RvT' in plt.name:
                                plt.xLabel = topetareco
                                plt.yLabel = topetatruth
                                if  'atop' in plt.name:
                                                plt.title = 'eta correlation matrix between reco and truth antitop'
                                else:
                                                plt.title = 'eta correlation matrix between reco and truth top'
                        elif 'LvH' in plt.name:
                                plt.xLabel = topetahad
                                plt.yLabel = topetalep
                                plt.title = 'eta correlation matrix between top and antitop'

                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.
                        plt.xMax = 2.5
                      #  plt.xLeg = 0.6
                      #  plt.yLeg = 0.8
        elif 'topphiRvT' in plt.name:
                        if 'RvT' in plt.name:
                                plt.xLabel = topetareco
                                plt.yLabel = topetatruth
                                if  'atop' in plt.name:
                                                plt.title = 'phi correlation matrix between reco and truth antitop'
                                else:
                                                plt.title = 'phi correlation matrix between reco and truth top'
                        elif 'LvH' in plt.name:
                                plt.xLabel = topetahad
                                plt.yLabel = topetalep
                                plt.title = 'phi correlation matrix between top and antitop'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.0
                        plt.xMax = 1.0
                       # plt.xLeg = 0.6
                       # plt.yLeg = 0.8
        elif 'topptRvT' in plt.name:
                        if 'RvT' in plt.name:
                                plt.xLabel = topetareco
                                plt.yLabel = topetatruth
                                if  'atop' in plt.name:
                                                plt.title = 'pt correlation matrix between reco and truth antitop'
                                else:
                                                plt.title = 'pt correlation matrix between reco and truth top'
                        elif 'LvH' in plt.name:
                                plt.xLabel = topetahad
                                plt.yLabel = topetalep
                                plt.title = 'pt correlation matrix between top and antitop'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.
                        plt.xMax = 300.
                        #plt.xLeg = 0.6
                        #plt.yLeg = 0.8
        elif plt.name == 'closeblcor':
                        plt.xLabel = dr_bVb
                        plt.yLabel = dr_bVl
                        plt.title = 'Using closest bjet to lightjet'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.
                        plt.xMax = 4.2
                        #plt.xLeg = 0.6
                        #plt.yLeg = 0.8
        elif plt.name == 'sameblcor':
                        plt.xLabel = dr_bVb
                        plt.yLabel = dr_bVl
                        plt.title = 'Using bjet from the same decay as ljets'
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.
                        plt.xMax = 4.2
                        #plt.xLeg = 0.6
                        #plt.yLeg = 0.8
        elif plt.name == 'ttbar_deta':
                        plt.xLabel = '$\Delta \eta (t,\bar{t})$'
                        plt.yLabel = IntNorm
                        plt.title = ''
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.
                        plt.xMax = 2.5
                        #plt.xLeg = 0.6
                        #plt.yLeg = 0.8
        elif plt.name == 'ttbar_dphi':
                        plt.xLabel = '$\Delta \phi (t,\bar{t})$'
                        plt.yLabel = IntNorm
                        plt.title = ''
                        plt.ratioRef = dict + plt.name
                        plt.normSum = 1
                        plt.logY = 1
                        plt.xMin = 0.
                        plt.xMax = 1.
                        #plt.xLeg = 0.6
                        #plt.yLeg = 0.8
