#!/usr/bin/python
# -*- coding: utf-8 -*-
import os 
from os.path import join
from os import listdir
#Need to change working dir if needed
#working_dir = "/home/matous/work/top/plots/dilepton/v4/"
working_dir = "/home/matous/work/top/plots/v27/"
#working_dir = "/home/matous/work/top/plots/ue/v1/"
comparison_fLag = True
files =  os.listdir( working_dir)
yoda_files = [y for y in files if y.find('.yoda') != -1]
YODA = []
#Move CR OFF to be first element
#Todo sorting mechanism
schemes = ["default_resOff", "default_resOn", "cr_off", "forced_random", "forced_nearest", "forced_farthest", "forced_minDlambda", "forced_smallest", "swap", "swap_flip1", "swap_flip2", "move", "move_flip1", "move_flip2"]
for iS in range(len(schemes)):
        for iC in range(len(yoda_files)):
              # print schemes[iS]
              # print yoda_files[iC]
               if schemes[iS] in yoda_files[iC]:
                        YODA.append(yoda_files[iC])


print YODA
output_dir_names = []
plot_file = open( join(working_dir, 'plot_top.sh'), 'w')

if not comparison_fLag:
   for yoda_file in YODA:
      output_dir_name = yoda_file.split('.')
      #output_dir_names.append( join( working_dir, output_dir_name[0] ))
      output_dir = join( working_dir, output_dir_name[0] )
      plot_file.write( 'rivet-mkhtml %s -o %s \n' % (yoda_file, output_dir) )
else:
  plot_file.write( 'rivet-cmphistos ')
  for yoda_file in YODA:
      #output_dir_name = yoda_file.split('.')
      #output_dir_names.append( join( working_dir, output_dir_name[0] ))
      #output_dir = join( working_dir, output_dir_name[0] )
      plot_file.write( '%s ' % (yoda_file) )
  plot_file.write( '-o%s\n' % (join(working_dir, 'cr_comparison') ) )
  #plot_file.write( 'rivet-mkhtml %s -o%s \n' % ( join( working_dir, 'Rivet_comparison.yoda'), join(working_dir,'Rivet_comparison')) )
  plot_file.write( 'make-plots --pdf %s/*.dat \n' % ( join( working_dir, 'cr_comparison')  ) )


  #@TODO print cr modes together
  #rivet-mkhtml Rivet_m1.yoda


