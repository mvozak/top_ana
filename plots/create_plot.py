#!/usr/bin/env python
#Macro to create plots
import os
from os.path import isdir, join  #check existence of dir, join paths
import copy
#from create_plot import compare_plot, set_plot
from plot_settings import compare_plot, set_plot
#@Todo create dict of observables
#@Todo dict of cr schemes

#This can be done already in pythia8 generation
#This function implemented in export.py function
# -> Move to external file that both plot and export python files can import?
def get_name( outname ):
   modeDict[ rivOutname + '0' ] = 'cr_off'
   modeDict[ rivOutname + '1' ] = 'default_resOff'
   modeDict[ rivOutname + '2' ] = 'default_resOn'
   modeDict[ rivOutname + '3' ] = 'swap'
   modeDict[ rivOutname + '4' ] = 'swap_flip1'
   modeDict[ rivOutname + '5' ] = 'swap_flip2'
   modeDict[ rivOutname + '6' ] = 'move'
   modeDict[ rivOutname + '7' ] = 'move_flip1'
   modeDict[ rivOutname + '8' ] = 'move_flip2'
   modeDict[ rivOutname + '9' ] = 'forced_random'
   modeDict[ rivOutname + '10' ] = 'forced_nearest'
   modeDict[ rivOutname + '11' ] = 'forced_farthest'
   modeDict[ rivOutname + '12' ] = 'forced_minDlambda'
   modeDict[ rivOutname + '13' ] = 'forced_Dlambda'
   #@Todo check whether it is in
   if modeDict.has_key(outname):
      return modeDict[outname]
   else:
      print 'Error, not in the directory'
      return False


plot_dir = '/home/matous/work/top/plots/'
rivet_output = '.yoda'
version = raw_input(" Select a version (e.g v8) \n")
working_dir = join(plot_dir, version)
analysis_name = 'ATLAS_2016_UETop'

all_plots = []
#Check existence of working dir
if not isdir( working_dir ):
    print '%s is not created' & working_dir
    quit()

# = ['bjetcorrelation', 'lightjetcorrelation']

#Load list of available cr schemes for selected version (only names without .yoda extension)
dir_entries = os.listdir( working_dir )
cr_schemes = [mode.split('.')[0] for mode in dir_entries if rivet_output in mode]
#Move CR OFF to be first element
for iC in range( len(cr_schemes)):
    if cr_schemes[iC] in 'cr_off':
        cr_schemes.insert(0, cr_schemes.pop(iC) )
    elif (len(cr_schemes) > 1) and cr_schemes[iC] in 'forced_random':
        cr_schemes.insert(1, cr_schemes.pop(iC) )
    elif (len(cr_schemes) > 2) and cr_schemes[iC] in 'swap':
        cr_schemes.insert(2, cr_schemes.pop(iC) )
    elif (len(cr_schemes) > 3) and cr_schemes[iC] in 'move':
        cr_schemes.insert(3, cr_schemes.pop(iC) )


#print cr_schemes
_cr_schemes = []  #Full path
for scheme in cr_schemes:
    _cr_schemes.append( "/" + scheme + '.yoda/' + analysis_name + '/')

#CMS plots
ue_observables = [ 'spt', 'mpt', 'nch', 'pt', 'spt_pt', 'mpt_pt', 'nch_pt', 'mpt_nch', 'spt_nch']
#regions = [ 'all', 'to', 'tr', 'aw']

pt_obs = ['pt_e', 'pt_mu', 'pt_lj', 'pt_leadinglj', 'pt_restljs', 'pt_w_h', 'pt_w_l', 'ptT_l', 'ptT_h']
ttbar_article = ['difjetshape', 'difbjetshape', 'difabjetshape', 'difl1jetshape', 'difl2jetshape', 'tmass_hadronic', 'bbjetcorrelation', 'lightjetcorrelation', 'tmass_leptonic', 'wmass_hadronic', 'wmass_leptonic']
#ttbar_article2 = ['spt', 'nch', 'pt', 'mpt_nch', 'spt_nch']
suffix = [ 'hb', 'lb', 'l1', 'l2', 'inc', 'ntrk', 'all', 'to', 'tr', 'trmax', 'trmin', 'aw' ]

#ttbar_article_regions = []
#for obs in ttbar_article2:
#    for i in suffix:
#        suf = '%s' %i
#        ue_obs = obs + '_%s' % suf
#        ttbar_article_regions.append( ue_obs )
#ttbar_article.extend( ttbar_article_regions )
#jplots = [ 'difBJ', 'difLJ', 'intBJ', 'intLJ']
#r_annulus = [ (2+4*i) for i in range(10) ]
jshape_plots = []
for type in ['dif', 'int']:
        for jet in ['BJ', 'LJ']:
                for obs in ['shape', 'nch']:
                        for ptc in ['none', '25_50GeV', '50_100GeV', '100_hGeV']:
                                for i in [ (2+4*i) for i in range(10) ]:
                                        jshape_plots.append( type + jet + obs +  '_%i_%s' % ( i, ptc) )
#Additional jetshape plots
jshape_plots.append( 'intBJshape_2_eta')
jshape_plots.append( 'intLJshape_2_eta')
jshape_plots.append( 'intBJshape_2_pt')
jshape_plots.append( 'intLJshape_2_pt')


plots = ['bljets_nch_r', 'bljets_mpt_r', 'bljets_r' ]
Rplots = []
for pl in plots:
        Rplots.append('same'+ pl )
        Rplots.append( pl )

Rplots.extend(  ['had_truthVreco_r', 'lep_truthVreco_r'] )
pa = ['pullangle_b1', 'pullangle_b2', 'pullangle_lj1','pullangle_lj2']

#Create a .plot file with given settings
plotFile = open( join( working_dir, analysis_name) + '.plot', 'w') #rewrite an existing file
#mkplotFile = open( join( working_dir, 'cr_comparison' ) + '/mkplot.sh', 'w') #rewrite an existing file
mkplotFile = open( working_dir + '/mkplot.sh', 'w') #rewrite an existing file
#Comparison plots in regions

h2 = ['topetaRvT', 'topphiRvT', 'topptRvT', 'closeblcor', 'sameblcor']
#Add ttbar plots from sjostrand and agr.
Cplot = compare_plot()
ttbar_plots = []
for iM in ttbar_article:
   plt = copy.deepcopy( Cplot )
   plt.name = iM
   set_plot(plt) 
   for iC in _cr_schemes:
                plt.add_plot( iC + iM  )
   ttbar_plots.append( plt )
all_plots.extend(ttbar_plots)


#Add object pt plots
pt_plots = []
for iO in pt_obs:
   plt = copy.deepcopy( Cplot )
   plt.name = iO
   set_plot(plt)
   for iC in _cr_schemes:
                plt.add_plot( iC + iO  )
   pt_plots.append( plt )
all_plots.extend(pt_plots)


#Add pullangle plots
pa_plots = []
for iO in pa:
   plt = copy.deepcopy( Cplot )
   plt.name = iO
   set_plot(plt)
   for iC in _cr_schemes:
                plt.add_plot( iC + iO  )
   pa_plots.append( plt )
all_plots.extend(pa_plots)

#Add plots connected to the deltaR between jets
dr_plots = []
for iO in Rplots:
    plt = copy.deepcopy( Cplot )
    plt.name = iO
    set_plot(plt)
    for iC in _cr_schemes:
                 plt.add_plot( iC + iO  )
    dr_plots.append( plt )
all_plots.extend(dr_plots)

#Add jet shape plots
shape_plots = []
for iO in jshape_plots:
    plt = copy.deepcopy( Cplot )
    plt.name = iO
    set_plot(plt)
    for iC in _cr_schemes:
                 plt.add_plot( iC + iO  )
    shape_plots.append( plt )
all_plots.extend(shape_plots)

#Add CMS plots
ue_plots = []
for iP in ue_observables:
        for iR in suffix:
                plt = copy.deepcopy( Cplot )
                plt.name = iP + '_' + iR
                set_plot(plt)
                #@Todo set cr_off as ratioReference
                for iC in _cr_schemes:
                        plt.add_plot( iC + iP + '_' + iR  )
                        print iC + iP + '_' + iR
                ue_plots.append( plt )
all_plots.extend(ue_plots)

#Add h2 plots
h2_plots = []
for iO in h2:
    plt = copy.deepcopy( Cplot )
    plt.name = iO
    set_plot(plt)
    for iC in _cr_schemes:
                 plt.add_plot( iC + iO  )
    h2_plots.append( plt )
all_plots.extend(h2_plots)

#for iP in ue_observables:
 #       for iC in _cr_schemes:
  #              plt = copy.deepcopy( Cplot )
   #             for iR in regions:
    #                    plt.add_plot( iC + iP + '_' + iR  )
     #           cms_plots.append( plt )

mk_pl = []
#mk_pl.extend( shape_plots )
#mk_pl.extend( dr_plots )
mk_pl.extend( pa_plots )
#mk_pl.extend( pt_plots )
#mk_pl.extend( cms_plots )
mk_pl.extend( ttbar_plots )

for iP in all_plots:
#for iP in shape_plots:
#for iP in dr_plots:
    name = analysis_name + '_' + iP.name + '.dat'
    mkplotFile.write( 'make-plots %s \n' %(name) )


for iP in all_plots:
        print >> plotFile,\
        """
# BEGIN PLOT %(plt_name)s
RatioPlotYLabel= %(rTitle)s
LegendOnly= %(Leg)s
XLabel= %(xLabel)s
YLabel= %(yLabel)s
RatioPlotReference=%(rRef)s
DrawOnly=%(Leg)s
RatioPlot=%(rPlot)s
Title=%(Title)s
Legend=1
Rebin=%(reb)s
LogY=%(logy)s
LogX=%(logx)s
XMin=%(xmin)s
XMax=%(xmax)s
RatioPlotYMin=%(rymin)s
RatioPlotYMax=%(rymax)s
LegendXPos=%(lxp)s
LegendYPos=%(lyp)s
NormalizeToIntegral=%(norm)s
# END PLOT

""" % {
'plt_name':  ('/' + analysis_name + '/' + iP.name),
'rTitle': iP.ratioTitle,
'Title': iP.title,
'logy' : iP.logY,
'logx' : iP.logX,
'Leg'    : iP.legends,
'xLabel'  : iP.xLabel,
'yLabel'  : iP.yLabel,
'rPlot'	: iP.ratio,
'rRef': iP.ratioRef,
'lxp': iP.xLeg,
'lyp': iP.yLeg,
'xmin': iP.xMin,
'xmax': iP.xMax,
'norm': iP.normSum,
'reb' : iP.rebin,
'rymin': iP.yRMin,
'rymax': iP.yRMax

        }
