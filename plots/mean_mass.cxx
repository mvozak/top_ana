#include "TFile.h"
#include "TH1D.h"
#include <iostream>
int main(){
    //std::string plot_dir = "p";
    std::string version = "v14/";
    std::vector< std::string > cr_schemes;
    cr_schemes.push_back( "cr_off");
    cr_schemes.push_back( "default_resOff");
    cr_schemes.push_back( "default_resOn");
    cr_schemes.push_back( "swap");
    cr_schemes.push_back( "move");
    cr_schemes.push_back( "swap_flip1");
    cr_schemes.push_back( "forced_random");

    std::string working_dir = version;
   double meanCRoff = 0;
    for( auto cr : cr_schemes){
        TFile* f = new TFile(( working_dir + cr  + ".root" ).c_str(), "READ" );
        f->ls();
        TH1D* h = dynamic_cast<TH1D*>( f->Get(  "ATLAS_2016_UETop/tmass_hadronic"   ) );
         if( cr.find( "cr_off" ) != -1){
        meanCRoff = h->GetMean();
        std::cout <<  " mean CRoff: " << meanCRoff << std::endl;
         }
        double mean = h->GetMean();
        double meanErr = h->GetMeanError();
        std::cout << cr << " mean: " << mean << " meanEr: " << meanErr << " diff to cr off"<< mean - meanCRoff << std::endl;
    }
    return 0;
}
