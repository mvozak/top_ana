#! /usr/bin/env python
# -*- python -*-
import optparse, os
from os import listdir, system
import ROOT
from ROOT import TFile, TH1D
from os.path import isdir, join  #check existence of dir, join paths

#from subprocess import call -> possible change for os
#Wut? How does this works?
op = optparse.OptionParser(usage=__doc__)
opts, args = op.parse_args()
#f = ROOT.TFile(args[0])


#Sort Colour reconnection models
def sort_cr(CR_LIST):
#Move CR OFF to be first element
 for iC in range( len(CR_LIST)):
    if 'cr_off' in CR_LIST[iC]:
        CR_LIST.insert(0, CR_LIST.pop(iC) )
    elif (len(CR_LIST) > 1) and 'default_resOff' in CR_LIST[iC]:
        CR_LIST.insert(1, CR_LIST.pop(iC) )
    elif (len(CR_LIST) > 2) and 'defaut_resOn' in CR_LIST[iC]:
        CR_LIST.insert(2, CR_LIST.pop(iC) )
    elif (len(CR_LIST) > 3) and 'swap' in CR_LIST[iC]:
        CR_LIST.insert(3, CR_LIST.pop(iC) )
    elif (len(CR_LIST) > 4) and 'move' in CR_LIST[iC]:
        CR_LIST.insert(4, CR_LIST.pop(iC) )
    elif (len(CR_LIST) > 5) and 'swap_flip1' in CR_LIST[iC]:
        CR_LIST.insert(5, CR_LIST.pop(iC) )
    elif (len(CR_LIST) > 6) and 'forced_random' in CR_LIST[iC]:
        CR_LIST.insert(6, CR_LIST.pop(iC) )

def get_mean(VERSION, CR_MODES, OBS, MeanDict, ErrDict):
    for cr in CR_MODES:
           #@Todo Check if f exists
           f = TFile( join( VERSION, cr) + '.root' )
           if f.IsZombie():
               print 'file %s not found' % cr+ '.root'
           if not f.IsOpen():
               print 'file %s not open' % cr+ '.root'
           #f.cd("ATLAS_2016_UETop")
           full_path = "ATLAS_2016_UETop" + "/" +OBS
           #@Note potentional problem if name of the analysis is changed
           h = f.Get(full_path)
           MeanDict[cr] = float( h.GetMean() )
           ErrDict[cr] = float( h.GetMeanError() )
           f.Close
           del f
def compute_rescaling(MeanDict, ErrDict, Rescaling):
    WNominal = 80.370    #80370 +- 19 MeV http://atlas.cern/updates/physics-briefing/measuring-w-boson-mass
    for cr in CR_MODES:
        Rescaling[cr] = MeanDict[cr]/WNominal

def create_simplemass_table(VERSION, CR_MODES, OBS, MeanDict, ErrDict, Rescaling ):
#Todo potential problems if name is changed
    MeanNominal = MeanDict['cr_off']
    MeanNominalRes = MeanDict['cr_off']*1/Rescaling['cr_off']

    tmassFile = open( (OBS + '_table.tex') , 'w') #rewrite an existing file
    tmassFile.write( '\\begin{table}[htp] \n')
    tmassFile.write( '\\begin{center} \n')
    tmassFile.write( '\\begin{tabular}{|l|l|l|l|} \n')
    tmassFile.write( '\\toprule \n')
    whichMass =  'top' if 'tmass' in OBS else 'w'
    tmassFile.write( 'CR model & \m%s [GeV] & \dm%s [MeV] & \m%s(cr)/\m%s(nominal)  \\\ \n' %(whichMass, whichMass, whichMass, whichMass))
    tmassFile.write( '\midrule \n')
    for cr in CR_MODES:
        DeltaM = 1000*(MeanDict[cr]-MeanNominal)
        MeanCRRes = (MeanDict[cr]*1/Rescaling[cr])
        DeltaMRescaled = 1000*( MeanCRRes -MeanNominalRes)
        cr_name = cr.replace('_', ' ')
        tmassFile.write( ' %s & %0.3f $\pm$ %0.3f & %0.3f & %0.3f  \\\ \n' %(cr_name, MeanDict[cr], ErrDict[cr], DeltaM, Rescaling[cr] ) )
    #    print 'CR MODE: %s' %(cr)
    #    print 'MeanCRoff: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanNominal, Rescaling[cr], MeanNominalRes)
    #    print 'MeanCR: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanDict[cr], Rescaling[cr], MeanCRRes)
    tmassFile.write( '\\bottomrule \n')
    tmassFile.write( '\end{tabular} \n')
    tmassFile.write( '\caption{ The last column represents the difference in mass between a given model and model where CR is off } \n')
    tmassFile.write( '\end{center} \n')
    tmassFile.write( '\end{table} \n')

def create_resmass_table(VERSION, CR_MODES, OBS, MeanDict, ErrDict, Rescaling ):
#Todo potential problems if name is changed
    MeanNominal = MeanDict['cr_off']
    MeanNominalRes = MeanDict['cr_off']*1/Rescaling['cr_off']

    tmassFile = open( (OBS + '_table.tex') , 'w') #rewrite an existing file
    tmassFile.write( '\\begin{table}[htp] \n')
    tmassFile.write( '\\begin{center} \n')
    tmassFile.write( '\\begin{tabular}{|l|l|l|l|l|l|} \n')
    tmassFile.write( '\\toprule \n')
    whichMass =  'top' if 'tmass' in OBS else 'w'
    tmassFile.write( 'CR model & \m%s [GeV] & \dm%s [MeV] & \m%s(cr)/\m%s(nominal) & \m%s(res) & \dm%s(res) [GeV]\\\ \n' %(whichMass, whichMass, whichMass, whichMass, whichMass, whichMass))
    tmassFile.write( '\midrule \n')
    for cr in CR_MODES:
        DeltaM = 1000*(MeanDict[cr]-MeanNominal)
        MeanCRRes = (MeanDict[cr]*1/Rescaling[cr])
        DeltaMRescaled = 1000*( MeanCRRes -MeanNominalRes)
        cr_name = cr.replace('_', ' ')
        tmassFile.write( ' %s & %0.3f $\pm$ %0.3f & %0.3f & %0.3f & %0.3f & %0.3f \\\ \n' %(cr_name, MeanDict[cr], ErrDict[cr], DeltaM, Rescaling[cr], MeanCRRes, DeltaMRescaled ) )
        print 'CR MODE: %s' %(cr)
        print 'MeanCRoff: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanNominal, Rescaling[cr], MeanNominalRes)
        print 'MeanCR: %0.4f  RES: %0.4f MeanCRoffRes: %0.4f' % (MeanDict[cr], Rescaling[cr], MeanCRRes)
        print 'Difference %0.4f' % (DeltaMRescaled)
    tmassFile.write( '\\bottomrule \n')
    tmassFile.write( '\end{tabular} \n')
    tmassFile.write( '\caption{ The last column represents the difference in mass between a given model and model where CR is off } \n')
    tmassFile.write( '\end{center} \n')
    tmassFile.write( '\end{table} \n')



##############BEGIN##########
VERSION = 'v21'
OBS = ['tmass_hadronic', 'wmass_hadronic']
WORKING_DIR = VERSION + '/'
#CR_MODES = [ m for m in LIST if '.yoda' in m ]
#CR_YODA = [ m for m in os.listdir( WORKING_DIR ) if '.yoda' in m ]
CR_YODA = [ m for m in os.listdir( WORKING_DIR ) if '.yoda' in m ]
CR_MODES = []
for yoda in CR_YODA:
        m = yoda.split('.')
        CR_MODES.append( m[0] )
#Create .root files
for cr in CR_MODES:
        os.system( "yoda2root %s.yoda %s.root" % (join(WORKING_DIR,cr),join(WORKING_DIR,cr)) )
#Do modes ordering
#print CR_MODES
sort_cr(CR_MODES)
#W mass table
WMassDict = {}
WMassErrDict = {}
Rescaling = {}
get_mean(VERSION, CR_MODES, 'wmass_hadronic', WMassDict, WMassErrDict)
compute_rescaling(WMassDict, WMassErrDict, Rescaling)
create_simplemass_table(VERSION, CR_MODES, 'wmass_hadronic', WMassDict, WMassErrDict, Rescaling)
#Top mass table
TMassDict = {}
TMassErrDict = {}
get_mean(VERSION, CR_MODES, 'tmass_hadronic', TMassDict, TMassErrDict)
create_resmass_table(VERSION, CR_MODES, 'tmass_hadronic', TMassDict, TMassErrDict,Rescaling)
