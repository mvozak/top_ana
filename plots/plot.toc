\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Content}{1}{section.1}
\contentsline {section}{\numberline {2}Masses}{1}{section.2}
\contentsline {section}{\numberline {3}Basic UE observables}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Nch distribution}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Pt distribution}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Sumpt distribution}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Mean sumpt wrt Nch}{11}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Mean pt wrt Nch}{13}{subsection.3.5}
\contentsline {section}{\numberline {4}Appendix}{15}{section.4}
\contentsline {section}{\numberline {5}Bjet shapes}{17}{section.5}
\contentsline {subsubsection}{\numberline {5.0.1}Differential Bjet SUMPT shapes}{17}{subsubsection.5.0.1}
\contentsline {subsubsection}{\numberline {5.0.2}Differential Bjet multiplicity shapes}{22}{subsubsection.5.0.2}
\contentsline {section}{\numberline {6}Outside Bjet shapes}{27}{section.6}
\contentsline {section}{\numberline {7}Lightjet shapes}{27}{section.7}
\contentsline {section}{\numberline {8}Outside Lightjet shapes}{27}{section.8}
\contentsline {section}{\numberline {9}Basic UE observables (all regions)}{27}{section.9}
\contentsline {section}{\numberline {10}Basic UE observables with respect to pttbar}{27}{section.10}
\contentsline {section}{\numberline {11}Area between jets}{27}{section.11}
\contentsline {section}{\numberline {12}Pull angle}{27}{section.12}
