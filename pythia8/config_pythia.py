#!/usr/bin/env python
import os
from os import listdir  #to list directories
from os.path import isdir, join  #check existence of dir, join paths
import copy
import random

class PythiaConfig:
   CR_Id = 0
   name = 'ttbar_cr_mode0.cc'
   beam1_id = '2212'
   beam2_id = '2212'
   cms_energy = '13000'
  # number_of_events = '10000'
  # gg2ttbar= 'on'  #allowed processes
  # qqbar2ttbar= 'on'  #allowed processes
   reconnect= 'off'
   early_resonances= 'off'
   pT0_cutoff = 2.28 #default value in pythia8 (4.1.2016)
  #Hook settings
   hook = 'false'
   #hook_mode = 1
   #flip = 0

#@Todo move paths to different path so it can be shared between rivet and pythia
rmFlag = False
pythia_dir = "/home/vozak/top_ana/pythia8"
setup_dir = "/home/vozak/setup"

#Choose type of events (Minimum bias or top)
type_choice = eventType = raw_input("Choose top, mb or inclusive jet (top/mb/incjet) \n")
cMB = ('mb', 'm')
cTOP = ('top', 't')
cDJ = ('incjet', 'ij')
eT = type_choice.strip().lower()
if eT in cTOP:
   pyFile_name = 'ttbar_cr'
   pyOutput = 'top'
if eT in cMB:
   pyFile_name = 'mb_cr'
   pyOutput = 'mb'
if eT in cDJ:
   pyFile_name = 'incjets'
   pyOutput = 'incjet'
pythia_exe_name = pyFile_name


bin_dir = "/home/vozak/bin"
_pythia_exe = join(pythia_dir, pythia_exe_name )
#@Todo check cms energy
CMS = '13000'
cms = ''
if CMS == '13000':
   cms = '13'
elif CMS == '7000':
   cms = '7'
elif CMS == '8000':
   cms = '8'

job_dir = 'Jobs' + cms + 'TeV/pythia8Jobs/' + pyOutput
#job_dir = 'Jobs8TeV/pythia8Jobs/' + pyOutput

working_dir = join( pythia_dir, job_dir)
#file_flags= []
#mode_working_dirs = []

print working_dir
PConfig = PythiaConfig()
PConfig.cms_energy = CMS
##setup of different colour reconnection modes
cr_modes = []
#No reconnection
mode = copy.deepcopy( PConfig )
mode.CR_Id = 0
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'off'
mode.hook = 'false'
#cr_modes.append( mode )

#Standard reconnection, but top decay products unaffected.
mode = copy.deepcopy( PConfig )
mode.CR_Id = 1
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'off'
#mode.hook = 'off'
mode.hook = 'false'
#cr_modes.append( mode )

#Standard reconnection, including top decay products.
mode = copy.deepcopy( PConfig )
mode.CR_Id = 2
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'on'
#mode.hook = 'off'
mode.hook = 'false'
#cr_modes.append( mode )

#Swap
mode = copy.deepcopy( PConfig )
mode.CR_Id = 3
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#mode.hook_mode = 1
#mode.flip = 0
mode.pT0_cutoff = 2.30
#cr_modes.append( mode )


#Swap + flip 1
mode = copy.deepcopy( PConfig )
mode.CR_Id = 4
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#mode.hook_mode = 1
#mode.flip = 1
mode.pT0_cutoff = 2.20
#cr_modes.append( mode )



#Swap + flip 2
mode = copy.deepcopy( PConfig )
mode.CR_Id = 5
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#mode.hook_mode = 1
#mode.flip = 2
mode.pT0_cutoff = 2.20
#cr_modes.append( mode )

#Move
mode = copy.deepcopy( PConfig )
mode.CR_Id = 6
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#mode.hook_mode = 2
#mode.flip = 0
mode.pT0_cutoff = 2.25
#cr_modes.append( mode )


#Move + flip 1
mode = copy.deepcopy( PConfig )
mode.CR_Id = 7
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#mode.hook_mode = 2
#mode.flip = 1
mode.pT0_cutoff = 2.15
#cr_modes.append( mode )

#Move + flip 2
mode = copy.deepcopy( PConfig )
mode.CR_Id = 8
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'off'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#mode.hook_mode = 2
#mode.flip = 2
mode.pT0_cutoff = 2.15
#cr_modes.append( mode )

#New models
#"FORCED RANDOM"  - Reconnect with random background gluon.
mode = copy.deepcopy( PConfig )
mode.CR_Id = 9
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'off'
#mode.hook = 'on'
mode.hook = 'true'
#cr_modes.append( mode )

#"FORCED NEAREST" - Reconnect with nearest (smallest-mass) background gluon.
mode = copy.deepcopy( PConfig )
mode.CR_Id = 10
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'off'
mode.hook = 'true'
#mode.hook = 'on'
cr_modes.append( mode )

#FORCED FARTHEST" - Reconnect with furthest (largest-mass) background gluon.
mode = copy.deepcopy( PConfig )
mode.CR_Id = 11
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'off'
mode.hook = 'true'
cr_modes.append( mode )

#"FORCED SMALLEST DELTA_LAMBDA" -Reconnect with smallest (with sign) lambda measure shift.
mode = copy.deepcopy( PConfig )
mode.CR_Id = 12
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'off'
mode.hook = 'true'
cr_modes.append( mode )

#"SMALLEST DELTA_LAMBDA" - Reconnect only if reduced lamda, and then to most reduction.
#@Todo need to change alfa_s
mode = copy.deepcopy( PConfig )
mode.CR_Id = 13
mode.name = 'ttbar_cr_mode%i.cc' % mode.CR_Id
mode.reconnect= 'on'
mode.early_resonances= 'off'
mode.hook = 'true'
#cr_modes.append( mode )



old_modes_dir = os.listdir( working_dir )

#if os.path.isdir( working_dir): 
if old_modes_dir: 
   message = '%s directory in %s already exists' % (job_dir, pythia_dir)
   remove = True if rmFlag == True else False
   if not remove:
            choices = 'y/n/a'
            choice = raw_input("%s \n Delete (%s) ?" % (message, choices))
            valuesY = ('y', 'yes')
            valuesN = ('n', 'no')
            valuesA = ('a', 'all')
            #strip() removes spaces, lower() return string with lowercased characters 
            ch = choice.strip().lower()
            decision = False
            if ch in valuesY:
                  remove = True

            if ch in valuesA:
                  remove = True
                  rmFlag = True
            if remove:
                  print "Removing modes in" + working_dir
                  os.system( "rm -r %s/*" % working_dir)
            else:
                  print "Modes in %s were not removed" % working_dir 
                  #quit()

submitfile = open(join(pythia_dir, 'submit_pythia.sh'), 'w') #rewrite an existing file
#Events generated in total
events = 10000000
#Events generated per file
max_file_size = 200000
print (events / max_file_size)
print  events % max_file_size
print "%i events generated" % (events) 
nDir_tobecreated = (events / max_file_size) if (events % max_file_size == 0) else ( (events/ max_file_size) + 1) 
print "Splitted into %i files" % int(nDir_tobecreated)
run_list = []
Dir_tobecreated = []
Seed_list = []
#os.mkdir( working_dir )
#@Todo check existance of dir
for m in cr_modes:
      dir_name = 'm%i' % m.CR_Id   
      mode_working_dir = join( working_dir, dir_name )
      os.mkdir( mode_working_dir )
      for iDir in range(0, int(nDir_tobecreated)):
         #Last file has different number of events
         if( iDir == nDir_tobecreated-1 ) and (events % max_file_size != 0):
            events_per_file = (events % max_file_size)
         else:
            events_per_file = max_file_size
         #Create a file with pythia settings
         pythia_card_name = 'ttbar_cr'  
         file_flag = 'm%i_%i' % (m.CR_Id, iDir)
         #file_flags.append( file_flag )
         pythia_card = pythia_card_name + file_flag + '.cmnd'
         pythia_output = pythia_card_name + file_flag + '.hepmc'
         _pythia_output = join( mode_working_dir ,pythia_output)
         pythia_run = 'run_' + file_flag + '.sh'
         _pythia_run = join( mode_working_dir, pythia_run )
         #run_list.append(_pythia_run)
         # _ -> full_path of pythia_card
         #Some of the modes has the same number but does not have hooks
         #cr_internal_modes = m.CR_Id if not m.hook else m.CR_Id - 8
         cr_internal_modes = m.CR_Id

         _pythia_card = join(mode_working_dir , pythia_card)
         pythia_file = open(_pythia_card, 'w')
         Seed = -1
         while ( Seed == -1 or Seed in Seed_list):
            Seed = random.randrange(1, 500000)
         Seed_list.append(Seed)
         print >> pythia_file,\
   """
Beams:idA = %(b1)s  ! first incoming beam is a 2212, i.e. a proton.
Beams:idB = %(b2)s  ! second beam is also a proton.
Beams:eCM = %(energy)s ! the cm energy of collisions.
Main:numberOfEvents = %(nE)s
ColourReconnection:reconnect = %(rec)s 
PartonLevel:earlyResDec = %(res)s 
MultipartonInteractions:pT0Ref = %(ptref)s
Random:setSeed = on
Random:seed = %(sd)s

Main:spareMode1 = %(mode1)s

Main:spareWord1 = %(output)s 
Main:spareWord2 = %(ptref)s
Main:spareWord3 = %(sd)s

Main:spareFlag1 = %(hook)s



         """ % { 
         'b1'    : m.beam1_id,
         'b2'    : m.beam2_id,
         'nE'    : events_per_file,
         'energy': m.cms_energy,
         'rec'   : m.reconnect,
         'res'   : m.early_resonances,
         'output': _pythia_output, 
         'mode1' : cr_internal_modes,
        # 'mode2' : m.hook_mode,
        # 'mode3' : m.flip,
         'ptref' : m.pT0_cutoff,
         'hook'  : m.hook,
         'sd'  : Seed
         }

         run_file = open(join(mode_working_dir, pythia_run), 'w')
         run_file.write( 'export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n')
         run_file.write( 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n')
         run_file.write( 'source %s \n' %join(setup_dir, 'setup_pythia8.sh') )
         run_file.write( 'cd %s \n' % pythia_dir )
         run_file.write( 'source %s \n' %join(setup_dir, 'setup_compilator.sh') )
         run_file.write( './%s %s \n' % (pythia_exe_name, _pythia_card))
         #@Todo iatlas or gridatlas?
         submitfile = open(join(pythia_dir, 'submit_pythia.sh'), 'a')
         submitfile.write( 'cd %s \n' % mode_working_dir )
         submitfile.write( '%s/send2pbsWithLimit gridatlas %s \n' % (bin_dir, _pythia_run) )


print 'Generated seeds'
print Seed_list
#Aditional settings in card file -> for now moved directly to pythia macro in order to use same card for mb and top pythia macros
#Top:gg2ttbar = %(gg)s ! switch on the process g g -> t tbar.
#Top:qqbar2ttbar = %(qqbar)s ! switch on the process q qbar -> t tbar.
#ColourReconnection:mode = 0


