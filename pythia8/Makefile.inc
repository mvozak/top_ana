# PYTHIA configuration file.
# Generated on Tue May 10 15:54:28 CEST 2016 with the user supplied options:
# --prefix=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/219-8edc0/x86_64-slc6-gcc48-opt
# --with-hepmc2=/cvmfs/sft.cern.ch/lcg/releases/HepMC/2.06.09-0a23a/x86_64-slc6-gcc48-opt
# --with-lhapdf6=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/6.1.5.cxxstd-552fd/x86_64-slc6-gcc48-opt
# --enable-shared
# --with-lhapdf6-plugin=LHAPDF5.h

# Install directory prefixes.
PREFIX_BIN=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/219-8edc0/x86_64-slc6-gcc48-opt/bin
PREFIX_INCLUDE=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/219-8edc0/x86_64-slc6-gcc48-opt/include
PREFIX_LIB=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/219-8edc0/x86_64-slc6-gcc48-opt/lib
PREFIX_SHARE=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/219-8edc0/x86_64-slc6-gcc48-opt/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=true
#CXX=/afs/cern.ch/sw/lcg/contrib/gcc/4.8/x86_64-slc6/bin/g++
#CXX=/cvmfs/sft.cern.ch/lcg/contrib/gcc/4.8/x86_64-slc6/bin/g++
#CXX=/cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.1/x86_64-slc6/bin/g++
#CXX=/cvmfs/atlas.cern.ch/repo/sw/atlas-gcc/484/x86_64/slc6/gcc48/bin/g++
#CXX=/cvmfs/atlas.cern.ch/repo/sw/atlas-gcc/484/x86_64/slc6/x86_64-slc6-gcc48-opt/bin/g++
#CXX=/cvmfs/atlas.cern.ch/repo/sw/atlas-gcc/481/x86_64/slc6/gcc48/bin/g++
#CXX=/cvmfs/atlas.cern.ch/repo/sw/atlas-gcc/481/x86_64/slc6/x86_64-slc6-gcc48-opt/bin/g++

CXX=g++
CXX_COMMON=-O2 -ansi -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname
LIB_SUFFIX=.so

# EVTGEN configuration.
EVTGEN_USE=false
EVTGEN_BIN=
EVTGEN_INCLUDE=./
EVTGEN_LIB=./

# FASTJET3 configuration.
FASTJET3_USE=false
FASTJET3_BIN=
FASTJET3_INCLUDE=./
FASTJET3_LIB=./

# HEPMC2 configuration.
HEPMC2_USE=true
HEPMC2_BIN=/cvmfs/sft.cern.ch/lcg/releases/HepMC/2.06.09-0a23a/x86_64-slc6-gcc48-opt/bin/
HEPMC2_INCLUDE=/cvmfs/sft.cern.ch/lcg/releases/HepMC/2.06.09-0a23a/x86_64-slc6-gcc48-opt/include
HEPMC2_LIB=/cvmfs/sft.cern.ch/lcg/releases/HepMC/2.06.09-0a23a/x86_64-slc6-gcc48-opt/lib

# HEPMC3 configuration.
HEPMC3_USE=false
HEPMC3_BIN=
HEPMC3_INCLUDE=./
HEPMC3_LIB=./

# LHAPDF5 configuration.
LHAPDF5_USE=false
LHAPDF5_BIN=
LHAPDF5_INCLUDE=./
LHAPDF5_LIB=./
LHAPDF5_PLUGIN=LHAPDF5.h

# LHAPDF6 configuration.
LHAPDF6_USE=true
LHAPDF6_BIN=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/6.1.5.cxxstd-552fd/x86_64-slc6-gcc48-opt/bin/
LHAPDF6_INCLUDE=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/6.1.5.cxxstd-552fd/x86_64-slc6-gcc48-opt/include
LHAPDF6_LIB=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/6.1.5.cxxstd-552fd/x86_64-slc6-gcc48-opt/lib
LHAPDF6_PLUGIN=LHAPDF5.h

# POWHEG configuration.
POWHEG_USE=false
POWHEG_BIN=
POWHEG_INCLUDE=./
POWHEG_LIB=./

# PROMC configuration.
PROMC_USE=false
PROMC_BIN=
PROMC_INCLUDE=./
PROMC_LIB=./

# ROOT configuration.
ROOT_USE=false
ROOT_BIN=
ROOT_INCLUDE=./
ROOT_LIB=./

# GZIP configuration.
GZIP_USE=false
GZIP_BIN=
GZIP_INCLUDE=./
GZIP_LIB=./

# BOOST configuration.
BOOST_USE=false
BOOST_BIN=
BOOST_INCLUDE=./
BOOST_LIB=./

# PYTHON configuration.
PYTHON_USE=false
PYTHON_BIN=
PYTHON_INCLUDE=./
PYTHON_LIB=./
