#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Pythia8Plugins/ColourReconnectionHooks.h"

using namespace Pythia8; // Let Pythia8:: be implicit.


//@Todo pass not as reference?
bool pass_el(Particle& p){
    bool pass = true;
    //So far only from negative W -> el, in case of both swith to absid()
    //if( p.id() != 11) pass = false;
    if( p.idAbs() != 11) pass = false;
    if( p.pT() < 10.) pass = false;
    if( abs(p.eta()) > 2.7) pass = false;
    if( !p.isFinal() ) pass = false;
    return pass;
}

bool pass_mu(Particle& p){
    bool pass = true;
    //So far only from negative W -> mu, in case of both swith to absid()
    //if( p.id() != 13) pass= false;
    if( p.idAbs() != 13) pass= false;
    if( p.pT() < 10.) pass= false;
    if( abs(p.eta()) > 2.7) pass= false;
    if( !p.isFinal() ) pass = false;
    return pass;
}

int main(int argc, char* argv[]) {
    // Set up anti-kT jet finder, with given deltaR, minPt and etaMax
    SlowJet sJet( -1, 0.4, 20., 4.);
    int cutflow[3] = { 0, 0, 0};
    Pythia pythia;
    Event &event = pythia.event;
    pythia.readFile(argv[1]);
    //Interface for conversion from Pythia8::Event to HepMC event.
    HepMC::Pythia8ToHepMC ToHepMC;
    HepMC::IO_GenEvent ascii_io( pythia.word("Main:spareWord1"), std::ios::out);


   //USER HOOK PART  ------------------------------------
    UserHooks* myUserHooks;
    //Select colour reconnection according to their ID number
    //0 no CR
    //1-2 default
    //3-8 mb models (require mb userhook)
    //9-13 top specialized models (require top userhook)
    int modeID =  pythia.mode("Main:spareMode1");

    //Best correspondence with data for values between 5-10%
    double MB_fracGluon = 1.;
    double TOP_fracGluon = 0.075;
    //Throws warning when compiled, apparently there is a function with if statement
    // if (mode == 5 && dLam > 0.), However, according to the article from Sj,Sp
    //this value should be set to zero to provide maximal CR
    double dLamCut   = 0.; //already by default


    //Flag to check if the model requires (top/mb)userhook from gluon-move schemes
    bool GluonMoveFlag = pythia.flag("Main:spareFlag1");
    //if( mFlag.find("on") != -1 ){
    if( GluonMoveFlag ){
        //mode and modeID is different for Top/MB userhooks
        //int mode = pythia.mode("Main:spareMode2");

        //select user hook according to modeID
        if( modeID >= 3 && modeID <= 8){
        std::cout << "Initializing MBReconUserHook" << std::endl;
            int mode = (modeID <= 5) ? 1 : 2;
            int flip = ( (modeID - 3) * mode) % 3;
            //int flip = pythia.mode("Main:spareMode3");
            //pT0Ref already defined in card file
            myUserHooks = new MBReconUserHooks(mode, flip, dLamCut, MB_fracGluon);
            pythia.setUserHooksPtr( myUserHooks);
        }
        else if( modeID > 8){
        std::cout << "Initializing TopReconUserHook" << std::endl;
        myUserHooks = new TopReconUserHooks( 9 - modeID, TOP_fracGluon );
        pythia.setUserHooksPtr(myUserHooks);
        }
    }
  //--------------------------------------------------------------------


    int nEv = pythia.mode("Main:numberOfEvents");
    //Settings
    //Allows user seed in pythia::init

    pythia.readString("Top:gg2ttbar = on");
    pythia.readString("Top:qqbar2ttbar = on");
    pythia.readString("6:m0 = 172.5"); //Setting the top mass (from ATLAS measurement)
    pythia.readString("24:m0 = 80.385"); //Setting the positive W boson mass (from ATLAS measurement)
    //Q: Why can not be W negative and positive for both cases?
    //pythia.readString("24:onPosIfAny = 1 2 3 4 5"); //Allowing pos W to decay to u,d,s,c,b quarks
    //ONLY DILEPTON CHANNEL
    //pythia.readString("24:onPosIfAny = -11 -13"); //Allowing pos W to decay to el, mu
    //pythia.readString("24:onNegIfAny = 11 13"); //Allowing negative W to decay to el, mu

    pythia.readString("24:onMode = off"); //Allowing W to decay to el, mu
   pythia.readString("24:onIfAny = 11 13"); //Allowing W to decay to el, mu

    pythia.init(); // Initialise pp (PDG 2212) at LHC.
    for(int iEv = 0; iEv < nEv; ++iEv){
         ++cutflow[0];
        //initialization of events
        pythia.next();
        //Print first two events, just for the check
        if( iEv == 0 || iEv == 1) event.list();
        unsigned int n_muons = 0;
        unsigned int n_electrons = 0;
        for( unsigned int iP = 0; iP < event.size(); ++iP){
            if( pass_el(event[iP]) ) ++n_electrons;
            if( pass_mu(event[iP]) ) ++n_muons;
        }
        //Cut on number of electrons and muons
        if( n_electrons == 0 && n_muons == 0) continue;
         ++cutflow[1];
         
        //Cut on number of jets -> at least 4 (without selection on bjets and lightjets)
        sJet.analyze(event);
        int nJet = sJet.sizeJet();
        if(nJet < 2 ) continue;
         ++cutflow[2];
        //Fill  the HepMC file
        HepMC:: GenEvent* hepmcevt= new  HepMC :: GenEvent ();
        ToHepMC.fill_next_event( pythia  , hepmcevt  );
        ascii_io  << hepmcevt;
        delete  hepmcevt;
    }
   std::cout << "CUTFLOW"<<std::endl;
   std::cout << "All: "<< cutflow[0] <<std::endl;
   std::cout << "e_mu: "<< cutflow[1] <<std::endl;
   std::cout << "Jets: "<< cutflow[2] <<std::endl;
   std::cout << "Alpha parameter for MB models set to: "<< MB_fracGluon <<std::endl;
   std::cout << "Alpha parameter for TOP models set to: "<< TOP_fracGluon <<std::endl;
   std::cout << "Delta lambda cut parameter set to: "<< dLamCut <<std::endl;
   std::cout << "pT0ref parameter set to: "<<  pythia.word("Main:spareWord2") <<std::endl;
   std::cout << "CHOSEN SEED (stored as well in .cmnd file)" <<std::endl;
   std::cout << pythia.word("Main:spareWord3") << std::endl;
    return 0;
}



