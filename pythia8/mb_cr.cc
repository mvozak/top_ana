#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Pythia8Plugins/ColourReconnectionHooks.h"

using namespace Pythia8; // Let Pythia8:: be implicit.
int main(int argc, char* argv[]) {
   Pythia pythia;
   pythia.readFile(argv[1]);
   //Interface for conversion from Pythia8::Event to HepMC event.
   HepMC::Pythia8ToHepMC ToHepMC;
   HepMC::IO_GenEvent ascii_io( pythia.word("Main:spareWord1"), std::ios::out);
   //Turn on contributions to Minimum bias (ND most significant)
   pythia.readString("SoftQCD:nonDiffractive = on"); // previously used as SoftQCD:minBias = on
   pythia.readString("SoftQCD:singleDiffractive = on");
   pythia.readString("SoftQCD:doubleDiffractive = on");
   //pythia.readString("Top:gg2ttbar = on");
   //pythia.readString("Top:qqbar2ttbar = on");




   //USER HOOK PART - identical to the one in top ------------------------------------
   UserHooks* myUserHooks;
   //Select colour reconnection according to their ID number
   //0 no CR
   //1-2 default
   //3-8 mb models (require mb userhook)
   //9-13 top specialized models (require top userhook)
   int modeID =  pythia.mode("Main:spareMode1");
   double dLamCut   = 0.;
   double fracGluon = 1.;

   //Flag to check if the model requires (top/mb)userhook
   std::string mFlag = pythia.word("Main:spareWord2");
   if( mFlag.find("on") != -1 ){
       //mode and modeID is different for Top/MB userhooks
       int mode = pythia.mode("Main:spareMode2");

       //select user hook according to modeID
       if( modeID >= 3 && modeID <= 8){
       std::cout << "Initializing MBReconUserHook" << std::endl;
           //int mode = (mLoop <= 5) ? 1 : 2;
           //int flip = ( mLoop - 3 * mode) % 3;
           int flip = pythia.mode("Main:spareMode3");
           //@Todo possible change of parameters?
           //pT0Ref already defined in card file
           myUserHooks = new MBReconUserHooks(mode, flip, dLamCut, fracGluon);
           pythia.setUserHooksPtr( myUserHooks);
       }
       //These modes do not work on events without top quarks
       //else if( modeID > 8){
       //std::cout << "Initializing TopReconUserHook" << std::endl;
       //double gluonStrength = 1.;
       //myUserHooks = new TopReconUserHooks( mode, gluonStrength );
       //pythia.setUserHooksPtr(myUserHooks);
       //}
   }
  //--------------------------------------------------------------------

   int nEv = pythia.mode("Main:numberOfEvents");
   //pythia.readString("6:m0 = 172.5"); //Setting the top mass (from ATLAS measurement)
   //pythia.readString("24:m0 = 80.385"); //Setting the positive W boson mass (from ATLAS measurement)
   //pythia.readString("24:onPosIfAny = 1 2 3 4 5"); //Allowing pos W to decay to u,d,s,c,b quarks
   //pythia.readString("24:onNegIfAny = 11 13"); //Allowing negative W to decay to el, mu

   pythia.init(); // Initialise pp (PDG 2212) at LHC.
   for(int iEv = 0; iEv < nEv; ++iEv){
      //initialization of events
      pythia.next();
      if( iEv == 0 || iEv == 1) pythia.event.list(); 
      //Fill  the HepMC file
      HepMC:: GenEvent* hepmcevt= new  HepMC :: GenEvent ();
      ToHepMC.fill_next_event( pythia  , hepmcevt  );
      ascii_io  << hepmcevt;
      delete  hepmcevt;
   }
   std::cout << "Alpha parameter set to: "<< fracGluon <<std::endl;
   std::cout << "Delta lambda cut parameter set to: "<< dLamCut <<std::endl;
   std::cout << "pT0 cutoff parameter set to: "<<  pythia.word("Main:spareWord3") <<std::endl;
   std::cout << "CHOSEN SEED (stored as well in .cmnd file)" <<std::endl;
   std::cout << pythia.word("Main:spareWord3") << std::endl;
   return 0;
}
