// main73.cc is a part of the PYTHIA event generator.
// Copyright (C) 2016 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Example how to compare "parton-level" and "hadron-level" properties.

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Pythia8Plugins/ColourReconnectionHooks.h"
using namespace Pythia8;

//--------------------------------------------------------------------------

// Generic routine to extract the particles that existed right before
// the hadronization machinery was invoked.

void getPartonLevelEvent( Event& event, Event& partonLevelEvent) {

    // Copy over all particles that existed right before hadronization.
    partonLevelEvent.reset();
    for (int i = 0; i < event.size(); ++i)
        if (event[i].isFinalPartonLevel()) {
            int iNew = partonLevelEvent.append( event[i] );

            // Set copied properties more appropriately: positive status,
            // original location as "mother", and with no daughters.
            partonLevelEvent[iNew].statusPos();
            partonLevelEvent[iNew].mothers( i, i);
            partonLevelEvent[iNew].daughters( 0, 0);
        }

}

//--------------------------------------------------------------------------

// Generic routine to extract the particles that exist after the
// hadronization machinery. Normally not needed, since SlowJet
// contains the standard possibilities preprogrammed, but this
// method illustrates further discrimination.

void getHadronLevelEvent( Event& event, Event& hadronLevelEvent) {

    // Iterate over all final particles.
    hadronLevelEvent.reset();
    for (int i = 0; i < event.size(); ++i) {
        bool accept = false;
        if (event[i].isFinal()) accept = true;

        // Example 1: reject neutrinos (standard option).
        int idAbs = event[i].idAbs();
        if (idAbs == 12 || idAbs == 14 || idAbs == 16) accept = false;

        // Example 2: reject particles with pT < 0.1 GeV (new possibility).
        if (event[i].pT() < 0.1) accept = false;

        // Copy over accepted particles, with original location as "mother".
        if (accept) {
            int iNew = hadronLevelEvent.append( event[i] );
            hadronLevelEvent[iNew].mothers( i, i);
        }
    }

}

//--------------------------------------------------------------------------

int main(int argc, char* argv[]) {
    // Generator. LHC process and output selection. Initialization.
    Pythia pythia;
    pythia.readFile(argv[1]);

    // Number of events, generated and listed ones.
    int nEvent = pythia.mode("Main:numberOfEvents");
    //int nEvent = 500;
    int nListEvts = 1;
    int nListJets = 2;
    int modeID =  pythia.mode("Main:spareMode1");

    // pythia.readString("Beams:eCM = 7000.");
    pythia.readString("Tune:pp = 5"); //4C tune
    //Retune of some models according to Sjostrand/Argyropoulos article about ttbar cr
   if(modeID == 3) pythia.readString( "MultipartonInteractions:pT0Ref = 2.30 ");
   if(modeID == 4) pythia.readString( "MultipartonInteractions:pT0Ref = 2.20 ");
   if(modeID == 5) pythia.readString( "MultipartonInteractions:pT0Ref = 2.20 ");
   if(modeID == 6) pythia.readString( "MultipartonInteractions:pT0Ref = 2.25 ");
   if(modeID == 7) pythia.readString( "MultipartonInteractions:pT0Ref = 2.15 ");
   if(modeID == 8) pythia.readString( "MultipartonInteractions:pT0Ref = 2.15 ");

    pythia.readString("HardQCD:all = on");  //Phasespace::pTHatMin cut necessary
    pythia.readString("PhaseSpace:pTHatMin = 20."); //Minimum invariant pT
    //  pythia.readString("Next:numberShowInfo = 0");
    //  pythia.readString("Next:numberShowProcess = 0");
    //  pythia.readString("Next:numberShowEvent = 0");


    //Interface for conversion from Pythia8::Event to HepMC event.
    HepMC::Pythia8ToHepMC ToHepMC;
    HepMC::IO_GenEvent ascii_io( pythia.word("Main:spareWord1"), std::ios::out);
    //   HepMC::IO_GenEvent ascii_io( "output.hepmc.", std::ios::out);
    //USER HOOK PART  ------------------------------------



     UserHooks* myUserHooks;
     //Select colour reconnection according to their ID number
     //0 no CR
     //1-2 default
     //3-8 mb models (require mb userhook)
     //9-13 top specialized models (require top userhook)
    //Best correspondence with data for values between 5-10%
    double MB_fracGluon = 1.;
    double TOP_fracGluon = 0.075;
    //Throws warning when compiled, apparently there is a function with if statement
    // if (mode == 5 && dLam > 0.), However, according to the article from Sj,Sp
    //this value should be set to zero to provide maximal CR
    double dLamCut   = 0.; //already by default

    //Flag to check if the model requires (top/mb)userhook from gluon-move schemes
    bool GluonMoveFlag = pythia.flag("Main:spareFlag1");
    //if( mFlag.find("on") != -1 ){
    if( GluonMoveFlag ){
        //mode and modeID is different for Top/MB userhooks
        //int mode = pythia.mode("Main:spareMode2");
        //select user hook according to modeID
        if( modeID >= 3 && modeID <= 8){
        std::cout << "Initializing MBReconUserHook" << std::endl;
            int mode = (modeID <= 5) ? 1 : 2;
            int flip = ( (modeID - 3) * mode) % 3;
            //int flip = pythia.mode("Main:spareMode3");
            //pT0Ref already defined in card file
            myUserHooks = new MBReconUserHooks(mode, flip, dLamCut, MB_fracGluon);
            pythia.setUserHooksPtr( myUserHooks);
        }
        //will this model work? probably not Check
        else if( modeID > 8){
        std::cout << "Initializing TopReconUserHook" << std::endl;
        myUserHooks = new TopReconUserHooks( 9 - modeID, TOP_fracGluon );
        pythia.setUserHooksPtr(myUserHooks);
        }
    }

    pythia.init();

    // Parton and Hadron Level event records. Remeber to initalize.
    //  Event partonLevelEvent;
    Event partonLevelEvent;
    partonLevelEvent.init("Parton Level event record", &pythia.particleData);
    Event hadronLevelEvent;
    hadronLevelEvent.init("Hadron Level event record", &pythia.particleData);

    //  Parameters for the jet finders. Need select = 1 to catch partons.
    double radius   = 0.4;
    double pTjetMin = 5.;
    double pTpartjetMin = 5.;
    double etaMax   = 1.2;
    int select      = 1;

    // Set up anti-kT clustering, comparing parton and hadron levels.
    SlowJet antiKTpartons( -1, radius, pTpartjetMin, etaMax, select);
    SlowJet antiKThadrons( -1, radius, pTjetMin, etaMax, select);

    // Histograms.
    Hist pThardP("pT for hardest jet, parton level", 100, 0., 100.);
    Hist pThardH("pT for hardest jet, hadron level", 100, 0., 100.);
    //int CUT_NUM_JETS = 0;
    //int CUT_JET_PT = 0;
    int PASSCUT_PARTON_PT = 0;

    double CUT_PARTON_PT = 5.;

    // Begin event loop. Generate event. Skip if error.
    for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
        if (!pythia.next()) continue;

        // Construct parton and hadron level event.
        getPartonLevelEvent( pythia.event, partonLevelEvent);
        getHadronLevelEvent( pythia.event, hadronLevelEvent);

        // List first few events.
        if (iEvent < nListEvts) {
            pythia.event.list();
            partonLevelEvent.list();
            hadronLevelEvent.list();
        }
        // Analyze jet properties and list first few analyses.
        antiKTpartons.analyze( partonLevelEvent );
        antiKThadrons.analyze( hadronLevelEvent );


        //Check pt of partons in jet
        bool JetHardness = false;
        //std::cout << "NEW EVENT" << std::endl;
        for( int j = 0 ; j < antiKTpartons.sizeJet(); ++j){
            std::vector<int> jetpartons = antiKTpartons.constituents(j);
          //  std::cout << "NEW JET" << std::endl;
            JetHardness = false;
            for( unsigned int jp = 0; jp < jetpartons.size(); ++jp){
            //   std::cout <<  partonLevelEvent[ jetpartons[jp] ].pT() << std::endl;
               if( partonLevelEvent[ jetpartons[jp] ].pT() > CUT_PARTON_PT ){
                   //All partons higher pt or at least one?
                   JetHardness = true;
                   break;
               }
            }
            //all jets must have hard components hard?
            if(!JetHardness) break;
        }

        //new event if too soft jets
        if(!JetHardness) continue;
        ++PASSCUT_PARTON_PT;

        if (iEvent < nListJets) {
            antiKTpartons.list();
            antiKThadrons.list();
        }

        //Dijet selection
        //cout << "Number of hadron jets: " << antiKThadrons.sizeJet() << endl;
        //if ( antiKThadrons.sizeJet() != 2)  continue;
        //++CUT_NUM_JETS;
        //for( int iJ = 0; iJ < antiKThadrons.sizeJet(); ++iJ)
        //Cut for max pt of jets
        //if ( antiKThadrons.pT(0) > 500 )  continue;
        //if ( antiKThadrons.pT(1) > 500 )  continue;
        //++CUT_JET_PT;

        //Filling hist of hardest jet pt
        if ( antiKThadrons.sizeJet() > 0)
            pThardH.fill( antiKThadrons.pT(0) ); // 0-hardest jet
        if ( antiKTpartons.sizeJet() > 0 )
            pThardP.fill( antiKTpartons.pT(0) ); // 0-hardest jet
        // End of event loop. Statistics. Histograms.

        //Fill  the HepMC file
        HepMC:: GenEvent* hepmcevt= new  HepMC :: GenEvent ();
        ToHepMC.fill_next_event( pythia  , hepmcevt  );
        ascii_io  << hepmcevt;
        delete  hepmcevt;
    }
    pythia.stat();
    cout << "NUMBER OF ALL EVENTS:" << nEvent << endl;
    cout << "NUMBER OF EVENTS AFTER PARTON PT CUT:" << PASSCUT_PARTON_PT << endl;
  //  cout << "NUMBER OF EVENTS AFTER DIJET CUT:" << CUT_NUM_JETS << endl;
    cout << "pT0 cutoff parameter set to: "<<  pythia.word("Main:spareWord2") <<endl;
    cout << "CHOSEN SEED (stored as well in .cmnd file)" <<  pythia.word("Main:spareWord3") << endl;

    //Check histograms
    cout << pThardP << pThardH;
    pThardP.table("pt_part");
    pThardH.table("pt_hard");
    return 0;
}

