#!/bin/sh
#This file serves to reduce file size using tar
#DR="plots/Jobs13TeV/pythia8/highstat_top"
#Todo list first the directory and then decide which shoudl be tarred
#echo Select which directory should be tarred
BINDR=~/bin
ANALYSISDR=~/top_ana
SUBMIT_SCRIPT=$BINDR/send2pbsWithLimit
SUBD=`pwd` 
WORKING_DR=$ANALYSISDR/pythia8/Jobs13TeV/pythia8Jobs/mb/
SUBMIT_FILE=$SUBD/submit_tar.sh
M=$1
DRM=$WORKING_DR$M
QUE="iatlas"
#QUE="gridlatlas"
echo Working in a directory $DRM

#Create run directory
#Todo check if already exist and add possiblitity to keep or remove
RUN_DR=$SUBD/tar_run
if [ ! -d "$RUN_DR" ]; then
mkdir $RUN_DR;
fi

# Need to put right upper commas at the beggining and end 
files=`ls $DRM | grep "hepmc"`
#cd /$VERSIONS/cr_comparison
cd $DRM

# Erase content of submit file if it exists
cat > $SUBMIT_FILE << EOF
EOF

# Fill the file with submit commands
for NUM in $files 
do
OBJECT=$DRM/$NUM


#Creating run file with given number according to scheme and ordering
SUFFIX=.hepmc
PREFIX=ttbar_crm
REDUCE=$PREFIX*$SUFFIX
NUMBER=`echo $NUM | sed -e "s/^$PREFIX//" -e "s/$SUFFIX$//"` 
RUN_FILE=$RUN_DR/run_$NUMBER.sh
cat > $RUN_FILE << EOF1
tar czf $OBJECT.tgz $OBJECT 
rm $OBJECT
EOF1


#Do not take hepmc files which are already tarred 
if echo $OBJECT | grep -q ".tgz"; then
continue;
else
cat >> $SUBMIT_FILE << EOF2
$SUBMIT_SCRIPT $QUE $RUN_FILE 
EOF2
fi


#tar czf $DRM/$NUM.tgz $DRM/$NUM
#rm $NUM
done
cd $SUBD


